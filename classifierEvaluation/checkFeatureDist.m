% checkFeatureDist plots feature distribution.
%
% checkFeatureDist(DS, dir_tar,clusterNum)
% ----------------------------------------------
%
% Plots feature distributions for users to check feature properties.
% These plots include:
% 1)class distribution: to see if the number of each classes distribute evenly
% 2)distribution of each feature using boxplot
% 3)distribution of each feature using histogram
% 4)relationship between class and feature: to check if there is single
% feature which is good enough for classifying polarities. The line links substructures belongs to the same neuron 
% 
%Input
% -----
% - DS: structure variable containing dataset information
%   (http://neural.cs.nthu.edu.tw/jang/books/dcpr/dataSetIntro.asp?title=2-1%20Intro.%20to%20Datasets)
% - dir_tar: target directory to save plots
% - clusterNum: substructure number of each neuron
% 
% Output
% ------
% figures 
% 
% Example
% -------
% checkFeatureDist(DS, './result',clusterNum)
%
% See also machine learning toolbox
% (http://neural.cs.nthu.edu.tw/jang/matlab/toolbox/machineLearning/)

function checkFeatureDist(DS, dir_tar,clusterNum)
color = eye(3);
identity = unique(DS.output);
fontsize_title = 12;
fontsize_label = 10;
linewidth_box = 3;
num_row = 4;
num_col = ceil(length(DS.inputName)/num_row);
neuron_ID = [];
for num = 1:length(clusterNum)
    neuron_ID = [neuron_ID num*ones(1,clusterNum(num))];
end

% basic data checkup
% (1) class distribution
figure;

[classSize, classLabel] = dsClassSize(DS,1);
title('Class distribution','fontsize',fontsize_title, 'fontweight', 'bold');
xlabel('Classes','fontsize',fontsize_label);
ylabel('Class sizes','fontsize',fontsize_label);
set(gca,'XTickLabel',{'Axon','Dendrite'},'Fontsize',fontsize_label);
saveas(gcf, strcat(dir_tar, 'Class_size_dist'), 'fig');
%close all

% (2) distribution of each input using boxplot
figure;

boxplot(DS.input', DS.inputName, 'plotstyle', 'compact');
title(' Feature boxplot','fontsize', fontsize_title, 'fontweight', 'bold');
saveas(gcf, strcat(dir_tar, 'Feature_boxplot'), 'fig');
%close all

% (3) distribution of each input using histogram
figure;

for f = 1:length(DS.inputName)
    subplot(num_row,num_col,f);
    hist(DS.input(f,:));
    title(strcat('(',num2str(f),')',DS.inputName(f)),'fontsize',12);
end
mtit('Feature distribution','fontsize', fontsize_title, 'fontweight', 'bold');
saveas(gcf, strcat(dir_tar, 'Feature_histogram'), 'fig');
%close all

% (4) relationship between class-input pairs
figure;

for f = 1:length(DS.inputName)
    subplot(num_row,num_col,f);
    for ide = 1:length(identity)
        member = find(DS.output == identity(ide));
        plot(DS.input(f,member), identity(ide), '.','color',color(identity(ide),:), 'markersize', 20);
        hold on
    end
    for num = 1:length(clusterNum)
        member = find(neuron_ID == num);
        plot(DS.input(f,member), DS.output(member), 'color', 'k');
    end
    set(gca,'YTickLabel',{'Axon','Dendrite'},'YTick', 2:3);
    title(strcat('(',num2str(f),')',DS.inputName(f)),'fontsize',12);
end
mtit('Feature vs class','fontsize', fontsize_title, 'fontweight', 'bold');
saveas(gcf, strcat(dir_tar, 'Feature_distribution_pair'), 'fig');
%close all