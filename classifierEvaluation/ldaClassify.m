% This function calculates LDA score and decide identity
function ldaResult = ldaClassify(DS, classifierInfo, clusterNum)
ldaResult.neuronID = [];
for num = 1:length(clusterNum)     
    ldaResult.neuronID = [ldaResult.neuronID ones(1,clusterNum(num))*num];
end
rawData = DS.input(classifierInfo.bestFeature,:);
ldaResult.polarity_ans = DS.output;
% calculate pair lda accuracy
[ldaResult.ldaScore ldaResult.polarity_pair] = judgePolarity(rawData, classifierInfo.weight, clusterNum, classifierInfo.identifyInfo.identityOrder);
ldaResult.accuracy_pair = (1 - length(find(ldaResult.polarity_ans - ldaResult.polarity_pair))/sum(clusterNum))*100; % the percentage of accurately classified samples
% calculate group lda accuracy 
DS_lda = DS;
DS_lda.input = ldaResult.ldaScore;
DS_lda.inputName = {'input1'};
knncPrm.k = 11;
ldaResult.accuracy_knnc = knncLoo(DS_lda, knncPrm, 0)*100;
ldaResult.polarity_linear = lincEval(DS_lda, classifierInfo.linearCoef);
ldaResult.polarity_linear = ldaResult.polarity_linear+1; % the linear classifier use 1 and 2 as class ID
ldaResult.accuracy_linear = (1 - length(find(ldaResult.polarity_ans - ldaResult.polarity_linear))/sum(clusterNum))*100;
for num = 1:length(classifierInfo.identifyInfo.class)
    member = find(ldaResult.polarity_ans == classifierInfo.identifyInfo.class(num));
    ldaResult.statistics.mean(num) = mean(ldaResult.ldaScore(member));
    ldaResult.statistics.std(num) = std(ldaResult.ldaScore(member));
end
% give warning
ldaResult.warning_pair = zeros(1, sum(clusterNum));
ldaResult.warning_linear = zeros(1, sum(clusterNum));
for num = 1:sum(clusterNum)
    member = find(ldaResult.neuronID == ldaResult.neuronID(num));
    [c inx_max] = max(ldaResult.ldaScore(member));
    [c inx_min] = min(ldaResult.ldaScore(member));
    exclude = member([inx_max, inx_min]);
    if ~isempty(intersect(num, exclude))
        continue;
    end
    for class = 1:length(unique(DS.output))
        ldaResult.dist2mean(num,class) = abs(ldaResult.ldaScore(num) - classifierInfo.identifyInfo.mean(class))/classifierInfo.identifyInfo.std(class);
    end
    judgedID_pair = find(unique(DS.output) == ldaResult.polarity_pair(num));
    anotherID_pair = setdiff(1:length(unique(DS.output)), judgedID_pair);
    ldaResult.distDiff_pair(num) = (ldaResult.dist2mean(num, judgedID_pair)- ldaResult.dist2mean(num, anotherID_pair));
    judgedID_linear = unique(DS.output) == ldaResult.polarity_linear(num);
    anotherID_linear = setdiff(1:length(unique(DS.output)), judgedID_linear);
    ldaResult.distDiff_linear(num) = (ldaResult.dist2mean(num, judgedID_linear)- ldaResult.dist2mean(num, anotherID_linear));
    
    if ldaResult.distDiff_pair(num)>0 && length(find(ldaResult.neuronID == ldaResult.neuronID(num)))>2
        ldaResult.warning_pair(num) = 1;
    end
    if ldaResult.distDiff_linear(num)>0 && length(find(ldaResult.neuronID == ldaResult.neuronID(num)))>2
        ldaResult.warning_linear(num) = 1;
    end
end

% use the mean difference divide summation of std to evaluate how good the
% lda projection is
ldaResult.evaluation = abs(ldaResult.statistics.mean(1)-ldaResult.statistics.mean(2))/sqrt(ldaResult.statistics.std(1)^2 + ldaResult.statistics.std(2)^2);   