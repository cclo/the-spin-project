% PLOTPAIRDISTRIBUTION   returns summary plot of identification result 
%
% h = plotPairDistribution(feature, realIdentity, clusterNum, annotation)
% --------------------
%
% returns summary plot figure handle
%
% Input
% -----
% - feature::discriminant score of each substructure
% - realIdentity:: the identified identity of substructures
% - clusterNum:: the number of substructures of each neuron
% - annotation:: a tag which will be shown on the title
%
% Output
% ------
% - h::figure handle of the resulting plot
%
% 
% 
function h = plotPairDistribution(feature, realIdentity, clusterNum, annotation)
fontsize_title = 25;
fontsize_label = 15;
markersize_small = 25;
linewidth_box = 3;
linewidth_plot = 7;
identity = [2 3];
color = eye(3);
marker = {'x','o'};
figure;
maximize(gcf);
neuron_ID = [];
for num = 1:length(clusterNum)
    neuron_ID = [neuron_ID num*ones(1,clusterNum(num))];
end
for ide = 1:length(identity)
    member = find(realIdentity == identity(ide));
    plot(neuron_ID(member), feature(member), marker{ide},...
        'color', color(identity(ide),:), 'markersize', markersize_small,...
        'linewidth',linewidth_plot); 
    hold on;
end
% plot neuron-by-neuron boundary
for neuron = 1:length(clusterNum)
    member = find(neuron_ID == neuron);
    boundary = (max(feature(member))+min(feature(member)))/2;
    plot(neuron, boundary, '*', 'color', 'k', 'markersize', 10);
end
box off;
title(strcat('Pairwise feature distribution in best LDA axis:',annotation), 'fontsize', fontsize_title,'fontweight', 'bold');
xlabel('Neuron ID', 'fontsize', fontsize_label, 'fontweight', 'bold');
ylabel('Best LDA axis', 'fontsize', fontsize_label, 'fontweight', 'bold');
xlim([0 length(clusterNum)+1]); 
legend('Axon','Dendrite','Location','northwestOutside');
set(gca, 'XTick', 1:1:length(clusterNum), 'Xgrid', 'on','fontsize',fontsize_label,'linewidth', linewidth_box,'fontweight', 'bold');
h = gcf;

%saveas(gcf, strcat(dir_tar, strcat('Best lda feature_',annotation)), 'fig');
%close all