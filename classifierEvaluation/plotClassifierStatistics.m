function plotClassifierStatistics(classifier, numSample)
color = eye(3);
for ide = 1:length(classifier.identifyInfo.mean)
    plot([1 numSample], [classifier.identifyInfo.mean(ide) classifier.identifyInfo.mean(ide)], 'color',color(classifier.identifyInfo.class(ide),:),'linewidth',3);
    x = [1 numSample numSample 1];
    y = [classifier.identifyInfo.mean(ide)-classifier.identifyInfo.std(ide),classifier.identifyInfo.mean(ide)-classifier.identifyInfo.std(ide), ...
         classifier.identifyInfo.mean(ide)+classifier.identifyInfo.std(ide),classifier.identifyInfo.mean(ide)+classifier.identifyInfo.std(ide)];
    patch(x,y, color(classifier.identifyInfo.class(ide),:));
    alpha(0.2);
end