function plotWrongCase(feature, realID, identifiedID, neuronID)
markersize_small = 25;
linewidth_plot = 7;
identity = [2 3];
marker = {'x','o'};
wrongCase = find(realID-identifiedID);

for ide = 1:length(identity)
    member = find(realID == identity(ide));
    wrong_temp = intersect(member, wrongCase);
    plot(neuronID(wrong_temp), feature(wrong_temp), marker{ide},...
        'color', 'r', 'markersize', markersize_small,...
        'linewidth',linewidth_plot); 
    hold on;
end
