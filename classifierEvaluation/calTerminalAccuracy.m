function accuracy = calTerminalAccuracy(tree_ans, tree_predict)
accuracy = zeros(1, length(tree_predict));
for num = 1:length(tree_predict)
    tp = find(T_tree(tree_ans{num}));
    % for real polarity
    regionInx = str2double(tree_ans{num}.rnames);
    polarity_real = regionInx(tree_ans{num}.R(tp));
    % for predict polarity
    regionInx = str2double(tree_predict{num}.rnames);
    % region index 20~23 all represent axons
    regionInx(regionInx >= 20 & regionInx <= 23) = 2;
    % region index 30~33 all represent axons
    regionInx(regionInx >= 30 & regionInx <= 33) = 3;
    polarity_predict = regionInx(tree_predict{num}.R(tp));
    difference = polarity_real - polarity_predict;
    wrong = length(find(difference));
    accuracy(num) = (1-(wrong/length(tp)))*100;    
    %{
    difference = tree_ans{num}.R(tp) - tree_predict{num}.R(tp);
    wrong = length(find(difference));
    accuracy(num) = (1-(wrong/length(tp)))*100;
    %}
end
