function plotDiscriminantAxisDist(DS)
identity = unique(DS.output);
color = eye(3);
%% setting
fontsize_title = 20;
fontsize_label = 15;
markersize_small = 25;
linewidth_box = 5;
linewidth_plot = 7;
marker = {'x','o'};
linePattern = {'-.s','--*'};
% plot discriminant axis distribution for different classes
figure('Position', [100, 100, 1300, 895])
%maximize(gcf);
for ide = 1:length(identity)
    member = find(DS.output == identity(ide));
    [n xout] = hist(DS.input(1,member));
    plot(xout, n, linePattern{ide},'color',color(ide,:), 'markersize',markersize_small,'linewidth',linewidth_plot);
    hold on;
    plot(DS.input(1,member), ones(1,length(member))*0.1, marker{ide}, 'color',color(ide,:), 'markersize', markersize_small,'linewidth',linewidth_box);
end
% plot mean and standard deviation
xlabel('Best LDA axis score', 'fontsize',fontsize_label,'fontweight','bold');
ylabel('Frequency','fontsize',fontsize_label,'fontweight','bold');
legend({'Axon','Axon','Dendrite','Dendrite'},'Location','NorthEastOutside');
title('LDA distribution for different classes', 'fontsize', fontsize_title,'fontweight','bold');
set(gca, 'fontsize', fontsize_label, 'linewidth', linewidth_box,'fontweight','bold');
grid on