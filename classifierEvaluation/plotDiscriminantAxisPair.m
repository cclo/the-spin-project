function plotDiscriminantAxisPair(DS, classifier)  
% plot discriminant axis distribution for different classes, neuron by neuron
identity = unique(DS.output);
color = eye(3);
%% setting
fontsize_title = 20;
fontsize_label = 15;
markersize_small = 25;
linewidth_box = 5;
linewidth_plot = 7;
marker = {'x','o'};
linePattern = {'-.s','--*'};
figure('Position', [100, 100, 1500, 895])
%maximize(gcf);
[recogRate inx] = max(classifier.allRecogRate_lda(1:length(DS.input(:,1))));
for ide = 1:length(identity)
    member = find(DS.output == identity(ide));
    plot(DS.neuronID(member), DS.input(inx(1), member), marker{ide}, 'color', color(identity(ide),:),...
        'markersize', markersize_small, 'linewidth',linewidth_plot); 
    hold on;
end
xlim([0 length(DS.clusterNum)+1]); 
title('Pairwise feature distribution in best LDA axis', 'fontsize', fontsize_title,'fontweight','bold');
xlabel('Neuron ID', 'fontsize', fontsize_label, 'fontweight', 'bold');
ylabel('Best LDA axis', 'fontsize', fontsize_label, 'fontweight', 'bold');
legend(DS.outputName(identity+1),'Location','NorthEastOutside');
set(gca, 'XTick', 1:1:length(DS.clusterNum), 'Xgrid', 'on','fontsize',fontsize_label,'linewidth', linewidth_box,'fontweight','bold');
