function plotWarning(ldaScore, neuronID, warning)
numNeuron = length(unique(neuronID));
for num = 1:numNeuron
    member = find(neuronID == num);
    for sub = 1:length(member)
        %text(num+0.25, DS_unknown.ldaScore(member(sub))+0.01, num2str(DS_unknown.oriID(member(sub))),'fontsize',12);
        text(num+0.4, ldaScore(member(sub))+0.01,num2str(sub),'fontsize',15);
        if warning(member(sub))==1 
            plot(num, ldaScore(member(sub)),'s','color','k','markersize',15,'linewidth',3);
        end
    end
end 