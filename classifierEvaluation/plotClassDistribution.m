function plotClassDistribution(feature, realIdentity, mean, std, annotation)
fontsize_title = 25;
fontsize_label = 20;
fontsize_text = 15;
markersize_small = 25;
markersize_large = 40;
linewidth_box = 3;
linewidth_plot = 5;
identity = [2 3];
color = eye(3);
marker = {'x','o'};
linePattern = {'-.s','--*'};
transparency = 0.2;

figure;
n = [];
for ide = 1:length(identity)
    member = find(realIdentity == identity(ide));
    [n_temp xout] = hist(feature(1,member));
    n = [n n_temp];
    plot(xout, n_temp, linePattern{ide},'color',color(ide,:), 'markersize',markersize_small,'linewidth',linewidth_plot);
    hold on;
    plot(feature(1,member), ones(1,length(member))*0.1, marker{ide}, 'color',color(ide,:), 'markersize', markersize_small,'linewidth',linewidth_plot);
end

legend({'Axon','Axon','Dendrite','Dendrite'},'fontsize',fontsize_label,'fontweight', 'bold','location','NorthEastOutside');
% plot mean and standard deviation
top = max(n) + 3;
for num = 1:length(identity)
    x = [mean(num) mean(num)];
    y = [0 top];
    plot(x, y, 'color',color(num,:), 'linewidth', linewidth_box);
    position_x = mean(num);
    position_y = max(n);
    %text(position_x, position_y, 'mean', 'fontsize', fontsize_text); 
    x = [mean(num) - std(num), mean(num) + std(num), mean(num) + std(num), mean(num) - std(num)];
    y = [0 0 top top];
    patch(x,y,color(num,:));
    position_x = mean(num)+std(num);
    position_y = max(n);
    %text(position_x, position_y, 'std','fontsize', fontsize_text); 
end
alpha(transparency);
xlabel('Best LDA axis score', 'fontsize',fontsize_label,'fontweight', 'bold');
ylabel('Frequency','fontsize',fontsize_label,'fontweight', 'bold');

title(strcat('LDA distribution for different classes:',annotation), 'fontsize', fontsize_title,'fontweight', 'bold');
set(gca, 'fontsize', fontsize_label, 'linewidth', linewidth_box, 'ytick', 0:5:top,'fontweight', 'bold');
%saveas(gcf, strcat(dir_tar, strcat('Feature_space_bestlda_',classifierType)), 'fig');
%close all
