% plotTrainingResult plot classification result
%
% plotTrainingResult(DS, classifier, dir_tar, clusterNum, classifierType)
% ---------------------------------------------------------
%
% plot classification result, including:
% 1) feature space of the best three features
% 2) plot discriminant axis distribution for different classes
% 3) plot discriminant axis distribution for different classes, neuron by
%    neuron
%
% Input
% ------
% - DS: structure variable containing neuron info and feature selection
% results
% - classifier: structure variable containing classifier info
% - dir_tar: target directory for saving figures
% - clusterNum: number of substructures of each neuron
% - classifierType: way performing feature selection (sequential/ exhaust) 
%
% Output
% ------
% figures
%
function plotTrainingResult(DS, classifier, dir_tar, clusterNum, classifierType)
identity = unique(DS.best.output);
color = eye(3);
neuron_ID = [];
for num = 1:length(clusterNum)
    neuron_ID = [neuron_ID num*ones(1,clusterNum(num))];
end

%% setting
fontsize_title = 20;
fontsize_label = 15;
markersize_small = 25;
markersize_large = 40;
linewidth_box = 5;
linewidth_plot = 7;
marker = {'x','o'};
linePattern = {'-.s','--*'};

%% plot classification results
% (1) plot the best 3 features
if length(classifier.bestSelectedInput) >= 3 
    [recogRate, inx] = sort(classifier.allRecogRate(classifier.bestSelectedInput),'descend');
    figure('Position', [100, 100, 1300, 895])
    %maximize(gcf);
    for ide = 1:length(identity)
        member = find(DS.best.output == identity(ide));
        plot3(DS.best.input(inx(1), member),...
        DS.best.input(inx(2), member),...
        DS.best.input(inx(3), member),...
        marker{ide}, 'color', color(identity(ide),:),...
        'markersize', markersize_small,...
        'linewidth',linewidth_plot); 
        hold on;
    end
    title('The best three features', 'fontsize', fontsize_title,'fontweight','bold');
    xlabel(DS.best.inputName{inx(1)},'fontsize',fontsize_label, 'fontweight', 'bold');
    ylabel(DS.best.inputName{inx(2)},'fontsize',fontsize_label, 'fontweight', 'bold');
    zlabel(DS.best.inputName{inx(3)},'fontsize',fontsize_label, 'fontweight', 'bold');
    grid on
    legend(DS.best.outputName(identity+1),'Location','NorthEastOutside');
    set(gca, 'fontsize', fontsize_label, 'linewidth', linewidth_box,'fontweight','bold');
    saveas(gcf, strcat(dir_tar, strcat('Feature_space_best3_',classifierType)), 'fig');
    %close all
end

% (2) plot discriminant axis distribution for different classes
figure('Position', [100, 100, 1300, 895])
%maximize(gcf);
for ide = 1:length(identity)
    member = find(DS.lda.output == identity(ide));
    [n xout] = hist(DS.lda.input(1,member));
    plot(xout, n, linePattern{ide},'color',color(ide,:), 'markersize',markersize_small,'linewidth',linewidth_plot);
    hold on;
    plot(DS.lda.input(1,member), ones(1,length(member))*0.1, marker{ide}, 'color',color(ide,:), 'markersize', markersize_small,'linewidth',linewidth_box);
end
% plot mean and standard deviation
xlabel('Best LDA axis score', 'fontsize',fontsize_label,'fontweight','bold');
ylabel('Frequency','fontsize',fontsize_label,'fontweight','bold');
legend({'Axon','Axon','Dendrite','Dendrite'},'Location','NorthEastOutside');
title('LDA distribution for different classes', 'fontsize', fontsize_title,'fontweight','bold');
set(gca, 'fontsize', fontsize_label, 'linewidth', linewidth_box,'fontweight','bold');
grid on
%set(gca, 'xgrid','on');
saveas(gcf, strcat(dir_tar, strcat('bestlda_distribution_group_',classifierType)), 'fig');
%close all

% (3) plot discriminant axis distribution for different classes, neuron by neuron
figure('Position', [100, 100, 1500, 895])
%maximize(gcf);
[recogRate inx] = max(classifier.allRecogRate_lda(1:length(DS.lda.input(:,1))));
for ide = 1:length(identity)
    member = find(DS.lda.output == identity(ide));
    plot(neuron_ID(member), DS.lda.input(inx(1), member), marker{ide}, 'color', color(identity(ide),:),...
        'markersize', markersize_small, 'linewidth',linewidth_plot); 
    hold on;
end
xlim([0 length(clusterNum)+1]); 
title('Pairwise feature distribution in best LDA axis', 'fontsize', fontsize_title,'fontweight','bold');
xlabel('Neuron ID', 'fontsize', fontsize_label, 'fontweight', 'bold');
ylabel('Best LDA axis', 'fontsize', fontsize_label, 'fontweight', 'bold');
legend(DS.lda.outputName(identity+1),'Location','NorthEastOutside');
set(gca, 'XTick', 1:1:length(clusterNum), 'Xgrid', 'on','fontsize',fontsize_label,'linewidth', linewidth_box,'fontweight','bold');
saveas(gcf, strcat(dir_tar, strcat('bestlda_distribution_pair_',classifierType)), 'fig');
%close all

