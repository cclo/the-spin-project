function plotFeatureIn3D(DS,classifier)
identity = unique(DS.output);
color = eye(3);
%% setting
fontsize_title = 20;
fontsize_label = 15;
markersize_small = 25;
markersize_large = 40;
linewidth_box = 5;
linewidth_plot = 7;
marker = {'x','o'};
linePattern = {'-.s','--*'};

%% plot classification results
% plot the best 3 features distribution in 3D space
if length(classifier.bestSelectedInput) >= 3 
    [recogRate, inx] = sort(classifier.allRecogRate(classifier.bestSelectedInput),'descend');
    figure('Position', [100, 100, 1300, 895])
    %maximize(gcf);
    for ide = 1:length(identity)
        member = find(DS.output == identity(ide));
        plot3(DS.input(inx(1), member),...
        DS.input(inx(2), member),...
        DS.input(inx(3), member),...
        marker{ide}, 'color', color(identity(ide),:),...
        'markersize', markersize_small,...
        'linewidth',linewidth_plot); 
        hold on;
    end
    title('The best three features', 'fontsize', fontsize_title,'fontweight','bold');
    xlabel(DS.inputName{inx(1)},'fontsize',fontsize_label, 'fontweight', 'bold');
    ylabel(DS.inputName{inx(2)},'fontsize',fontsize_label, 'fontweight', 'bold');
    zlabel(DS.inputName{inx(3)},'fontsize',fontsize_label, 'fontweight', 'bold');
    grid on
    legend(DS.outputName(identity+1),'Location','NorthEastOutside');
end