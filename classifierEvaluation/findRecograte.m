% This function find the accuracy of selected feature combination
function [accuracy id] = findRecograte(targetCombination, subjectClassifier)

num_featureCombination = cellfun(@length, subjectClassifier.allSelectedInput);
candidate_temp = find(num_featureCombination == length(targetCombination));
ID_wanted = cellfun(@(x) isequal(targetCombination, x), subjectClassifier.allSelectedInput(candidate_temp));
accuracy = subjectClassifier.allRecogRate(candidate_temp(ID_wanted));
id = candidate_temp(ID_wanted);