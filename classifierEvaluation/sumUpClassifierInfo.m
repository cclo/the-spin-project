% sumUpClassifierInfo sums up final classifier information
%
% classifierInfo_result = sumUpClassifierInfo(DS, bestAxis)
% ---------------------------------------------------------
%
% gather information got from feature selection to serve as the classifier
%
% Input
% ------
% - DS: structure variable containing representative features for classifying (DS.input) and the desired classes (DS.output)  
%
% Output
% ------
% -classifierInfo_result: information of the classifier
% -- classifierInfo_result.class: classes that could be identified by this
%    classifier
% -- classifierInfo_result.mean: the meane score of each class
% -- classifierInfo_result.std: standard deviation of each class
% -- classifierInfo_result.identifyOrder: the order of identity, from large to small.
%    For example, if idaOrder = [3 2], this means in this lda axis, the larger the score is, the larger the 
%    probability it belongs to class 3.
%

function classifierInfo_result = sumUpClassifierInfo(DS, bestAxis)
classifierInfo_result.class = unique(DS.output);
for num = 1:length(classifierInfo_result.class)
    member = find(DS.output == classifierInfo_result.class(num));
    classifierInfo_result.mean(num) = mean(DS.input(bestAxis, member)); % the mean score of certain class on the best lda axis
    classifierInfo_result.std(num) = std(DS.input(bestAxis, member)); % the std score of certain class on the best lda axis
end
[val inx] = sort(classifierInfo_result.mean, 'descend');
classifierInfo_result.identityOrder = classifierInfo_result.class(inx);