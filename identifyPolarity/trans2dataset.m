% trans2dataset    Transform our neuron data to fit the dataset format in machine learning toolbox. 
%
% DS = trans2dataset(data, datasetName, identityName, relationship)
%----------------------------------------------------------
%
% Transform our neuron data to fit the dataset format in machine learning toolbox. 
% 
% Input
% -----
% - data: neuron morphological and polarity information
% - datasetName: name of this dataset
% - identityName: classes names, like axon, dendrite, ......
% - relationship: forget T^T
%
% Output
% ------
% - DS: structure variable for storing all the information of a dataset
% -- DS.input:: morphological features
% -- DS.output:: desired classes / ground truth of the dataset
% -- DS.neuronID:: neuron ID of the substructure
% -- DS.dataName:: name of the dataset
%
% Example
% -------
%  DS = trans2dataset(data, datasetName, identityName, relationship)
%
function DS = trans2dataset(data, datasetName, identityName, relationship)
featureName = [fieldnames(data.feature.par{1,1}.gstats)', fieldnames(data.feature.glob{1,1})'];
% normalize the feature to complete neuron (devide by the feature
% calculated in complete neuron)
feature_norm = cell(1,length(data.feature.ori(1,:)));
for num = 1:length(data.feature.ori)
    x = data.feature.ori{1,num};
    y = repmat(data.feature.complete{1,num}, 1, length(x(1,:)));
    feature_norm{1,num} = x./y; 
end
DS = transDataSet_test(feature_norm, data.identity.ori, featureName, datasetName, identityName, relationship, data.clusterNum);
% delete ignored features
DS.input(data.feature.ignore,:) = [];
DS.inputName(data.feature.ignore) = [];

function DS = transDataSet_test(feature_ori, identity_ori, featureName, datasetName, identityName, relationship, clusterNum)
DS = struct;
DS.input = [];
DS.output = [];
DS.relationship = [];
DS.neuronID = [];
for num = 1:length(feature_ori)
    DS.input = [DS.input, feature_ori{1,num}];
    DS.output = [DS.output, identity_ori{1,num}];
    if isempty(relationship)
        continue;
    else
        DS.relationship = [DS.relationship, relationship{num}];
    end
    if isempty(clusterNum)
        continue;
    else
        DS.neuronID = [DS.neuronID, ones(1, clusterNum(num))*num];
    end
end
DS.dataName = datasetName; % the name of the dataset
DS.inputName = featureName;
DS.outputName = identityName;% a cell string that represents the name of the output classes(correspond to the index of dataset.output)