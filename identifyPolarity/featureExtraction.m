function [data tree_sub] = featureExtraction(tree_sub, featureList, featureID, relationship)
%FEATUREEXTRACTION extract morphological features from given tree, and 
% also organize related info into a structure.
% 
% Input
% -----
% - tree_sub: tree structures. tree_sub{1} is the complete neuron. 
% tree_sub{2:end} are subtrees of the neuron.
% - featureList: function name of all the available features. (i.e. all the
% content in featureList.txt')
% featureID: the ID of desired features 
% relationship: (if critical point exists) indicates which subtrees are on
% the same side.
%
% Output
% ------
% - data: structure, contatining
% -- data.clusterNum:: number of subtrees in the given neuron
% -- data.relationship:: the spatial relationship among substructures. 
% (test data only, because training data don't perform morphological 
% clustering) 
% -- data.feature.sub:: extracted features for all the substructures (if
% the feature is not selected, use 0 to fill the table.)
% -- data.feature.complete:: extracted features for the complete neuron
% -- data.identity:: For training data, experimental polarity of each 
% subtree; for test data, means nothing.
% -- data.del_data:: the substructures ID that is deleted (When the 
% substurecture is composed of only one branch, some features like mean 
% branch angle, mean path length, mean asymmetry at branch point will be 
% NaN. In these cases, substructures will be deleted, and their ID will be
% recorded in data.del_data.
%
% - tree_sub: rearrange tree_sub that put deleted subtrees in the end
% 
data.clusterNum = length(tree_sub)-1; % number of substructure
if ~isempty(relationship)
    data.relationship = relationship;
end
% extract features
data.feature.sub = zeros(length(featureList),data.clusterNum);
data.feature.complete = zeros(length(featureList),1);
for iFeature = 1:length(featureID)
    ID = featureID(iFeature);
    tempFeature = eval([featureList{ID} '(tree_sub)']);
    data.feature.complete(ID,1) = tempFeature(1);
    data.feature.sub(ID, :) = tempFeature(2:end);
end
% get region index for training data
data.identity = zeros(1, data.clusterNum);
for iSub = 1:data.clusterNum
    data.identity(iSub) = str2double(tree_sub{iSub+1}.rnames{1});
end
       
% remove substructures that have NaN feature(s)
[temp c] = find(isnan(data.feature.sub));
data.del_data = unique(c)'; % there might be more than one NaN features for a substructure. 
data.feature.sub(:,data.del_data) = [];
if ~isempty(relationship)
    data.relationship(data.del_data) = [];
end
data.identity(data.del_data) = [];

% rearrange order (move deleted substructures to the end)
arrangeOrder = [setdiff(1:data.clusterNum, data.del_data), data.del_data]; 
%{
if ~isempty(relationship)
    data.relationship = data.relationship(arrangeOrder);
end
%}
tree_sub = tree_sub([1 arrangeOrder+1]);
data.del_data = (length(tree_sub)- length(data.del_data) +1) : length(tree_sub);

