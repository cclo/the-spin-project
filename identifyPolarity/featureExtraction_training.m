% featureExtraction_training   calculate morphological features.
%
% data = featureExtraction(tree_sub, clusterNum)
% ----------------------------------------------
%
% return a structure containing morphology features and related info.
% 
% Input
% -----
% - tree_sub: tree structure of all the training neuron
% - clusterNum: substructure number of each neuron
%
% Output
% ------
% - data: structure, contatining
% -- data.feature.par:: eatract local features ( features that could be extracted within a sungle substructure) by "stats_tree"
% -- data.feature.glob:: extract global features (features that could only be calculated when it is thought as part of a complete neuron) by "stats_glob"
% -- data.featureName:: name list of morphological features
% -- data.feature.complete:: morphological features of complete neurons
% -- data.feature.ori :: morphological features of substructures
% -- data.del_data:: the substructures ID that is deleted ( When the substurecture is composed of only one branch, some features likemean
%    branch angle, mean path length, mean asymmetry at branch point will be NaN. In these cases, substructures will be deleted, and their ID will be
%    recorded in data.del_data.
% -- data.clusterNum:: the number of substructures after deleting inappropriate substructure(s) 
% 
% Example
% -------
% [tree_sub, filelist, clusterNum] = getExpData(direc.src.expData);
% trainingData = featureExtraction_training(tree_sub, clusterNum);
%
% See also status_tree stats_glob

function data = featureExtraction_training(tree_sub, clusterNum)
% get partial morphological feature      
data.feature.par = cell(1,length(tree_sub));
disp(' Calculate local feature'); 
disp(' Neuron ID = ');
for f = 1:length(tree_sub)
    fprintf('%d\n', f);
	data.feature.par{f} = stats_tree(tree_sub{f},[],[],'none');
end
% get global morphological feature
data.feature.glob = cell(1,length(tree_sub));
disp(' Calculate global feature'); % 
disp(' Neuron ID = ');
for f = 1:length(tree_sub)
    fprintf('%d\n', f);
	data.feature.glob{f} = stats_glob(tree_sub(f));
end
% Reorganize the feature region index, and delete data with NaN. Usually NaN
% happens when the substructure is composed of only one branch. These kind of structure should be avoided.  
data.featureName = [fieldnames(data.feature.par{1,1}.gstats)', fieldnames(data.feature.glob{1,1})'];
data.feature.ori = cell(1,length(tree_sub));
data.feature.complete = cell(1,length(tree_sub));
data.identity.ori = cell(1,length(tree_sub));
data.del_data = cell(1,length(tree_sub));
for num = 1:length(tree_sub)
    temp_feature = [structure_content(data.feature.par{1,num}.gstats); structure_content(data.feature.glob{num})];
    temp_identity = [];
    for cluster = 1:clusterNum(num)+1
        temp_identity = [ temp_identity str2double(tree_sub{1,num}{1,cluster}.rnames{1})];
    end
    [r c] = find(isnan(temp_feature));
    data.del_data{num} = unique(c)';
    temp_feature(:,data.del_data{num}) = [];
    temp_identity(data.del_data{num}) = [];
    data.feature.ori{num} = real(temp_feature(:,2:end)); % morphology features of each substructure
    data.feature.complete{num} = real(temp_feature(:,1)); % morphology features of each neuron (complete)
    data.identity.ori{num} = temp_identity(2:end); % region index (axon or dendrite) of each substructure 
    data.clusterNum(num) = clusterNum(num) - length(data.del_data{num}); % substructure number 
end