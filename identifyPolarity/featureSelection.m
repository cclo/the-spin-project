% featureSelection(DS, dir_tar)    Select useful features for classify neuronal polarities. 
%
% [classifierInfo, DS] = featureSelection(DS, dir_tar)
%----------------------------------------------------------
%
% Select useful features for classifing neuronal polarities. 
% 
% Input
% -----
% - DS: neuron info organized as dataset format (organized by trans2dataset)
% - dir_tar: target directory for saving feature selection result plot
%
% Output
% ------
% - classifierInfo: structure variable for storing classifier information, containing
% -- classifierInfo.seq/exh.bestSelectedInput:: best feature(s) selected by sequential/exhaust method
% -- classifierInfo.seq/exh.bestRecogRate:: the best recognition rate when using the best feature combination
% -- classifierInfo.seq/exh.allSelectedInput:: all the feature combinations tetsted in sequential/exhaust method
% -- classifierInfo.seq/exh.allRecogRate:: recognition rates of all the feature combinations
% -- classifierInfo.seq/exh.elapsedTime:: time taken for sequential/exhaust feature selection
% - DS.seq/exh: neuron info which only contains best features selected by sequential/exhaust ways, and linear discriminant
%   analysis will be performed on these dataset to find the discrminant axis
%
% Example
% -------
%  [classifierInfo, DS] = featureSelection(DS, './Result/')
%
function [classifierInfo, DS] = featureSelection(DS, k, option_seq, option_exh)
%k = 11; % number of neighbors used in k-nearest classifier (knnc)
inputNum = length(DS.machineLearning.input(:,1)); %17;
classifiertype = 'knncLoo';
%% sequential selection
if option_seq
    disp( 'Perform sequential feature selection:');
    [classifierInfo.seq.bestSelectedInput, classifierInfo.seq.bestRecogRate,...
     classifierInfo.seq.allSelectedInput, classifierInfo.seq.allRecogRate,...
     classifierInfo.seq.elapsedTime] = inputSelectSequential(DS.machineLearning,inputNum,classifiertype,k,0);
    [DS.seq.best DS.seq.lda classifierInfo.seq] = getDiscriminantAxis(classifierInfo.seq.bestSelectedInput, DS.machineLearning, k, classifiertype);
 %{
    %%%%% (when the number of best features is more than 1)dimension reduction: use the best combination found in sequential
    %%%%% selection to do linear discriminant analysis (LDA)
    DS.seq.best = DS.machineLearning;
    DS.seq.best.input = DS.seq.best.input(classifierInfo.seq.bestSelectedInput,:);
    DS.seq.best.inputName = DS.seq.best.inputName(classifierInfo.seq.bestSelectedInput);
    [DS.seq.lda, DS.seq.lda.discrimVec, DS.seq.lda.eigValues] = lda(DS.seq.best);
    DS.seq.lda.inputName = {};
    for num = 1:length(DS.seq.lda.input(:,1))
        DS.seq.lda.inputName = [DS.seq.lda.inputName, strcat('Input', num2str(num))];
    end
    %%%%% use result gained in LDA to do sequential feature selection
    [classifierInfo.seq.bestSelectedInput_lda, classifierInfo.seq.bestRecogRate_lda,...
     classifierInfo.seq.allSelectedInput_lda, classifierInfo.seq.allRecogRate_lda,...
     classifierInfo.seq.elapsedTime_lda] = inputSelectSequential(DS.seq.lda,length(classifierInfo.seq.bestSelectedInput),classifiertype,k,0);
     %}
end

%% exhaust feature selection
if option_exh
    disp( 'Perform exhaustive feature selection:');
    [classifierInfo.exh.bestSelectedInput, classifierInfo.exh.bestRecogRate,...
    classifierInfo.exh.allSelectedInput, classifierInfo.exh.allRecogRate,...
    classifierInfo.exh.elapsedTime] = inputSelectExhaustive(DS.machineLearning,inputNum,classifiertype,k,0);
    [DS.exh.best DS.exh.lda classifierInfo.exh] = getDiscriminantAxis(classifierInfo.exh.bestSelectedInput, DS.machineLearning, k, classifiertype);
 %{
    %%%%% (when the number of best features is more than 1)dimension reduction:
    %%%%% use the best combination found in exhaust
    %%%%% selection to do linear discriminant analysis (LDA)
    DS.exh.best = DS.machineLearning;
    DS.exh.best.input = DS.exh.best.input(classifierInfo.exh.bestSelectedInput,:);
    DS.exh.best.inputName = DS.exh.best.inputName(classifierInfo.exh.bestSelectedInput);
    [DS.exh.lda, DS.exh.lda.discrimVec, DS.exh.lda.eigValues] = lda(DS.exh.best);
    DS.exh.lda.inputName = {};
    for num = 1:length(DS.exh.lda.input(:,1))
        DS.exh.lda.inputName = [DS.exh.lda.inputName, strcat('Input', num2str(num))];
    end
    %%%% use result gained in LDA to do sequential feature selection
    [classifierInfo.exh.bestSelectedInput_lda, classifierInfo.exh.bestRecogRate_lda,...
    classifierInfo.exh.allSelectedInput_lda, classifierInfo.exh.allRecogRate_lda,...
    classifierInfo.exh.elapsedTime_lda] = inputSelectSequential(DS.exh.lda,length(classifierInfo.exh.bestSelectedInput),classifiertype,k,0);
 %}
end
end

