% getExpData    Gather training data information. 
%
% [tree_sub, filelist, clusterNum] = getExpData(Direct_src)
%----------------------------------------------------------
%
% Since users might discard some neuron data while labeling polarity, this
% function counts the number of subtrees and gather the final neuron list serving as training data.
% 
% Input
% -----
% - Direct_src: the directory path of labeled neurons (have been divided into several subtrees)
%
% Output
% ------
% - tree_sub: 1*N structure, where N is the number of neurons. 
% Each cell contains the loaded tree information, including one complete neuron
% (tree_sub{n}{1}, where n is the neuron ID), and several corresponding 
% substructures (tree_sub{n}{2:end}).
% - filelist: neuron name list participating in classifier training.
% - clusterNum: the substructure number of each neuron.
%
% Example
% -------
%  [tree_sub, filelist, clusterNum] = getExpData('./training_neuron')
%

function [tree_sub, filelist, clusterNum] = getExpData(Direct_src)
datalist = dir(strcat(Direct_src,'*.swc'));
filelist_temp = get_norepeat_list(datalist);
filelist = filelist_temp(:,1);
clusterNum = cell2mat(filelist_temp(:,2));
% load tree structure
tree_sub = cell(1,length(filelist));
for data = 1:length(filelist)
	temp_fileName = strcat(filelist{data},'.swc');
	tree_sub{data} = cell(1,clusterNum(data)+1);
	tree_sub{data}{1} = load_tree(strcat(Direct_src,temp_fileName));
	for cluster = 1:clusterNum(data)
		temp_fileName = strcat(filelist{data},'_',num2str(cluster),'.swc');
		tree_sub{data}{1+cluster} = load_tree(strcat(Direct_src,temp_fileName));
	end
end