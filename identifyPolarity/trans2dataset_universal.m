function DS = trans2dataset_universal(data, datasetName, featureName, featureID, identityName)
%TRANS2DATASET_UNIVERSAL transform neuron data to fit the dataset 
% format in machine learning toolbox. "Universal" means this function could
% be applied to both training and test data.

% Input
% -----
% - data: neuron morphological and polarity information
% - datasetName: name of this dataset (optional)
% - featureName: name of all the available features (see featureList.txt)
% - featureID: ID of selected features
% - identityName: name of the types (ex:axon,dendrite) (optional) 
%
% Output
% ------
% - DS: structure variable for storing all the information of a dataset
% -- DS.input:: morphological features
% -- DS.output:: desired classes / ground truth of the dataset
% -- DS.inputName: name of selected features
% -- DS.outputName: name of 
% -- DS.neuronID:: neuron ID of the substructure (i.e. a tag for
% identifying the neuron ID the substructure belongs to)
% -- DS.clusterNum: number of substructure of each neuron
% -- DS.relationship: the spatial relationship among substructures
% -- DS.dataName:: name of the dataset
%
% For more information, please read machine learning toolbox tutorial at:
% http://mirlab.org/jang/books/dcpr/dataSetIntro.asp?title=2-1%20Intro.%20to%20Datasets
%

% normalize the feature to complete neuron (devide by the feature
% calculated in complete neuron)
for iNeuron = 1:length(data)
    data{iNeuron}.feature.sub = data{iNeuron}.feature.sub(featureID, :);
    data{iNeuron}.feature.complete = data{iNeuron}.feature.complete(featureID, :);
    data{iNeuron}.feature_norm = zeros(size(data{iNeuron}.feature.sub)); 
    x = data{iNeuron}.feature.sub;
    y = repmat(data{iNeuron}.feature.complete, 1, length(x(1,:)));
    tempNorm = x./y;
    data{iNeuron}.feature_norm(find(y)) = tempNorm(find(y)); % use zeros to replace inf
end
DS = transDataSet_test(data);
DS.dataName = datasetName;
DS.inputName = featureName(featureID);
DS.outputName = identityName;

% TRANSDATASET_TEST concatenates all the substructure features for further 
% processing
function DS = transDataSet_test(data)
DS = struct;
DS.input = [];
DS.neuronID = [];
DS.clusterNum = zeros(1,length(DS));
if isfield(data{1}, 'identity')
    DS.output = [];
end
if isfield(data, 'relationship')
    DS.relationship = [];
end
for iNeuron = 1:length(data)
    DS.input = [DS.input, data{iNeuron}.feature_norm];
    DS.clusterNum(iNeuron) = data{iNeuron}.clusterNum;
    DS.neuronID = [DS.neuronID, ones(1, data{iNeuron}.clusterNum)*iNeuron];
    if isfield(data{iNeuron}, 'identity')
        DS.output = [DS.output, data{iNeuron}.identity];
    end
    if isfield(data, 'relationship')
        DS.relationship = [DS.relationship, data{iNeuron}.relationship];
    end
end