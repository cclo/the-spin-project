% transDataSet:transform to machinelearning dataset format
% Input:
%   feature_ori: cell array that stores features of each neuron, each cell
%       array is a matrix of the size num_feature*num_substructure
%   identity_ori: cell array that stores identities of each neuron, each
%       cell array is a matrix of size 1*num_substructure
%   featureName: the name of each feature
%   datasetName: the name of this dataset, ex: {'MED_neuron'}
%   identityName: the corresponding name of each index given in
%       identity_ori, ex:{'ori', 'Axon', 'Dendrite'}
% Output:
%   DS: the dataset that match the required format of machineLearning
%       toolbox
% History: included into trans2dataset


function DS = transDataSet(feature_ori, identity_ori, featureName, datasetName, identityName, relationship, clusterNum)
DS = struct;
DS.input = [];
DS.output = [];
DS.relationship = [];
DS.neuronID = [];
for num = 1:length(feature_ori)
    DS.input = [DS.input, feature_ori{1,num}];
    DS.output = [DS.output, identity_ori{1,num}];
    if isempty(relationship)
        continue;
    else
        DS.relationship = [DS.relationship, relationship{num}];
    end
    if isempty(clusterNum)
        continue;
    else
        DS.neuronID = [DS.neuronID, ones(1, clusterNum(num))*num];
    end
end
DS.dataName = datasetName; % the name of the dataset
DS.inputName = featureName;
DS.outputName = identityName;% a cell string that represents the name of the output classes(correspond to the index of dataset.output)
