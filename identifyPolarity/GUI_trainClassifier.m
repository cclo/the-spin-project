%% Train neuronal polarity classifier 
% This script extracts morphology features to train neuronal polarity
% classifier.

%% User setting
neuropil = 'example';
option.featureExtraction = 1;% perform feature extraction or not 
k = 1; %the number of neighbors for performing k-nearest-neighbor classifier 
option.featureSelection = 1; 
option.featureSelection_seq = 1;
option.featureSelection_exh = 1;
direc.src.expData = './example';% the dir where experimental data save
featureIgnored = 12:14;
%-----------------------optional changes--------------------------
tarDirectory = 'Classifier';
direc.tar.classifier = strcat('./', tarDirectory, '/',neuropil,'/');% save classifier information
fileName.features = 'features.mat'; % the filename used to save classifier information
fileName.featureSelection = 'featureSelectionResult.mat';
fileName.classifier = 'classifierInfo.mat';
if ~isdir(direc.tar.classifier)
    mkdir(direc.tar.classifier);
end
%% Feature extraction
% Get tree structure, feature data
if option.featureExtraction
    % Readfile
    [tree_sub, trainingfilelist, clusterNum] = getExpData(direc.src.expData);
    % Extract features
    feature_training = featureExtraction_training(tree_sub, clusterNum);
    % Save result
	save(strcat(direc.tar.classifier,fileName.features), 'feature_training', 'trainingfilelist');
    % Output training neuron list 
    fid = fopen(strcat(direc.tar.classifier,neuropil,'_filelist_training.txt'),'w');
    for num = 1:length(trainingfilelist)
        fprintf(fid, '%s\n',trainingfilelist{num});
    end
else
	load(strcat(direc.tar.classifier,fileName.features)) ;
end
%% Feature selection
% Not all the features are suitable for identifying neuronal polarities.
% Threrfore, we use K-nearest classifier to search the best feature
% combination to train our classifier. 
%
% Reference:
% <http://neural.cs.nthu.edu.tw/jang/books/dcpr/fsMethod.asp?title=10-2%20Feature%20Selection%20Methods%20(%AFS%BCx%BF%EF%A8%FA%A4%E8%AAk)
% Machine Learning Toolbox (MLT)>
if option.featureSelection
    feature_training.feature.ignore = featureIgnored;
    % transform to dataset format
    datasetName = strcat(neuropil,'_neuron');
    identityName = {'Undefines', 'Soma', 'Axon', 'Dendrite'};
    DS.ori = trans2dataset(feature_training, datasetName, identityName, []);
    % select good features combination
    [featureSelectionInfo, DS] = featureSelection(DS, direc.tar.classifier, option.featureSelection_seq, option.featureSelection_exh, k);
    featureSelectionInfo.featureIgnored = feature_training.feature.ignore;
    save(strcat(direc.tar.classifier, fileName.featureSelection), 'featureSelectionInfo', 'DS');
else
	load(strcat(direc.tar.classifier,fileName.featureSelection)) ;
end
%%  Generate discriminant axis from classifierInfo
% Use the features achieving highest recognition rate in "feature
% selection" to train discriminant axis by linear discriminant analysis (LDA)

% 1)sequential 
if isfield(featureSelectionInfo, 'seq')
    classifier.seq.bestFeature = featureSelectionInfo.seq.bestSelectedInput;
    [~, bestLDA] = max(featureSelectionInfo.seq.allRecogRate_lda(1:length(classifier.seq.bestFeature)));
    classifier.seq.identifyInfo = sumUpClassifierInfo(DS.seq.lda, bestLDA);
    classifier.seq.weight = DS.seq.lda.discrimVec(:, bestLDA)';
    classifier.seq.featureIgnored = featureSelectionInfo.featureIgnored;
    save(strcat(direc.tar.classifier, fileName.classifier), 'classifier');
end
% 2)exhaustive
if isfield(featureSelectionInfo, 'exh')
    classifier.exh.bestFeature = featureSelectionInfo.exh.bestSelectedInput;
    [~, bestLDA] = max(featureSelectionInfo.exh.allRecogRate_lda(1:length(classifier.exh.bestFeature)));
    classifier.exh.identifyInfo = sumUpClassifierInfo(DS.exh.lda, bestLDA);
    classifier.exh.weight = DS.exh.lda.discrimVec(:, bestLDA)';
    classifier.exh.featureIgnored = featureSelectionInfo.featureIgnored;
    save(strcat(direc.tar.classifier, fileName.classifier), 'classifier');
end
%% Dataset examination
% Examine dataset proterties
checkFeatureDist(DS.ori, direc.tar.classifier, feature_training.clusterNum);
%% Classifier training results (sequential)
% Summarize feature selection, discriminnt axis training results 
if isfield(featureSelectionInfo, 'seq')
    % recover to the original ID
    ID_ori = zeros(1,length(feature_training.featureName));
    ID_ori(featureSelectionInfo.featureIgnored) = -1;
    used = find(ID_ori == 0);
    ID_ori(used(featureSelectionInfo.seq.bestSelectedInput)) = 1;
    oriID4selectedFeature = find(ID_ori==1);
    % display
    disp(['Best feature combination (sequential):', num2str(oriID4selectedFeature)]);
    disp(['Best recognition rate :', num2str(featureSelectionInfo.seq.bestRecogRate*100), '%']);
    disp(['Discriminant axis component weight (sequential):', num2str(classifier.seq.weight)]);
    disp('Discriminant axis recognition rate :'); 
    plotTrainingResult(DS.seq, featureSelectionInfo.seq, direc.tar.classifier, feature_training.clusterNum, 'sequential');
else
    disp('Have not performed sequential feature selection')
end
%% Classifier training results (exhaustive)
% Summarize feature selection, discriminnt axis training results 
if isfield(featureSelectionInfo, 'exh')
    % recover to the original ID
    ID_ori = zeros(1,length(feature_training.featureName));
    ID_ori(featureSelectionInfo.featureIgnored) = -1;
    used = find(ID_ori == 0);
    ID_ori(used(featureSelectionInfo.exh.bestSelectedInput)) = 1;
    oriID4selectedFeature = find(ID_ori==1);
    % display
    disp(['Best feature combination (exhaustive):', num2str(oriID4selectedFeature)]);
    disp(['Feature weight (exhaustive):', num2str(classifier.exh.weight)]);
    disp(['Best recognition rate (exhaustive):', num2str(featureSelectionInfo.exh.bestRecogRate*100), '%']);
    plotTrainingResult(DS.exh, featureSelectionInfo.exh, direc.tar.classifier, feature_training.clusterNum, 'exhaustive');
else
    disp('Have not performed exhaustive feature selection')
end


