% This program identifies neuronal polarity
%
% See also:
% clean_improbableBranch, morphologyClustering, featureExtraction,
% trans2dataset_universal, judgePolarity, giveWarning,
% labelPolarity_modify, calTerminalAccuracy, plotClassificationResult.

%% Setting path, directories
%{
% Include needed path
addpath('./GroundTruth_uncleaned/MED_testing/',...
        './morphological_clustering/',...
        './identifyPolarity/',...
        './polarity_classification/',...
        './toolbox/TREES1.14/',...
        './toolbox/machineLearning/');
start_trees
% construct needed directories
directories = fieldnames(direc.tar); 
for num = 1:numel(directories) 
    if ~isdir(getfield(direc.tar, directories{num}))  
        mkdir(getfield(direc.tar, directories{num}));
    end
end

%}
%{
% User setting (get input from GUI)
classifierName = 'MED';
testingDataName = 'example';
direc.src.fileList = 'E:\Dropbox\Lab\SPIN_improve\toolbox\the-spin-project\fileList.txt';
direc.src.swcfile = 'E:\Dropbox\Lab\SPIN_improve\toolbox\the-spin-project\SWC_labeled\';
classifierType = 'seq';
option.defaultParameter = 1;
option.calAccuracy = 1; % swc file contains answer or not
%------------temp
nowPosition = 'E:\Dropbox\Lab\SPIN_improve\toolbox\the-spin-project';
direc.src.classifier = strcat(nowPosition, '/Classifier/',classifierName, '/classifierInfo.mat');
direc.tar.cleanedPlot = strcat(nowPosition, '/Result/', testingDataName, '/cleanedTreePlot/');
direc.tar.morphoClustPlot = strcat(nowPosition, '/Result/', testingDataName, '/morphoClustPlot/');
direc.tar.classifiedResultPlot = strcat(nowPosition, '/Result/', testingDataName, '/classifiedResultPlot/');
direc.tar.classifiedResultSWC = strcat(nowPosition, '/Result/', testingDataName, '/classifiedResultSWC/');
direc.tar.classifiedResultMat = strcat(nowPosition, '/Result/', testingDataName, '/classifiedResultMat/');
direc.record.all = strcat(nowPosition, '/Result/', testingDataName, '/resultRecord_',testingDataName, '.txt');
direc.record.metadata = strcat(nowPosition, '/Result/', testingDataName, '/metaData_',testingDataName, '.txt');
%}
% --------------------------optional changes-------------------------
direc.src.classifier = strcat('./Classifier/',classifierName, '/classifierInfo.mat');
direc.src.featureFile = 'featureList.txt';
direc.tar.cleanedPlot = strcat('./Result/', testingDataName, '/cleanedTreePlot/');
direc.tar.morphoClustPlot = strcat('./Result/', testingDataName, '/morphoClustPlot/');
direc.tar.classifiedResultPlot = strcat('./Result/', testingDataName, '/classifiedResultPlot/');
direc.tar.classifiedResultSWC = strcat('./Result/', testingDataName, '/classifiedResultSWC/');
direc.tar.classifiedResultMat = strcat('./Result/', testingDataName, '/classifiedResultMat/');
direc.record.all = strcat('./Result/', testingDataName, '/resultRecord_',testingDataName, '.txt');
direc.record.metadata = strcat('./Result/', testingDataName, '/metaData_',testingDataName, '.txt');
direc.record.log = strcat('./Result/', testingDataName, '/logFile.txt');

% output option
output.cleanedPlot = 1;
output.morphoClustPlot = 1;
output.classifiedResultPlot = 1;
output.classifiedResultSWC = 1;
output.classifiedResultMat = 1;
errorName = {};
errorMsg = {};
%% Setting parameters
if option.defaultParameter
    % 1 clean tree
    morphoClust.clean_tree.times = 3; % how many iteration you run to get the trunk
    morphoClust.clean_tree.deleteTerminal = 0.37;  % the percentage of the length which will be deleted
    morphoClust.clean_tree.drawOption = false;
    % 2 critical point
    morphoClust.cp.threshold = 0.35; % define the proportion of critical point gap
    morphoClust.cp.preserve = 0.85; % the number of terminal point following the labele point
end

% construct needed directories
directories = fieldnames(direc.tar); 
for num = 1:numel(directories) 
    if ~isdir(getfield(direc.tar, directories{num})) && getfield(output, directories{num}) 
        mkdir(getfield(direc.tar, directories{num}));
    end
end
% open an empty file to record identified results
fid = fopen(direc.record.all, 'w');
clear ('tree', 'tree_sub', 'filelist');
%%  Poalrity identification
% read neuron names
fid_neuronNames = fopen(direc.src.fileList); filelist = textscan(fid_neuronNames, '%s%*s%*f%*f%*f'); fclose(fid_neuronNames);
% read feature list
fid_features = fopen(direc.src.featureFile); featureList = textscan(fid_features, '%s'); fclose(fid_features);
% open file for recording error
fid_log = fopen(direc.record.log,'w'); 
% load discriminant info
load(direc.src.classifier);
classifier = getfield(classifier, classifierType);
wait_h = waitbar(0, 'Please wait...', 'Name', 'Identify polarity');
for num = 1:length(filelist{1,1})
    tic;
    waitbar(num / length(filelist{1,1}));
    fileName = strcat(direc.src.swcfile,filelist{1,1}{num}, '.swc');
    fprintf('%d\t%s\n', num, filelist{1,1}{num});
    %fileName = 'ChaMARCM-F000374_seg002.swc';
    try
        tree{1,1} = load_tree(fileName);
        % 4 decide cleaned segment parameters by terminal point number
        % (numTp)
        numTp = length(find(T_tree(tree{1,1})));
        if option.defaultParameter
            if numTp <= 50
                morphoClust.cleanSeg.scanLength_par_up = 0.1;%branchLength/pathLengthMax;  MED:0.14;PCB:0.12
                morphoClust.cleanSeg.scanLength_par_low = 0.1;%branLengthMean/pathLengthMax;   MED:0.12;PCB:0.1
                morphoClust.cleanSeg.scanLength_delta = 0.005;
                morphoClust.cleanSeg.terminalRatio = 0; % the lower terminalRatio makes it more difficult to label the current node
                morphoClust.cleanSeg.numTerminalThreshold = 0;% MED:0.05; PCB:0.01
                % 5 morphology clustering 
                morphoClust.numTerminalThreshold = 0.01;% MED:0.08; PCB:0.01
            else
                morphoClust.cleanSeg.scanLength_par_up = 0.12;%branchLength/pathLengthMax;  MED:0.14;PCB:0.12
                morphoClust.cleanSeg.scanLength_par_low = 0.12;%branLengthMean/pathLengthMax;   MED:0.12;PCB:0.1
                morphoClust.cleanSeg.scanLength_delta = 0.005;
                morphoClust.cleanSeg.terminalRatio = 0; % the lower terminalRatio makes it more difficult to label the current node
                morphoClust.cleanSeg.numTerminalThreshold = 0;% MED:0.05; PCB:0.01
                 % 5 morphology clustering 
                morphoClust.numTerminalThreshold = 0.08;% MED:0.08; PCB:0.01
            end
        end

        % Clean improbable tree
        [tree{1,2} tree_trunk h] = clean_improbableBranch(tree{1,1}, morphoClust.clean_tree, output.cleanedPlot); 
        if output.cleanedPlot
            saveas(h, strcat(direc.tar.cleanedPlot,tree{1,1}.name,'_cleaned'), 'fig');
            close(h);
        end

        % Morphological clustering
        [tree_sub relationship h] = morphologyClustering(tree, morphoClust, output.morphoClustPlot);
        if output.morphoClustPlot
            saveas(h, strcat(direc.tar.morphoClustPlot,tree{1,1}.name,'_morphoClust'), 'fig');
            close(h);
        end
        
        % Feature extraction
        [morphoInfo tree_sub] = featureExtraction(tree_sub, featureList{1}, classifier.bestFeature, relationship);

        % polarity classification
        rawdata = trans2dataset_universal({morphoInfo}, [], featureList{1}, classifier.bestFeature, []);
        [morphoInfo.ldaScore morphoInfo.output] = judgePolarity(rawdata.input, classifier.weight, classifier.identifyInfo.identityOrder);
        
        % give warning
        morphoInfo = giveWarning(morphoInfo, classifier.identifyInfo);
        
        % plot/output result 
        % encode warning type into swc: the first number indicates
        % polarity, while the second number indicates warning type (3 means
        % this node is issued both warning type I and II). For
        % example, 21 means this node is identified as axon but issued type
        % I warning. 
        structureInx_withWarning = morphoInfo.output*10 + morphoInfo.resultEval.warning + morphoInfo.resultEval.warning_blackSheep.*2; 
        %structureInx_withWarning = morphoInfo.output.*((-1).^morphoInfo.resultEval.warning);
        tree_sub = labelPolarity_modify(structureInx_withWarning, tree_sub, morphoInfo.del_data); %this version label warning in swc files by giving negative index
        accuracy = calTerminalAccuracy(tree(1,1), tree_sub(1,1));
        if output.classifiedResultSWC
            swc_tree(tree_sub{1,1}, strcat(direc.tar.classifiedResultSWC,tree_sub{1,1}.name,'.swc'));
            % also export substructures
            for iSub = 1:length(tree_sub)-1
                swc_tree(tree_sub{1,iSub+1}, strcat(direc.tar.classifiedResultSWC,tree_sub{1,1}.name, '_', num2str(iSub), '.swc'));
            end
        end
        if output.classifiedResultPlot
            if option.calAccuracy
                h = plotClassificationResult(tree_sub, tree{1,1},1,1);
                save(strcat(direc.tar.classifiedResultMat,tree_sub{1,1}.name,'.mat'), 'tree_sub', 'tree');
            else
                h = plotClassificationResult(tree_sub, [], 1,1);
                save(strcat(direc.tar.classifiedResultMat,tree_sub{1,1}.name,'.mat'), 'tree_sub');
            end
            saveas(h, strcat(direc.tar.classifiedResultPlot,tree{1,1}.name), 'fig');
            close(h);
        end      
    % record
        warningType = morphoInfo.resultEval.warning_dist + morphoInfo.resultEval.warning_blackSheep*2;
        nlength = size(warningType,2);
        warningType_str = num2str(warningType);
        warningType_str(isspace(warningType_str)) = [];
        if option.calAccuracy          
            fprintf(fid, '%s\t%s\t%f\n', filelist{1,1}{num}, warningType_str, accuracy);
        else
            fprintf(fid, '%s\t%s\n', filelist{1,1}{num}, warningType_str);
        end
        %{
        if option.calAccuracy           
            stringFormat = ['%s\t%f\t\t', repmat('%d\t',1,nlength), '\n'];
            fprintf(fid, stringFormat, filelist{1,1}{num}, accuracy, warningType);
        else
            stringFormat = ['%s\t\t', repmat('%d\t',1,nlength), '\n'];
            fprintf(fid, stringFormat, filelist{1,1}{num}, warningType);
        end
        %}
    catch err
        fprintf(fid, '%s\t%s\n', filelist{1,1}{num}, 'error');
        fprintf(fid_log, '%s\n%s\n', filelist{1,1}{num}, getReport(err, 'extended'));
        save(strcat(direc.tar.classifiedResultMat,filelist{1,1}{num},'_all.mat'));
        errorName = [errorName; filelist{1,1}{num}];
        errorMsg = [errorMsg; getReport(err, 'extended')];
        save(fullfile('Result', testingDataName, 'errorList.mat'), 'errorName', 'errorMsg');
    end 
    save(strcat(direc.tar.classifiedResultMat,filelist{1,1}{num},'_all.mat'));
    elapseTime = toc;
end    
close(wait_h);
% export setting
fid_metadata = fopen(direc.record.metadata, 'w');
fprintf(fid_metadata, '%s%s\n','date = ', date ); %the position of swc files
fprintf(fid_metadata, '%s%s\n','swc directory = ',direc.src.swcfile ); %the position of swc files
fprintf(fid_metadata, '%s%s\n','classifier directory = ',direc.src.classifier ); %the position of clasifier
fprintf(fid_metadata, '%s%s\n','classifier type = ',classifierType ); %classifier type  
% six tunable parameters
if ~option.defaultParameter
    fprintf(fid_metadata, 'n_CleanTimes = %d\n', morphoClust.clean_tree.times); 
    fprintf(fid_metadata, 'Th_RemoveLen = %f\n', morphoClust.clean_tree.deleteTerminal);
    fprintf(fid_metadata, 'Th_LongBranch = %f\n', morphoClust.cleanSeg.scanLength_par_up);  
    fprintf(fid_metadata, 'Th_CritP = %f\n', morphoClust.cp.threshold);
    fprintf(fid_metadata, 'Th_numTP = %f\n', morphoClust.cp.preserve);
    fprintf(fid_metadata, 'Th_DP = %f\n',  morphoClust.numTerminalThreshold);
else
    fprintf(fid_metadata, 'n_CleanTimes = %d\n', morphoClust.clean_tree.times); 
    fprintf(fid_metadata, 'Th_RemoveLen = %f\n', morphoClust.clean_tree.deleteTerminal);
    fprintf(fid_metadata, 'Th_CritP = %f\n', morphoClust.cp.threshold);
    fprintf(fid_metadata, 'Th_numTP = %f\n', morphoClust.cp.preserve);
    fprintf(fid_metadata, 'If number of terminal points < = 50 \n');
    fprintf(fid_metadata, 'Th_LongBranch = %f\n', 0.1);
    fprintf(fid_metadata, 'Th_DP = %f\n',  0.01);
    fprintf(fid_metadata, 'If number of terminal points > 50 \n');
    fprintf(fid_metadata, 'Th_LongBranch = %f\n', 0.12);
    fprintf(fid_metadata, 'Th_DP = %f\n',  0.08);
end
fclose('all');    
   

