function [DS_best DS_lda classifierInfo] = getDiscriminantAxis(bestSelect, classifierInfo, DS, k, classifiertype)
    %%%%% dimension reduction (when the number of best features is more than 1)
    %%%%% use the best combination found in exhaust
    %%%%% selection to do linear discriminant analysis (LDA)
    DS_best = DS;
    if ~isempty(bestSelect)
        DS_best.input = DS_best.input(bestSelect,:);
        DS_best.inputName = DS_best.inputName(bestSelect);
    end
    [DS_lda, DS_lda.discrimVec, DS_lda.eigValues] = lda(DS_best);
    DS_lda.inputName = {};
    for num = 1:length(DS_lda.input(:,1))
        DS_lda.inputName = [DS_lda.inputName, strcat('Input', num2str(num))];
    end
    %%%%% use result gained in LDA to do sequential feature selection
    [classifierInfo.bestSelectedInput_lda, classifierInfo.bestRecogRate_lda,...
     classifierInfo.allSelectedInput_lda, classifierInfo.allRecogRate_lda,...
     classifierInfo.elapsedTime_lda] = inputSelectSequential(DS_lda,length(DS_best.input(:,1)),classifiertype,k,0);
end
    