%% Train neuronal polarity classifier 
% This script extracts morphology features to train neuronal polarity
% classifier.

%% User setting
%{
datasetName = 'example';
option.featureExtraction = 0;% perform feature extraction or not 
k = 1; %the number of neighbors for performing k-nearest-neighbor classifier 
option.featureSelection = 1; 
option.featureSelection_seq = 1;
option.featureSelection_exh = 1;
direc.src.expData = 'E:\Dropbox\Lab\SPIN_improve\toolbox\the-spin-project\SWC_labeled\';% the dir where experimental data save
featureIgnored = [12:14, 21:23];%12:14;
featureAssigned = 19; %[1 3 5 7];% add this option in GUI-----------------------
%}
%-----------------------optional changes--------------------------
tarDirectory = 'Classifier';
direc.tar.classifier = strcat('./', tarDirectory, '/',datasetName,'/');% save classifier information
fileName.features = 'features.mat'; % the filename used to save classifier information
fileName.featureSelection = 'featureSelectionResult.mat';
fileName.classifier = 'classifierInfo.mat';
fileName.featureList = 'featureList.txt';
identityName = {'Undefines', 'Soma', 'Axon', 'Dendrite'}; %-----could be further generalized to other classifying applications 
classifiertype = 'knncLoo'; % assign classifier type
if ~isdir(direc.tar.classifier)
    mkdir(direc.tar.classifier);
end
%% Feature extraction
% Get tree structure, feature data
if option.featureExtraction
    % Readfile
    [tree_sub, trainingfilelist, clusterNum] = getExpData(direc.src.expData);
    % Export training neuron list
    fid = fopen(strcat(direc.tar.classifier,datasetName,'_filelist_training.txt'),'w');
    for num = 1:length(trainingfilelist)
        fprintf(fid, '%s\n',trainingfilelist{num});
    end
    fclose(fid);
    % Read featureList
    fid_feature = fopen(fileName.featureList);
    featureList.name = textscan(fid_feature, '%s');
    fclose(fid_feature);
    % Get feature list for different conditions
    featureList.machineLearning = setdiff(1:length(featureList.name{1}), featureIgnored); % remove ignored features
    featureList.assign = featureAssigned;
    featureList.needed = union(featureList.machineLearning, featureList.assign); 
    % Extract features
    feature_training = cell(1, length(trainingfilelist));
    for iNeuron = 1:length(trainingfilelist)
        disp(iNeuron);
        [feature_training{iNeuron} tree_sub{iNeuron}] = featureExtraction(tree_sub{iNeuron}, featureList.name{1}, featureList.needed, []);
    end
    % Save result
	save(strcat(direc.tar.classifier,fileName.features), 'feature_training', 'trainingfilelist', 'featureList', 'tree_sub');    
else
	load(strcat(direc.tar.classifier,fileName.features)) ;
end
%% Feature selection
% Not all the features are suitable for identifying neuronal polarities.
% Threrfore, we use K-nearest classifier to search the best feature
% combination to train our classifier. 
%
% Reference:
% <http://neural.cs.nthu.edu.tw/jang/books/dcpr/fsMethod.asp?title=10-2%20Feature%20Selection%20Methods%20(%AFS%BCx%BF%EF%A8%FA%A4%E8%AAk)
% Machine Learning Toolbox (MLT)>
if option.featureSelection
    % transform to dataset format
    DS.machineLearning = trans2dataset_universal(feature_training, datasetName, featureList.name{1}, featureList.machineLearning, identityName);
    if option.featureSelection_seq
        disp( 'Perform sequential feature selection:');
        inputNum = length(DS.machineLearning.input(:,1)); 
        % sequential feature selection
        [featureSelectionInfo.seq.bestSelectedInput, featureSelectionInfo.seq.bestRecogRate,...
         featureSelectionInfo.seq.allSelectedInput, featureSelectionInfo.seq.allRecogRate,...
         featureSelectionInfo.seq.elapsedTime] = inputSelectSequential(DS.machineLearning,inputNum,classifiertype,k,0);
        [DS.seq.best DS.seq.lda featureSelectionInfo.seq] = getDiscriminantAxis(featureSelectionInfo.seq.bestSelectedInput, featureSelectionInfo.seq, DS.machineLearning, k, classifiertype);
    end
    if option.featureSelection_exh
        disp( 'Perform exhaustive feature selection:');
        inputNum = length(DS.machineLearning.input(:,1)); 
        % exhaustive feature selection
        [featureSelectionInfo.exh.bestSelectedInput, featureSelectionInfo.exh.bestRecogRate,...
        featureSelectionInfo.exh.allSelectedInput, featureSelectionInfo.exh.allRecogRate,...
        featureSelectionInfo.exh.elapsedTime] = inputSelectExhaustive(DS.machineLearning,inputNum,classifiertype,k,0);
        [DS.exh.best DS.exh.lda featureSelectionInfo.exh] = getDiscriminantAxis(featureSelectionInfo.exh.bestSelectedInput, featureSelectionInfo.exh, DS.machineLearning, k, classifiertype);
    end
    save(strcat(direc.tar.classifier, fileName.featureSelection), 'featureSelectionInfo', 'DS');
else
	load(strcat(direc.tar.classifier,fileName.featureSelection)) ;
end
if ~isempty(featureList.assign)
    DS.assign = trans2dataset_universal(feature_training, datasetName, featureList.name{1}, featureList.assign, identityName);
    featureSelectionInfo.assign = [];
    [DS.assign DS.assign.lda featureSelectionInfo.assign] = getDiscriminantAxis([], featureSelectionInfo.assign, DS.assign, k, classifiertype);
    save(strcat(direc.tar.classifier, fileName.featureSelection), 'featureSelectionInfo', 'DS');
end
%%  Generate discriminant axis from classifierInfo
% Use the features achieving highest recognition rate in "feature
% selection" to train discriminant axis by linear discriminant analysis (LDA)

% 1)sequential 
if isfield(featureSelectionInfo, 'seq')
    classifier.seq.bestFeature = featureList.machineLearning(featureSelectionInfo.seq.bestSelectedInput);
    [temp, bestLDA] = max(featureSelectionInfo.seq.allRecogRate_lda(1:length(classifier.seq.bestFeature)));
    classifier.seq.identifyInfo = sumUpClassifierInfo(DS.seq.lda, bestLDA);
    classifier.seq.weight = DS.seq.lda.discrimVec(:, bestLDA)';
    save(strcat(direc.tar.classifier, fileName.classifier), 'classifier');
end
% 2)exhaustive
if isfield(featureSelectionInfo, 'exh')
    classifier.exh.bestFeature = featureList.machineLearning(featureSelectionInfo.exh.bestSelectedInput);
    [temp, bestLDA] = max(featureSelectionInfo.exh.allRecogRate_lda(1:length(classifier.exh.bestFeature)));
    classifier.exh.identifyInfo = sumUpClassifierInfo(DS.exh.lda, bestLDA);
    classifier.exh.weight = DS.exh.lda.discrimVec(:, bestLDA)';
    save(strcat(direc.tar.classifier, fileName.classifier), 'classifier');
end
% 3)assign
if isfield(featureSelectionInfo, 'assign')
    classifier.assign.bestFeature = featureList.assign;
    [temp, bestLDA] = max(featureSelectionInfo.assign.allRecogRate_lda(1:length(classifier.assign.bestFeature)));
    classifier.assign.identifyInfo = sumUpClassifierInfo(DS.assign.lda, bestLDA);
    classifier.assign.weight = DS.assign.lda.discrimVec(:, bestLDA)';
    save(strcat(direc.tar.classifier, fileName.classifier), 'classifier');
end
%% Dataset examination
% Examine dataset proterties
DS.needed = trans2dataset_universal(feature_training, datasetName, featureList.name{1}, featureList.needed, identityName);
checkFeatureDist(DS.needed, direc.tar.classifier, clusterNum);
%% Classifier training results (sequential)
% Summarize feature selection, discriminnt axis training results 
if isfield(featureSelectionInfo, 'seq')
    featureSelectType = 'sequential';
    % display
    disp(['Best feature combination (sequential):', num2str(classifier.seq.bestFeature)]);
    disp(['Best recognition rate :', num2str(featureSelectionInfo.seq.bestRecogRate*100), '%']);
    disp(['Discriminant axis component weight (sequential):', num2str(classifier.seq.weight)]);
    disp('Discriminant axis recognition rate :');
    % plot first 3 best feature distribution in 3D
    plotFeatureIn3D(DS.seq.best, featureSelectionInfo.seq);
    saveas(gcf, strcat(direc.tar.classifier, strcat('Feature_space_best3_',featureSelectType)), 'fig');
    % plot discriminant axis distribution
    plotDiscriminantAxisDist(DS.seq.lda)
    saveas(gcf, strcat(direc.tar.classifier, strcat('bestlda_distribution_group_',featureSelectType)), 'fig');
    % plot discriminant axis distribution (pair)
    plotDiscriminantAxisPair(DS.seq.lda, featureSelectionInfo.seq)  
    saveas(gcf, strcat(direc.tar.classifier, strcat('bestlda_distribution_pair_',featureSelectType)), 'fig');
else
    disp('Have not performed sequential feature selection')
end
%% Classifier training results (exhaustive)
% Summarize feature selection, discriminnt axis training results 
if isfield(featureSelectionInfo, 'exh')
    featureSelectType = 'exhaustive';
    % display
    disp(['Best feature combination (exhaustive):', num2str(classifier.exh.bestFeature)]);
    disp(['Feature weight (exhaustive):', num2str(classifier.exh.weight)]);
    disp(['Best recognition rate (exhaustive):', num2str(featureSelectionInfo.exh.bestRecogRate*100), '%']);
    % plot first 3 best feature distribution in 3D
    plotFeatureIn3D(DS.exh.best, featureSelectionInfo.exh);
    saveas(gcf, strcat(direc.tar.classifier, strcat('Feature_space_best3_',featureSelectType)), 'fig');
    % plot discriminant axis distribution
    plotDiscriminantAxisDist(DS.exh.lda)
    saveas(gcf, strcat(direc.tar.classifier, strcat('bestlda_distribution_group_',featureSelectType)), 'fig');
    % plot discriminant axis distribution (pair)
    plotDiscriminantAxisPair(DS.exh.lda, featureSelectionInfo.exh)  
    saveas(gcf, strcat(direc.tar.classifier, strcat('bestlda_distribution_pair_',featureSelectType)), 'fig');
else
    disp('Have not performed exhaustive feature selection')
end
%% Classifier training results (assign)
if isfield(featureSelectionInfo, 'assign')
    featureSelectType = 'assign';
    % display
    disp(['Best feature combination (assign):', num2str(classifier.assign.bestFeature)]);
    disp(['Feature weight (assign):', num2str(classifier.assign.weight)]);
    disp(['Best recognition rate (assign):', num2str(featureSelectionInfo.assign.bestRecogRate_lda*100), '%']);
    % plot discriminant axis distribution
    plotDiscriminantAxisDist(DS.assign.lda)
    saveas(gcf, strcat(direc.tar.classifier, strcat('bestlda_distribution_group_',featureSelectType)), 'fig');
    % plot discriminant axis distribution (pair)
    plotDiscriminantAxisPair(DS.assign.lda, featureSelectionInfo.assign)  
    saveas(gcf, strcat(direc.tar.classifier, strcat('bestlda_distribution_pair_',featureSelectType)), 'fig');
else
    disp('No assigned feature')
end


