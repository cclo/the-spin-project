% This program extract features from neurons
function [data tree_sub] = featureExtraction_testing(tree_sub,relationship)
clusterNum = length(tree_sub) -1;
% get partial morphological feature      
data.feature.par = stats_tree(tree_sub,[],[],'none');

% if the substructure only have 2 branches, set the hull = 0
%data.feature.par.gstats.hull(isnan(data.feature.par.gstats.hull)) = 0;

% get global morphological feature
data.feature.glob = stats_glob(tree_sub);

% Reorganize the feature and identity(delete data with NaN)
data.featureName = [fieldnames(data.feature.par.gstats)', fieldnames(data.feature.glob)'];
temp_feature = [structure_content(data.feature.par.gstats); structure_content(data.feature.glob)];
[r c] = find(isnan(temp_feature));
data.del_data = unique(c)';
temp_feature(:,data.del_data) = [];
data.feature.ori = real(temp_feature(:,2:end));
data.feature.complete = real(temp_feature(:,1));
data.clusterNum = clusterNum - length(data.del_data);

% rearrange the sub tree order (move the deleted substructures to the
% end)
temp_relationship = relationship(data.del_data);
relationship(data.del_data) = [];
data.relationship = [relationship temp_relationship];

temp_sub = tree_sub(data.del_data);
tree_sub(data.del_data) = [];
tree_sub = [tree_sub temp_sub];
data.del_data = (length(tree_sub)-length(data.del_data)+1) : length(tree_sub);
