% GOMANUALDENOISE is a GUI for manually clean skeleton data noise.
%goManualDenoise will read neuron in 'neuronList.txt' file 
% and call ManualDenoiseGUI function. Below is information of buttons.
%	previous:
%		draw previous neuron if it is not the first neuron.
%	next:
%		draw next neuron if it is not the last neuron.
%	clean:
%		clean unwanted branches which has beeb identified as noise manually.
%   replot:
%       reset a neuron to uncleaned state
%	export:
%		export the cleaned neuron in swc format
%   exportAll:
%       skip manual denoise and copy all the files to "SWC_cleaned" for
%       further processing
%
%   See tutorial in SPIN_OnlineResource_v0_1_2.pdf 
%   See also GUI_ManualDenoise
%
%	---------
%	Author : Yi-Hsuan Lee, Yen-Nan Lin
%	Created: 2011-05-18, using mac Matlab R2009a. Develop for manually
%	label polarity.
%   Hostory: 
%   2012-10-21: modified as manually denoise GUI tool for skeleton 
%   neuron ,using windows7 Matlab R2012a

clear all; close all;
figure('Position', [700, 150, 250, 500]);
set(gcf, 'tag', 'HandLabel');
uicontrol('Style', 'pushbutton', 'String', 'Previous', 'Position', [50 400 150 50], ...
	'Callback', 'GUI_ManualDenoise(''previous'')' );
uicontrol('Style', 'pushbutton', 'String', 'Next', 'Position', [50 350 150 50], ...
	'Callback', 'GUI_ManualDenoise(''next'')' );
uicontrol('Style', 'pushbutton', 'String', 'Clean', 'Position', [50 250 150 50], ...
	'Callback', 'GUI_ManualDenoise(''clean'')' );
uicontrol('Style', 'pushbutton', 'String', 'Replot', 'Position', [50 200 150 50], ...
	'Callback', 'GUI_ManualDenoise(''replot'')' );
uicontrol('Style', 'pushbutton', 'String', 'Export', 'Position', [50 100 150 50], ...
	'Callback', 'GUI_ManualDenoise(''export'')' );
uicontrol('Style', 'pushbutton', 'String', 'Export all', 'Position', [50 50 150 50], ...
	'Callback', 'GUI_ManualDenoise(''exportAll'')' );
GUI_ManualDenoise('initialize');
