function [parent] =plot_HierarchicalTree(tree)
adjMatrix = tree.dA;
node_ID = (1:length(adjMatrix))';
parent = adjMatrix*node_ID;
parent = parent';
figure('Tag','tree_plot');
treeplot(parent);
figure('Tag','morpho_plot');
plot_tree(tree);
hold on;
plot3(tree.X(1),tree.Y(1),tree.Z(1),'.','color','r','markersize',20);
grid on;
hold off;
end