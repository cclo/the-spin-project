% k_ind gives group index for k-fold cross validation
% input: 
%	list:the list you want to assign index
%	k_fold:the number of group you want
% output:
%	group_list:the index for each element in list
function group_list = k_ind(list,k_fold)
order = randperm(length(list));
group_list = zeros(1,length(order));
for num = 1:length(order)
	if mod(order(num),k_fold) == 0
		group_list(num) = k_fold;
	else
		group_list(num) = mod(order(num),k_fold);
	end
end