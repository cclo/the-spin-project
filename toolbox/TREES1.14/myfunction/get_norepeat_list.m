% get_norepeat_list extract no repeat filename list of a given dir content
function [no_repeat_list] = get_norepeat_list(list)
no_repeat_list = {};
for num = 1:length(list)
	% skip'.' and '..'
	if strcmp(list(num).name(1), '.')
		continue;
	end
	% extract name to compare
	tempname = list(num).name(1:length(list(num).name)-4);
	if isempty(no_repeat_list)
		c = 0;
		no_repeat_list = [no_repeat_list tempname c];
		continue;
	end
	if findstr(no_repeat_list{length(no_repeat_list)-1},tempname)
		c = c+1;
		no_repeat_list{length(no_repeat_list)} = c;
		continue;
	end
	c = 0;
	no_repeat_list = [no_repeat_list tempname c];
end
no_repeat_list = (reshape(no_repeat_list,2,length(no_repeat_list)/2))';