% Strucutre_content extract every content in the structure (But the content
% type need to be single type,i.e. all number or all string.
% Input: structure you want to extract 
% Output: structure content
% Example :
%	a structure "Student_A" have several field : studentName, subject, score
%	info = structure_content(Student_A)
%	info = ['Tom';'English';'A+']

function [content] = structure_content(structure)
fieldname = fieldnames(structure);
content =[];
for num = 1:length(fieldname)
	content = [content;(getfield(structure,fieldname{num}))'];
end