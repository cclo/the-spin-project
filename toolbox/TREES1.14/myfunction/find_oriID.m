% find_oriID returns the substructure root ID in a complete neuron
% The input should contain the tree structure of the complete tree and the
% tree structure of smaal group
function [ori_ID] = find_oriID(tree)
num_trees = length(tree);
ori_ID = zeros(1,num_trees);
for num = 1: num_trees
	if num == 1
		ori_ID(1,num) = 1;
		continue;
	end
	tar_node = [tree{num}.X(1), tree{num}.Y(1), tree{num}.Z(1)];
	for node = 1: length(tree{1}.X)
		cur_node = [tree{1}.X(node) tree{1}.Y(node) tree{1}.Z(node)];
		if isequal(cur_node,tar_node)
			ori_ID(1,num) = node;
			break;
		end
	end
end
		
	