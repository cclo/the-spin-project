% stats_glob    Claculate global morphological features. 
%
% state = stats_glob(intree)
%----------------------------------------------------------
%
% Calculate global features of a neuron. Global features are features that
% could only be calculated when the substructures are considered as part of
% the complete neuron.
% Note: this function is design for the specific purpose of SPIN, the first
% tree in each group will be treated as complete neuron. 
% The feature of complete neuron will be replaced with the maximum value of
% that feature in that neuron.  
% 
% Input
% -----
% - intree: neuron structure. intree has to be a cell array, with the first
% element containing the complete neuron information, the rest
% of the elements are substructure neuron info.
%
% Output
% ------
% - state: structure containing
% -- state.plen:: the path length from root (in the complete neuron)to the dividing point (the
% root of the substructure)
% -- state.BO:: the branch order of the dividing point when it is in the complete neuron
% Note: state(te).plen(1), state(te).BO(1) are the max value of the
% global features in the complete neuron, these values are used for
% normalizing substructure features.
% 
% Example
% -------
%  state = stats_glob(intree)
%
function state = stats_glob(intree)
% make intrees cell array convoluted to 2 depth
if ~iscell (intree)
    intree = {{intree}};
else
    if ~iscell (intree {1})
        intree = {intree};
    end
end
% get mean path length as a complete tree
lens = length (intree);
for te = 1 : lens % walk through tree groups 
	lent = length (intree {te}); % number of trees in this group
	real_ID = find_oriID(intree{te}); % find the node ID of the substructure root in a complete neuron
	state(te).plen = zeros(lent,1); % calculate mean path length as a complete tree
	state(te).BO = zeros(lent,1);   % calculate the number of branch point of the child_tree root in the complete tree
	pathlength = Pvec_tree(intree{te}{1});
	branch_order = BO_tree(intree{te}{1});
	for ward = 1 : lent
		% for complete tree, calculate max value of each property
        if ward == 1
            state(te).plen(ward) = max(pathlength);  
            state(te).BO(ward) = max(branch_order);
        else
            % calculate the path length from root to dividing point
            %child_mplen = mean(Pvec_tree(intree{te}{ward}));
            state(te).plen(ward) = pathlength(real_ID(ward)); % + child_mplen; 
            state(te).BO(ward) = branch_order(real_ID(ward));
        end
	end
end


