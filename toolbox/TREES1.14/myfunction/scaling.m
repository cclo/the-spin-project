function after_sca = scaling(table, bottom)
after_sca = zeros(size(table));
for col = 1:length(table(1,:))
	Min = min(table(:,col));
	Max = max(table(:,col));
	after_sca(:,col) = (table(:,col) - Min).*((1-bottom)/(Max-Min)) + bottom;
end