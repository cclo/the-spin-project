function gmmcOpt=gmmcOptSet
%gmmcOptSet: Set the specs of GMMC (Gaussian-mixture-model classifier) for further training
%
%	Usage:
%		gmmcOpt=gmmcOptSet
%
%	Description:
%		gmmcOpt=gmmcOptSet returns the default parameters for GMMC
%
%	Example:
%		gmmcOpt=gmmcOptSet;
%		gmmcOpt.config
%		gmmcOpt.train
%
%	See also gmmcTrain, gmmcEval.

%	Category: GMM classifier
%	Roger Jang, 20110616

gmmcOpt=gmmOptSet;
