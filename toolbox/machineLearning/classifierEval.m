function [computedClass, logLike, recogRate, hitIndex]=classifierEval(classifier, DS, cPrm, plotOpt)
% classifierEval: Evaluation of a given classifier
%
%	Usage:
%		computedClass=classifierEval(classifier, DS, cPrm)
%		computedClass=classifierEval(classifier, DS, cPrm, plotOpt)
%		[computedClass, logLike]=classifierEval(...)
%		[computedClass, logLike, recogRate]=classifierEval(...)
%
%		If DS does not have "output" field, then this command won't return "recogRate" and "hitIndex".
%
%	Description:
%		computedClass=classifierEval(classifier, DS, cPrm) returns the computed class of the dataset DS on a given classifier.
%			classifier: a string specifying a classifier
%				classifier='qc' for quadratic classifier
%				classifier='nbc' for naive Bayes classifier
%				classifier='gmmc' for GMM classifier
%				classifier='linc' for linear classifier
%				classifier='src' for sparse-representation classifier
%			DS: data set for training
%			cPrm: parameters for the classifier, where cPrm.class(i) is the parameters for class i.
%			computedClass: a vector of computed classes for data instances in DS
%		computedClass=classifierEval(classifier, DS, cPrm, plotOpt) also plots the decision boundary if the dimension is 2.
%		[computedClass, logLike, recogRate]=classifierEval(...) returns	more info, including the log likelihood of each data instance, and the recognition rate (if the DS has the info of desired classes).
%
%	Example:
%		DS=prData('3classes');
%		plotOpt=1;
%		classifier='qc';
%		[cPrm, logLike, recogRate, hitIndex]=classifierTrain(classifier, DS);
%		DS.hitIndex=hitIndex;		% Attach hitIndex to DS for plotting
%		classifierPlot(classifier, DS, cPrm, 'decBoundary');

%	See also classifierTrain, classifierPlot.

%	Category: Classifier Evaluation
%	Roger Jang, 20110506

if nargin<1, selfdemo; return; end
if nargin<2, classifier='qc'; end
if nargin<3, cPrm=[]; end
if nargin<4, plotOpt=0; end

if isnumeric(DS)		% DS is actually the input matrix
	inputData=DS;
	clear DS;
	DS.input=inputData;
end

classNum=length(cPrm.class);
[dim, dataNum]=size(DS.input);
logLike=zeros(classNum, dataNum);
computedClass=zeros(1, dataNum);

switch(lower(classifier))
	case 'qc'
		for i=1:classNum
			logLike(i,:)=log(cPrm.prior(i));		% Take prior into consideration
		%	logLike(i,:)=logLike(i,:)+gaussianLog(DS.input, cPrm.class(i));
			dataMinusMu = DS.input-cPrm.class(i).mu*ones(1, dataNum);
			logLike(i,:)=logLike(i,:)-0.5*sum(dataMinusMu.*(cPrm.class(i).invSigma*dataMinusMu), 1)+cPrm.class(i).gconst;
		end
		[~, computedClass]=max(logLike);
	case 'nbc'
		for i=1:classNum
			logLike(i,:)=log(cPrm.prior(i));		% Take weight into consideration
			for j=1:dim
				dataMinusMu = DS.input(j,:)-cPrm.class(i).dim(j).mu*ones(1, dataNum);
				logLike(i,:)=logLike(i,:)-0.5*sum(dataMinusMu.*(cPrm.class(i).dim(j).invSigma*dataMinusMu), 1)+cPrm.class(i).dim(j).gconst;
			end
		end
		[~, computedClass]=max(logLike);
	case 'gmmc'
		priorLogProb=log(cPrm.prior/sum(cPrm.prior));
		% ====== Fully vectorized version, which is likely to be out of memory
		%outputLogProb=zeros(classNum, dataNum);		% This is memory hog, especially when classNum is big (in speaker id, for example)!!!
		%for i=1:classNum
		%	outputLogProb(i,:)=gmmEval(data, cPrm.class(i).gmmPrm)+priorLogProb(i);
		%end
		%[maxValue, computedClass]=max(outputLogProb);
		% ====== Partial vectorized version, which operates with a chunk of 10000 entries of data at a time to avoid "out of memory" error.
		% chunkSize=1 for fully for-loop version
		% chunkSize=inf for fully vectorized version (which could cause "out of memory" is data size is large)
		chunkSize=10000;
		chunkNum=ceil(dataNum/chunkSize);
		if chunkNum==0, chunkNum=1; end		% This happens when chunkSize=inf
		computedClass=zeros(1, dataNum);
		for j=1:chunkNum
			rangeIndex=((j-1)*chunkSize+1):min(j*chunkSize, dataNum);
			theLogProb=zeros(classNum, length(rangeIndex));
			for i=1:classNum
				theLogProb(i,:)=gmmEval(DS.input(:, rangeIndex), cPrm.class(i).gmmPrm)+priorLogProb(i);
				logLike(i,rangeIndex)=theLogProb(i,:);
			end
		%	[maxValue, computedClass(rangeIndex)]=max(theLogProb);
		end
		[~, computedClass]=max(logLike);
	case 'src'
		if strcmpi(cPrm.optimMethod, 'linprog')
			linProgOpt = struct('Display', 'off', 'TolFun', [], 'Diagnostics', 'off', 'LargeScale', 'on', 'MaxIter', [], 'Simplex', 'off');
			classDist=zeros(classNum, dataNum);
			A=[cPrm.class.input];
			if cPrm.useUnitFeaVec
				A=A/(diag(sqrt(diag(A'*A))));	% Normalize all feature vectors to have a length of 1. Why is this necessary??? 
			end
			n=size(A,2);
			% ====== Compute the predicted class for each data instance
			for j=1:dataNum
				% === Find x1 such that norm(x1, 1) is minimized subject to the constraint A*x1=y
				y=DS.input(:,j);
				if cPrm.useUnitFeaVec, y=y/norm(y); end
				f=ones(2*n,1);
				Aeq=[A -A];
				lb=zeros(2*n,1);
				x1=linprog(f, [], [], Aeq, y, lb, [], [], linProgOpt);
				x1=x1(1:n)-x1(n+1:2*n);
				% === Find the class
				for i=1:classNum
					deltaX=zeros(n, 1); deltaX(cPrm.class(i).index)=x1(cPrm.class(i).index);
					classDist(i,j)=norm(y-A*deltaX);
				end
			end
			logLike=1./classDist;
			[~, computedClass]=max(logLike);
		elseif strcmpi(cPrm.optimMethod, 'spg')
			addpath([mltRoot, '/externalTool']);			% For using SC
			addpath([mltRoot, '/externalTool/spgl1-1.7']);	% For using L1 minimization within SC
			Train.X=[cPrm.class.input];
			output=[];
			for i=1:length(cPrm.class)
				output=[output, i*ones(1, length(cPrm.class(i).index))];
			end
			Train.y=output;
			Test.X=DS.input;
			Test.y=DS.output;
			[computedClass, relative_error]=SC(Train, Test, 150);
			recogRate=1-relative_error;
			logLike=nan*DS.output;
		else
			error('Unknown cPrm.optimMethod: %s', cPrm.optimMethod);
		end
	otherwise
		error('Unknown classifier: %s', classifier);
end

recogRate=[];
hitIndex=[];
if isfield(DS, 'output')
	hitIndex=find(computedClass==DS.output);
	recogRate=length(hitIndex)/dataNum;
	DS.hitIndex=hitIndex;	% For plotting by classifierPlot
end

if plotOpt && dim==2
	classifierPlot(classifier, DS, cPrm, 'decBoundary');
end

% ====== Self demo
function selfdemo
mObj=mFileParse(which(mfilename));
strEval(mObj.example);
