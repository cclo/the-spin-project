function [prm, logLike, recogRate, hitIndex]=nbcTrain(DS, trainPrm, plotOpt)
% nbcTrain: Training the naive Bayes classifier
%
%	Usage:
%		[prm, logLike, recogRate, hitIndex]=nbcTrain(DS, trainPrm, plotOpt)
%			DS: data set for training
%			trainPrm: parameters for training
%				trainPrm.prior: a vector of class prior probability
%				(Data count based prior is assume if an empty matrix is given.)
%			plotOpt: 1 for plotting
%			prm: prm.class(i) is the parameters for class i, etc.
%			recogRate: recognition rate
%			hitIndex: index of the correctly classified data points
%
%	Description:
%		[prm, logLike, recogRate, hitIndex]=nbcTrain(DS, trainPrm, plotOpt) returns the training results of the naive bayes classifier
%
%	Example:
%		DS=prData('iris');
%		DS.input=DS.input(3:4, :);
%		[prm, logLike, recogRate, hitIndex]=nbcTrain(DS);
%		DS.hitIndex=hitIndex;		% Attach hitIndex to DS for plotting
%		nbcPlot(DS, prm, 'decBoundary');
%
%	See also nbcEval, nbcSurface.

%	Category: Naive Bayes classifier
%	Roger Jang, 20110428

if nargin<1, selfdemo; return; end
% ====== Set the default options
if ischar(DS) && strcmpi(DS, 'defaultOpt')
	prm=[];
	return
end
if nargin<2||isempty(trainPrm), trainPrm=feval(mfilename, 'defaultOpt'); end
if nargin<3, plotOpt=0; end
[prm, logLike, recogRate, hitIndex]=classifierTrain('nbc', DS, trainPrm, plotOpt);

% ====== Self demo
function selfdemo
mObj=mFileParse(which(mfilename));
strEval(mObj.example);
