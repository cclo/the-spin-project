function [optimValue, dpPath, dpTable, time]=dpOverMapM(stateProbMat, transProbMat, plotOpt)
% dpOverMap: An m-file implementation of DP over matrix of state probability.
%
%	Usage:
%		optimValue=dpOverMapM(stateProbMat, transProbMat)
%		optimValue=dpOverMapM(stateProbMat, transProbMat, plotOpt)
%		[optimValue, dpPath, dpTable, time]=dpOverMapM(...)
%
%	Description:
%		[optimValue, dpPath]=dpOverMapM(stateProbMat, transProbMat) returns the optimum value and the corresponding DP (dynamic programming) for HMM evaluation.
%			stateProbMat: matrix of state probabilities
%			transProbMat: matrix of transition probabilities
%
%	Example:
%		load pfMat.mat
%		pfMat(1:20, :)=0;
%		%pfMat=[5 2 6; 2 9 3];
%		penalty=10000;
%		plotOpt=1;
%		[optimValue, dpPath, dpTable, time]=dpOverMapM(pfMat, penalty, plotOpt);
%		fprintf('Time=%.2f sec\n', time);
%
%	See also dpOverMap.

%	Category: HMM
%	Roger Jang, 20090928

if nargin<1, selfdemo; return; end
if nargin<3, plotOpt=0; end

[stateNum, frameNum]=size(stateProbMat);
if isscalar(transProbMat)	% This is actually penalty for state transition
	penalty=transProbMat;
	[xx,yy]=meshgrid(1:stateNum); transProbMat=-penalty*abs(xx-yy);
end

stateProbMat=stateProbMat';
[frameNum, stateNum]=size(stateProbMat);
dpTable=zeros(frameNum, stateNum);
prevPos=zeros(frameNum, stateNum);
dpPath=zeros(frameNum, 2);

tic
% ====== Fill dpTable
dpTable(1,:)=stateProbMat(1,:);
for i=2:frameNum
	for j=1:stateNum
		prob=dpTable(i-1,:)+transProbMat(:,j)';
		[maxProb, index]=max(prob);
		prevPos(i,j)=index;
		dpTable(i,j)=stateProbMat(i,j)+maxProb;
	end
end

% ===== Backtrack to find the optimal path
dpPath(:,1)=1:frameNum;
[optimValue, dpPath(end,2)]=max(dpTable(end, :));
for j=frameNum-1:-1:1
	dpPath(j,2)=prevPos(j+1, dpPath(j+1,2));
end

time=toc;
dpPath=dpPath';
dpTable=dpTable';
stateProbMat=stateProbMat';

if plotOpt
	subplot(2,2,1);
	frameNum=size(stateProbMat,2);
	frameTime=1:frameNum;
	imagesc(frameTime, 1:size(stateProbMat,1), stateProbMat); shading flat; axis xy; colorbar
	title('Map');
	for i=1:frameNum
		line(dpPath(1,i), dpPath(2,i), 'color', 'k', 'marker', '.');
	end
	subplot(2,2,3);
	mesh(frameTime, 1:size(stateProbMat,1), stateProbMat); axis tight; colorbar
	for i=1:frameNum
		line(dpPath(1,i), dpPath(2,i), stateProbMat(dpPath(2,i), dpPath(1,i)), 'color', 'k', 'marker', '.');
	end
	title('Opt. path over the map');
	
	subplot(2,2,2);
	imagesc(frameTime, 1:size(dpTable,1), dpTable); shading flat; axis xy; colorbar
	title('dpTable');
	for i=1:frameNum
		line(dpPath(1,i), dpPath(2,i), 'color', 'k', 'marker', '.');
	end
	subplot(2,2,4);
	mesh(frameTime, 1:size(dpTable,1), dpTable); axis tight; colorbar
	for i=1:frameNum
		line(dpPath(1,i), dpPath(2,i), dpTable(dpPath(2,i), dpPath(1,i)), 'color', 'k', 'marker', '.');
	end
	title('Opt. path over the DP table');
end

% ====== Self demo
function selfdemo
mObj=mFileParse(which(mfilename));
strEval(mObj.example);
