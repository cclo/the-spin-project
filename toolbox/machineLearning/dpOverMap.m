function [optimValue, dpPath, dpTable, time]=dpOverMap(stateProbMat, transProbMat, plotOpt)
% dpOverMap: DP over matrix of state probability.
%
%	Usage:
%		optimValue=dpOverMap(stateProbMat, transProbMat)
%		optimValue=dpOverMap(stateProbMat, transProbMat, plotOpt)
%		[optimValue, dpPath, dpTable, time]=dpOverMap(...)
%
%	Description:
%		[optimValue, dpPath]=dpOverMap(stateProbMat, transProbMat) returns the optimum value and the corresponding DP (dynamic programming) for HMM evaluation.
%			stateProbMat: matrix of state probabilities
%			transProbMat: matrix of transition probabilities
%
%	Example:
%		load pfMat.mat
%		pfMat(1:20, :)=0;
%		%pfMat=[5 2 6; 2 9 3];
%		penalty=10000;
%		plotOpt=1;
%		[optimValue, dpPath, dpTable]=dpOverMap(pfMat, penalty, plotOpt);

%	Category: HMM
%	Roger Jang, 20101028

if nargin<1, selfdemo; return; end
if nargin<3, plotOpt=0; end

[stateNum, frameNum]=size(stateProbMat);
if isscalar(transProbMat)	% This is actually penalty for state transition
	penalty=transProbMat;
	[xx,yy]=meshgrid(1:stateNum); transProbMat=-penalty*abs(xx-yy);
end

mexCommand='dpOverMapMex';
mCommand='dpOverMapM';

tic
try
	[optimValue, dpPath, dpTable]=feval(mexCommand, stateProbMat', transProbMat);
catch exception
	fprintf('%s is disabled due to the error message "%s".\n%s is activated instead.\n', mexCommand, exception.message, mCommand);
	[optimValue, dpPath, dpTable]=feval(mCommand, stateProbMat', transProbMat);
end
time=toc;
dpTable=dpTable';

if plotOpt
	subplot(2,2,1);
	frameNum=size(stateProbMat,2);
	frameTime=1:frameNum;
	imagesc(frameTime, 1:size(stateProbMat,1), stateProbMat); shading flat; axis xy; colorbar
	title('Map');
	for i=1:frameNum
		line(dpPath(1,i), dpPath(2,i), 'color', 'k', 'marker', '.');
	end
	subplot(2,2,3);
	mesh(frameTime, 1:size(stateProbMat,1), stateProbMat); axis tight; colorbar
	for i=1:frameNum
		line(dpPath(1,i), dpPath(2,i), stateProbMat(dpPath(2,i), dpPath(1,i)), 'color', 'k', 'marker', '.');
	end
	title('Opt. path over the map');
	
	subplot(2,2,2);
	imagesc(frameTime, 1:size(dpTable,1), dpTable); shading flat; axis xy; colorbar
	title('dpTable');
	for i=1:frameNum
		line(dpPath(1,i), dpPath(2,i), 'color', 'k', 'marker', '.');
	end
	subplot(2,2,4);
	mesh(frameTime, 1:size(dpTable,1), dpTable); axis tight; colorbar
	for i=1:frameNum
		line(dpPath(1,i), dpPath(2,i), dpTable(dpPath(2,i), dpPath(1,i)), 'color', 'k', 'marker', '.');
	end
	title('Opt. path over the DP table');
end

% ====== Self demo
function selfdemo
mObj=mFileParse(which(mfilename));
strEval(mObj.example);
