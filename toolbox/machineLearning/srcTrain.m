function [srcModel, logLike, recogRate, hitIndex]=srcTrain(DS, srcOpt, plotOpt)
% srcTrain: Training the SRC (sparse-representation classifier)
%
%	Usage:
%		srcModel=srcTrain(DS, srcOpt, plotOpt)
%			DS: data set for training
%			srcOpt: parameters for training (whic is passed to srcEval and used there)
%			plotOpt: 1 for plotting (which is not used for now)
%			srcModel: which is the same as DS for now
%
%	Description:
%		srcModel=srcTrain(DS, srcOpt, plotOpt) returns the training results of SRC
%
%	Example:
%		DS=prData('iris');
%		DS.input=DS.input(3:4, :);
%		srcModel=srcTrain(DS)
%
%	See also srcEval.

%	Category: Sparse-representation classifier
%	Roger Jang, 20111029

if nargin<1, selfdemo; return; end
% ====== Set the default options
if ischar(DS) && strcmpi(DS, 'defaultOpt')
	srcModel.useUnitFeaVec=1;
	srcModel.optimMethod='linProg';		% 'linProg' or 'SPG'
	return
end
if nargin<2||isempty(srcOpt), srcOpt=feval(mfilename, 'defaultOpt'); end
if nargin<3, plotOpt=0; end
[srcModel, logLike, recogRate, hitIndex]=classifierTrain('src', DS, srcOpt, plotOpt);

% ====== Self demo
function selfdemo
mObj=mFileParse(which(mfilename));
strEval(mObj.example);
