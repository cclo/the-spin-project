function [computedClass, logLike, recogRate, hitIndex]=svmcEval(DS, svmModel)
% svmTrain: Evaluation of SVM (support vector machine) classifier
%
%	Usage:
%		computedClass=svmTrain(DS, svmModel)
%		[computedClass, ~, recogRate, hitIndex]=svmcEval(DS, svmModel)
%
%	Description:
%		<p><tt>computedClass=svmcEval(DS, svmModel)</tt> returns values of SVM (support vector machine) based on the given dataset DS and the SVM model svmModel.
%		Note that this function calls the mex files of libsvm directly. If the mex file libsvmpredict.mex* under [mltRoot, '/private'] is not found, you need to obtain it from libsvm website at "http://www.csie.ntu.edu.tw/~cjlin/libsvm/".
%
%	Example:
%		DS=prData('iris');
%		DS.input=DS.input(3:4, :);
%		trainSet.input=DS.input(:, 1:2:end); trainSet.output=DS.output(:, 1:2:end);
%		testSet.input=DS.input(:, 2:2:end);  testSet.output=DS.output(:, 2:2:end);
%		[svmPrm, logLike1, recogRate1]=svmcTrain(trainSet);
%		[computedClass, logLike2, recogRate2, hitIndex]=svmcEval(testSet, svmPrm);
%		fprintf('Inside recog. rate = %g%%\n', recogRate1*100);
%		fprintf('Outside recog. rate = %g%%\n', recogRate2*100);
%
%	See also svmTrain.

%	Category: Support vector machine
%	Roger Jang, 20111029

if nargin<1, selfdemo; return; end

% ====== Add the path of libsvm by Prof. Chih-Jen Lin
addpath([mltRoot, '/externalTool/libsvm-3.11/matlab']);

if ~isfield(DS, 'output')
	DS.output=[];
end
[computedClass, rr, dec_values_L]=svmpredict(DS.output', DS.input', svmModel);
computedClass=computedClass';
logLike=nan*ones(1, length(DS.output));
recogRate=rr(1)/100;
hitIndex=find(computedClass==DS.output);

% ====== Self demo
function selfdemo
mObj=mFileParse(which(mfilename));
strEval(mObj.example);
