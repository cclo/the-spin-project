function gmmTrainOpt=gmmTrainOptSet
%gmmTrainOptSet: Set training options for GMM
%
%	Usage:
%		gmmTrainOpt=gmmTrainOptSet
%
%	Description:
%		gmmTrainOpt=gmmTrainOptSet returns the training parameters for GMM
%
%	Example:
%		gmmTrainOpt=gmmTrainOptSet
%
%	See also gmmTrain, gmmEval.

%	Category: GMM
%	Roger Jang

% The following parameters are used for gmmTrain()
gmmTrainOpt.dispOpt=0;		% Display info during training
%gmmTrainOpt.plotOpt=0;		% Display rr with respect to mix numbers
gmmTrainOpt.useKmeans=1;		% Use kmeans to find the initial centers
gmmTrainOpt.maxIteration=20;		% Max. iteration
gmmTrainOpt.minImprove=eps;		% Min. improvement
gmmTrainOpt.minVariance=eps;		% Min. variance
gmmTrainOpt.usePartialVectorization=0;

% The following parameters are used for gmmTrainEvalWrtGaussianNum()
gmmTrainOpt.useCenterSplitting=0;	% Use center splitting (Only if the no. of gaussians increases by the power of 2.)
