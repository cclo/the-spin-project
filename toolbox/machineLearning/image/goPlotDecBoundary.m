DS=prData('iris');
DS.input=DS.input(3:4, :);

figure; knncPlot(DS, [], 'decBoundary');
title('RR = 100%');
print -dpng knncDecBoundary

[nbcPrm, logProb, recogRate, DS.hitIndex]=nbcTrain(DS);
figure; nbcPlot(DS, nbcPrm, 'decBoundary');
print -dpng nbcDecBoundary


[qcPrm, logProb, recogRate, DS.hitIndex]=qcTrain(DS);
figure; qcPlot(DS, qcPrm, 'decBoundary');
print -dpng qcDecBoundary
