function [cvData, count]=cvDataGen(DS, m, mode)
% cvDataGen: Generate m-fold cross validation (CV) data for performance evaluation
%
%	Usage:
%		cvData=cvDataGen(DS, m)
%		cvData=cvDataGen(DS, m, mode)
%		[cvData, count]=cvDataGen(...)
%
%	Description:
%		[cvData, count]=cvDataGen(DS, m, mode) generates m-fold cross-validation data for performance evaluation.
%			DS: dataset to be partitioned
%			m: number of folds
%			mode: 'full' (full data) or 'index' (index only, default)
%		The m-fold CV data is generated to satisfy the following two criteria:
%			Each fold has the same number (or as close as possible) of data instances.
%			Each fold has the same (or as close as possible) class distribution.
%		You can example the	class distribution via the matrix count, where count(i,j) is the number of instances of class i within fold j.
%		If the mode is "full", then cvData is a structure array of m elements, with "TS" and "VS" fields for "training set" and "validating set", respectively.
%		If the mode is "index", then both cvData.TS and cvData.VS contain only the indices of the original data for saving memory.
%
%	Example:
%		DS=prData('wine');
%		[cvData, count]=cvDataGen(DS, 5);
%		fprintf('cvData:\n'); disp(cvData);
%		fprintf('count:\n'); disp(count);

%	Category: Performance evaluation
%	Roger Jang, 20070410, 20110425

if nargin<1, selfdemo; return; end
if nargin<2, m=10; end
if nargin<3, mode='index'; end

[dim, dataNum]=size(DS.input);
if isinf(m), m=dataNum; end
subSize=round(dataNum/m);
classSize=dsClassSize(DS);
classNum=length(classSize);
for i=1:classNum
	classIndex{i}=find(i==DS.output);	% Indices of data in class i
end

% Compute the data count for each class in each fold
count=zeros(classNum, m);
startPos=1;
for i=1:classNum
	k=floor(classSize(i)/m);
%	if k==0
%		error(sprintf('The fold number %d if larger than data count in class %d!', m, i));
%	end
	count(i,:)=k*ones(1,m);
	largerCount=classSize(i)-k*m;
	for j=startPos:startPos+largerCount-1
		count(i,mod(j-1,m)+1)=count(i,mod(j-1,m)+1)+1;
	end
	startPos=startPos+largerCount;
end
%disp(count)

for i=1:m
	index=[];	% This is the index for validation set
	for j=1:classNum
		if i==1
			start=1;
		else
			start=sum(count(j,1:i-1))+1;
		end
		stop=start+count(j,i)-1;
		indexRange=start:stop;
		index=[index, classIndex{j}(indexRange)];
	end
	vIndex=index;
	tIndex=1:dataNum;
	tIndex(vIndex)=[];
	if strcmp(mode, 'index')
		cvData(i).TS.index=tIndex;
		cvData(i).VS.index=vIndex;
		continue;
	end
	cvData(i).TS.input=DS.input(:, tIndex);
	cvData(i).TS.output=DS.output(:, tIndex);
	cvData(i).TS.inputName=DS.inputName;
	cvData(i).TS.outputName=DS.outputName;
	cvData(i).VS.input=DS.input(:, vIndex);
	cvData(i).VS.output=DS.output(:, vIndex);
	cvData(i).VS.inputName=DS.inputName;
	cvData(i).VS.outputName=DS.outputName;
end

% ====== Self demo
function selfdemo
mObj=mFileParse(which(mfilename));
strEval(mObj.example);
