%% perfLoo
% Leave-one-out recognition rate of given dataset and classifier
%% Syntax
% * 		recogRate=looTest(DS, classifier, classifierOpt)
% * 		recogRate=looTest(DS, classifier, classifierOpt, plotOpt)
% * 		[recogRate, computedClass]=looTest(...)
%% Description
%
% <html>
% <p>recogRate=looTest(DS, classifier, classifierOpt) returns the leave-one-out recognition rate of the given dataset and classifier.
% 	<ul>
% 	<li>recogRate: recognition rate
% 	<li>DS: Dataset
% 		<ul>
% 		<li>DS.input: Input data (each column is a feature vector)
% 		<li>DS.output: Output class (ranging from 1 to N)
% 		</ul>
% 	<li>classifierOpt: Training parameters for the classifier
% 	</ul>
% <p>recogRate=looTest(DS, classifier, classifierOpt, 1) also plots the dataset and misclasified instances (if the dimension is 2).
% <p>[recogRate, computedClass]=looTest(...) also returns the computed class of each data instance in DS.
% </html>
%% Example
%%
%
DS=prData('random2');
plotOpt=1;
recogRate=perfLoo(DS, 'qc', [], plotOpt);
