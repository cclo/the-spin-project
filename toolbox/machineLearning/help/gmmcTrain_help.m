%% gmmcTrain
% Train a GMM classifier
%% Syntax
% * 		[gmmcPrm, logLike] = gmmcTrain(DS)
% * 		[gmmcPrm, logLike] = gmmcTrain(DS, opt)
%% Description
%
% <html>
% <p>[gmmcPrm, logLike] = gmmcTrain(DS) returns the parameters of a GMM classifier based on the training of the give dataset DS.
% 	<ul>
% 	<li>DS: Design dataset
% 	<li>opt: GMMC options
% 		<ul>
% 		<li>opt.config.gaussianNum: A column vector indicating no. of Gaussians for each class
% 		<li>opt.config.covType: Type of covariance matrix
% 		<li>tmmcOpt.train: Parameters for training each GMM, which can be obtained via gmmTrainOptSet.m.
% 		</ul>
% 	<li>gmmcPrm: Parameters for GMM classifier
% 		<ul>
% 		<li>gmmcPrm.gmm(i): Parameters for class i, which is modeled as a GMM
% 			<ul>
% 			<li>gmmcPrm.gmm(i).gmmPrm(j).mu: a mean vector of dim x 1 for Gaussian component j
% 			<li>gmmcPrm.gmm(i).gmmPrm(j).sigma: a covariance matrix for Gaussian component j
% 			<li>gmmcPrm.gmm(i).gmmPrm(j).w: a weighting factor for Gaussian component j
% 			</ul>
% 		<li>gmmcPrm.prior: Vector of priors, or simply the vector holding no. of entries in each class
% 		<li>(To obtain the class sizes, you can use "dsClassSize".
% 		</ul>
% 	<li>logLike: Vector of log likelihood during training
% 	</ul>
% </html>
%% Example
%%
%
[DS, TS]=prData('iris');
DS.input=DS.input(3:4, :);	% Only use the last 2 dim
TS.input=TS.input(3:4, :);	% Only use the last 2 dim
opt=gmmcOptSet;
gmmcPrm=gmmcTrain(DS, opt);
cOutput=gmmcEval(DS, gmmcPrm);
recogRate1=sum(DS.output==cOutput)/length(DS.output);
fprintf('Inside-test recog. rate = %g%%\n', recogRate1*100);
cOutput=gmmcEval(TS, gmmcPrm);
recogRate2=sum(TS.output==cOutput)/length(TS.output);
fprintf('Outside-test recog. rate = %g%%\n', recogRate2*100);
TS.hitIndex=find(TS.output==cOutput);
gmmcPlot(TS, gmmcPrm, 'decBoundary');
%% See Also
% <gmmcEval_help.html gmmcEval>,
% <gmmEval_help.html gmmEval>,
% <gmmTrain_help.html gmmTrain>,
% <gmmMixNumEstimate_help.html gmmMixNumEstimate>.
