%% srcTrain
% Training the SRC (sparse-representation classifier)
%% Syntax
% * 		srcModel=srcTrain(DS, srcOpt, plotOpt)
% * 			DS: data set for training
% * 			srcOpt: parameters for training (whic is passed to srcEval and used there)
% * 			plotOpt: 1 for plotting (which is not used for now)
% * 			srcModel: which is the same as DS for now
%% Description
% 		srcModel=srcTrain(DS, srcOpt, plotOpt) returns the training results of SRC
%% Example
%%
%
DS=prData('iris');
DS.input=DS.input(3:4, :);
srcModel=srcTrain(DS)
%% See Also
% <srcEval_help.html srcEval>.
