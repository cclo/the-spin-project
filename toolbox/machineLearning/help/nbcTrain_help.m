%% nbcTrain
% Training the naive Bayes classifier
%% Syntax
% * 		[prm, logLike, recogRate, hitIndex]=nbcTrain(DS, trainPrm, plotOpt)
% * 			DS: data set for training
% * 			trainPrm: parameters for training
% * 				trainPrm.prior: a vector of class prior probability
% * 				(Data count based prior is assume if an empty matrix is given.)
% * 			plotOpt: 1 for plotting
% * 			prm: prm.class(i) is the parameters for class i, etc.
% * 			recogRate: recognition rate
% * 			hitIndex: index of the correctly classified data points
%% Description
% 		[prm, logLike, recogRate, hitIndex]=nbcTrain(DS, trainPrm, plotOpt) returns the training results of the naive bayes classifier
%% Example
%%
%
DS=prData('iris');
DS.input=DS.input(3:4, :);
[prm, logLike, recogRate, hitIndex]=nbcTrain(DS);
DS.hitIndex=hitIndex;		% Attach hitIndex to DS for plotting
nbcPlot(DS, prm, 'decBoundary');
%% See Also
% <nbcEval_help.html nbcEval>,
% <nbcSurface_help.html nbcSurface>.
