%% qcTrain
% Training the quadratic classifier (QC)
%% Syntax
% * 		qcModel=qcTrain(DS)
% * 		qcModel=qcTrain(DS, qcOpt)
% * 		qcModel=qcTrain(DS, qcOpt, plotOpt)
% * 		[qcModel, logLike, recogRate, hitIndex]=qcTrain(DS, ...)
%% Description
%
% <html>
% <p><p><tt>qcModel=qcTrain(DS)</tt> returns the parameters of the quadratic classifier
% <p>based on the given dataset DS. The parameters for class i is stored
% <p>in qcModel.class(i).
% <p><p><tt>qcModel=qcTrain(DS, qcOpt)</tt> uses the train parameters qcOpt for
% <p>training the QC. qcOpt only contains a field prior to represent the
% <p>prior probability of each class. If qcOpt is empty, the the
% <p>default prior probability is based on the data counts of each
% <p>class.
% <p><p><tt>qcModel=qcTrain(DS, qcOpt, plotOpt)</tt> plots the decision
% <p>boundary of the QC (if the feature dimensionity is 2).
% <p><p><tt>[qcModel, logLike, recogRate, hitIndex]=qcTrain(DS, ...)</tt> also
% <p>returns the log likelihood, recognition rate, and the hit indice of
% <p>data instances in DS.
% </html>
%% Example
%%
%
DS=prData('nonlinearSeparable');
[qcModel, logLike, recogRate, hitIndex]=qcTrain(DS);
DS.hitIndex=hitIndex;		% Attach hitIndex to DS for plotting
qcPlot(DS, qcModel, 'decBoundary');
%% See Also
% <qcEval_help.html qcEval>,
% <qcOptSet_help.html qcOptSet>.
