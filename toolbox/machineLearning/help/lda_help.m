%% lda
% Linear discriminant analysis
%% Syntax
% * 		DS2 = lda(DS)
% * 		DS2 = lda(DS, discrimVecNum)
% * 		[DS2, discrimVec, eigValues] = lda(...)
%% Description
%
% <html>
% <p>DS2 = lda(DS, discrimVecNum) returns the results of LDA (linear discriminant analysis) on DS
% 	<ul>
% 	<li>DS: input dataset (Try "DS=prData('iris')" to get an example of DS.)
% 	<li>discrimVecNum: No. of discriminant vectors
% 	<li>DS2: output data set, with new feature vectors
% 	</ul>
% <p>[DS2, discrimVec, eigValues] = lda(DS, discrimVecNum) returns extra info:
% 	<ul>
% 	<li>discrimVec: discriminant vectors identified by LDA
% 	<li>eigValues: eigen values corresponding to the discriminant vectors
% 	</ul>
% </html>
%% References
% # 		[1] J. Duchene and S. Leclercq, "An Optimal Transformation for Discriminant Principal Component Analysis," IEEE Trans. on Pattern Analysis and Machine Intelligence, Vol. 10, No 6, November 1988
%% Example
%%
%
DS=prData('iris');
DS2=lda(DS);
subplot(1,2,1); dsScatterPlot(DS2); xlabel('Input 1'); ylabel('Input 2');
title('Iris dataset projected on the first 2 LDA vectors');
DS2.input=DS2.input(3:4, :);
subplot(1,2,2); dsScatterPlot(DS2); xlabel('Input 3'); ylabel('Input 4');
title('Iris dataset projected on the last 2 LDA vectors');
%% See Also
% <ldaPerfViaKnncLoo_help.html ldaPerfViaKnncLoo>.
