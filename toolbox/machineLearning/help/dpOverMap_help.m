%% dpOverMap
% DP over matrix of state probability.
%% Syntax
% * 		optimValue=dpOverMap(stateProbMat, transProbMat)
% * 		optimValue=dpOverMap(stateProbMat, transProbMat, plotOpt)
% * 		[optimValue, dpPath, dpTable, time]=dpOverMap(...)
%% Description
%
% <html>
% <p>[optimValue, dpPath]=dpOverMap(stateProbMat, transProbMat) returns the optimum value and the corresponding DP (dynamic programming) for HMM evaluation.
% 	<ul>
% 	<li>stateProbMat: matrix of state probabilities
% 	<li>transProbMat: matrix of transition probabilities
% 	</ul>
% </html>
%% Example
%%
%
load pfMat.mat
pfMat(1:20, :)=0;
%pfMat=[5 2 6; 2 9 3];
penalty=10000;
plotOpt=1;
[optimValue, dpPath, dpTable]=dpOverMap(pfMat, penalty, plotOpt);
