function [qcModel, logLike, recogRate, hitIndex]=qcTrain(DS, qcOpt, plotOpt)
% qcTrain: Training the quadratic classifier (QC)
%
%	Usage:
%		qcModel=qcTrain(DS)
%		qcModel=qcTrain(DS, qcOpt)
%		qcModel=qcTrain(DS, qcOpt, plotOpt)
%		[qcModel, logLike, recogRate, hitIndex]=qcTrain(DS, ...)
%
%	Description:
%		<p><tt>qcModel=qcTrain(DS)</tt> returns the parameters of the quadratic classifier
%		based on the given dataset DS. The parameters for class i is stored
%		in qcModel.class(i).
%		<p><tt>qcModel=qcTrain(DS, qcOpt)</tt> uses the train parameters qcOpt for
%		training the QC. qcOpt only contains a field prior to represent the
%		prior probability of each class. If qcOpt is empty, the the
%		default prior probability is based on the data counts of each
%		class.
%		<p><tt>qcModel=qcTrain(DS, qcOpt, plotOpt)</tt> plots the decision
%		boundary of the QC (if the feature dimensionity is 2).
%		<p><tt>[qcModel, logLike, recogRate, hitIndex]=qcTrain(DS, ...)</tt> also
%		returns the log likelihood, recognition rate, and the hit indice of
%		data instances in DS.
%
%	Example:
%		DS=prData('nonlinearSeparable');
%		[qcModel, logLike, recogRate, hitIndex]=qcTrain(DS);
%		DS.hitIndex=hitIndex;		% Attach hitIndex to DS for plotting
%		qcPlot(DS, qcModel, 'decBoundary');
%
%	See also qcEval, qcOptSet.

%	Category: Quadratic classifier
%	Roger Jang, 20041123, 20080924

if nargin<1, selfdemo; return; end
% ====== Set the default options
if ischar(DS) && strcmpi(DS, 'defaultOpt')
	qcModel=qcOptSet;
	return
end
if nargin<2||isempty(qcOpt), qcOpt=feval(mfilename, 'defaultOpt'); end
if nargin<3, plotOpt=0; end
[qcModel, logLike, recogRate, hitIndex]=classifierTrain('qc', DS, qcOpt, plotOpt);

% ====== Self demo
function selfdemo
mObj=mFileParse(which(mfilename));
strEval(mObj.example);
