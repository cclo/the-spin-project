function [gmmcPrm, logLike, recogRate, hitIndex]=gmmcTrain(DS, opt, plotOpt)
% gmmcTrain: Train a GMM classifier
%
%	Usage:
%		[gmmcPrm, logLike] = gmmcTrain(DS)
%		[gmmcPrm, logLike] = gmmcTrain(DS, opt)
%
%	Description:
%		[gmmcPrm, logLike] = gmmcTrain(DS) returns the parameters of a GMM classifier based on the training of the give dataset DS.
%			DS: Design dataset
%			opt: GMMC options
%				opt.config.gaussianNum: A column vector indicating no. of Gaussians for each class
%				opt.config.covType: Type of covariance matrix
%				tmmcOpt.train: Parameters for training each GMM, which can be obtained via gmmTrainOptSet.m.
%			gmmcPrm: Parameters for GMM classifier
%				gmmcPrm.gmm(i): Parameters for class i, which is modeled as a GMM
%					gmmcPrm.gmm(i).gmmPrm(j).mu: a mean vector of dim x 1 for Gaussian component j
%					gmmcPrm.gmm(i).gmmPrm(j).sigma: a covariance matrix for Gaussian component j
%					gmmcPrm.gmm(i).gmmPrm(j).w: a weighting factor for Gaussian component j
%				gmmcPrm.prior: Vector of priors, or simply the vector holding no. of entries in each class
%				(To obtain the class sizes, you can use "dsClassSize".
%			logLike: Vector of log likelihood during training
%
%	See also gmmcEval, gmmEval, gmmTrain, gmmMixNumEstimate.
%
%	Example:
%		[DS, TS]=prData('iris');
%		DS.input=DS.input(3:4, :);	% Only use the last 2 dim
%		TS.input=TS.input(3:4, :);	% Only use the last 2 dim
%		opt=gmmcOptSet;
%		gmmcPrm=gmmcTrain(DS, opt);
%		cOutput=gmmcEval(DS, gmmcPrm);
%		recogRate1=sum(DS.output==cOutput)/length(DS.output);
%		fprintf('Inside-test recog. rate = %g%%\n', recogRate1*100);
%		cOutput=gmmcEval(TS, gmmcPrm);
%		recogRate2=sum(TS.output==cOutput)/length(TS.output);
%		fprintf('Outside-test recog. rate = %g%%\n', recogRate2*100);
%		TS.hitIndex=find(TS.output==cOutput);
%		gmmcPlot(TS, gmmcPrm, 'decBoundary');

%	Category: GMM classifier
%	Roger Jang, 20090123, 20090303, 20100615

if nargin<1, selfdemo; return; end
% ====== Set the default options
if ischar(DS) && strcmpi(DS, 'defaultOpt')
	gmmcPrm=gmmcOptSet;
	return
end
if nargin<2||isempty(opt), opt=feval(mfilename, 'defaultOpt'); end
if nargin<3, plotOpt=0; end
[gmmcPrm, logLike, recogRate, hitIndex]=classifierTrain('gmmc', DS, opt, plotOpt);

% ====== Self demo
function selfdemo
mObj=mFileParse(which(mfilename));
strEval(mObj.example);
