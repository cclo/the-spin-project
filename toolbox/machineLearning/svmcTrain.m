function [svmModel, logLike, recogRate, hitIndex]=svmcTrain(DS, opt)
% svmcTrain: Training SVM (support vector machine) classifier
%
%	Usage:
%		svmModel=svmcTrain(DS)
%		svmModel=svmcTrain(DS, opt)
%		[svmModel, ~, recogRate, hitIndex]=qcTrain(DS, ...)
%
%	Description:
%		<p><tt>svmModel=svmcTrain(DS)</tt> returns the parameters of SVM (support vector machine) based on the given dataset DS.
%		<p><tt>opt=svmcTrain('defaultOpt')</tt> returns the default options for SVM. You can modify the options if necessary, and send the options for training a SVM: <p><tt>svmModel=svmcTrain(DS, opt)</tt>
%		Note that this function calls the mex files of libsvm directly. If the mex file svmtrain.mex* and svmpredict.mex* under [mltRoot, '/private'] is not found, you need to obtain it from libsvm website at "http://www.csie.ntu.edu.tw/~cjlin/libsvm/".
%
%	Example:
%		DS=prData('iris');
%		DS.input=DS.input(3:4, :);
%		trainSet.input=DS.input(:, 1:2:end); trainSet.output=DS.output(:, 1:2:end);
%		testSet.input=DS.input(:, 2:2:end);  testSet.output=DS.output(:, 2:2:end);
%		[svmModel, logLike1, recogRate1]=svmcTrain(trainSet);
%		[computedClass, logLike2, recogRate2, hitIndex]=svmcEval(testSet, svmModel);
%		fprintf('Inside recog. rate = %g%%\n', recogRate1*100);
%		fprintf('Outside recog. rate = %g%%\n', recogRate2*100);
%
%	See also svmcEval.

%	Category: Support vector machine
%	Roger Jang, 20111029


if nargin<1, selfdemo; return; end
% ====== Set the default options
if ischar(DS) && strcmpi(DS, 'defaultOpt')
	svmModel.cost=3;
	return
end
if nargin<2||isempty(opt), opt=feval(mfilename, 'defaultOpt'); end
if nargin<3, plotOpt=0; end

% ====== Add the path of libsvm by Prof. Chih-Jen Lin
addpath([mltRoot, '/externalTool/libsvm-3.11/matlab']);

svmOpt=['-t 2 -c ' num2str(opt.cost)];
svmModel=svmtrain(DS.output', DS.input', svmOpt);
logLike=nan*ones(1, length(DS.output));
computedClass=svmcEval(DS, svmModel);
recogRate=sum(computedClass==DS.output)/length(DS.output);
hitIndex=find(computedClass==DS.output);

% ====== Self demo
function selfdemo
mObj=mFileParse(which(mfilename));
strEval(mObj.example);
