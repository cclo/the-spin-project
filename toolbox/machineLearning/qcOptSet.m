function qcOpt=qcOptSet
% qcOptSet: Set the parameters for training a quadratic classifier
%
%	Usage:
%		qcOpt=qcOptSet
%
%	Description:
%		qcOpt=qcOptSet returns the parameters for training a
%		quadratic classifier. (Since there is no such parameters for now,
%		so the returned parameter vector is empty.)
%
%	Example:
%		qcOpt=qcOptSet
%
%	See also qcTrain, qcEval.

%	Category: Quadratic classifier
%	Roger Jang, 20100701

qcOpt=[];