function gmmOpt=gmmOptSet
% gmmOptSet: Set the options of a GMM (Gaussian mixture model) for configuration and training
%
%	Usage:
%		gmmOpt=gmmOptSet;
%
%	Description:
%		gmmOpt=gmmOptSet return the options of a GMM.
%			gmmOpt.config: Configuration of GMM
%			gmmOpt.config.guassianNum: No. of Gaussian components
%			gmmOpt.config.covType: Type of covariance matrix
%			gmmOpt.train: Training options, see gmmTrainOptSet.m
%
%	Example:
%		DS=dcData(4);
%		gmmOpt=gmmOptSet;
%		gmmPrm=gmmInitPrmSet(DS.input, gmmOpt);
%
%	See also gmmTrain, gmmEval, gmmTrainOptSet.

%	Category: GMM
%	Roger Jang, 20080726

gmmOpt.config.gaussianNum=2;
gmmOpt.config.covType=2;
gmmOpt.train=gmmTrainOptSet;
gmmOpt.train.usePartialVectorization=1;
