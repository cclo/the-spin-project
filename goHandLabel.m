% GOHANDLABEL is a GUI for hand-label axon and dendrite.
% DESCRIPTION goHandLabel will read neuron in 'neuronList.txt' file
%	and call hand-label function. Below is information of buttons.
%
%	previous:
%		draw previous neuron if it is not the first neuron.
%	next:
%		draw next neuron if it is not the last neuron.
%	axon:
%		label selected node as axon cluster 
%	dendrite:
%		label selected node as dendrite cluster 
%	replot:
%		reset the figure, axon, dendrite and cluster state of neuron.
%	export:
%		export a swc format file containing labeled axon, dendrite & Soma.
%
%   See tutorial in SPIN_OnlineResource_v0_1_2.pdf 
%   See also GUI_handLabel
%	------------------------
%	Author : Yen-Nan Lin, Yi-Hsuan Lee
%	Created: 2011-05-18, using mac Matlab R2009a
%   Modified: 2012-10-21, incorporate trees toolbox, using Windows 7 Matlab R2012a 
close all;
figure('Position', [727, 361, 250, 500]);
set(gcf, 'tag', 'HandLabel');
uicontrol('Style', 'pushbutton', 'String', 'Previous', 'Position', [49 399 151 51], ...
	'Callback', 'GUI_handLabel(''previous'')' );
uicontrol('Style', 'pushbutton', 'String', 'Next', 'Position', [49 349 151 51], ...
	'Callback', 'GUI_handLabel(''next'')' );
uicontrol('Style', 'pushbutton', 'String', 'Axon', 'Position', [49 249 151 51], ...
	'Callback', 'GUI_handLabel(''axon'')' );
uicontrol('Style', 'pushbutton', 'String', 'Dendrite', 'Position', [49 199 151 51], ...
	'Callback', 'GUI_handLabel(''dendrite'')' );
uicontrol('Style', 'pushbutton', 'String', 'Replot', 'Position', [49 150 151 51], ...
	'Callback', 'GUI_handLabel(''replot'')' );
uicontrol('Style', 'pushbutton', 'String', 'Export', 'Position', [49 50 151 51], ...
	'Callback', 'GUI_handLabel(''export'')' );
GUI_handLabel('initialize');
