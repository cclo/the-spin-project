% plot_ExpResult label region index with different colors
% axon: blue
% dendrite:green
function h = plot_ExpResult_radius(tree)
    parentID = idpar_tree(tree, '-0');
    childRegionInx = tree.R(2:end);
    parentRegionInx = tree.R(parentID(2:end));
    dp = find(childRegionInx - parentRegionInx ~= 0) + 1;
    % find the region index of each compartment
    somaInx = find(strcmp('1', tree.rnames));
    axonInx = find(strcmp('2', tree.rnames));
    dendriteInx = find(strcmp('3', tree.rnames));
    h = plot_tree(tree);
    hold on;
    % plot soma 
    soma = find(tree.R == somaInx);
    if ~isempty(soma)
        plot3(tree.X(soma(1)), tree.Y(soma(1)), tree.Z(soma(1)), '.', 'color', 'r', 'markersize', 40); % use the first one to represent soma 
    end
    tree.subtree = getSubtree(tree, dp);
    for iSub = 1:length(dp)
        if tree.R(dp(iSub)) == axonInx
                color = [0 1 0]; % plot axon subtree in blue
        elseif tree.R(dp(iSub)) == dendriteInx
            color = [0 0 1]; % plot axon subtree in green
        else
            color = [0 0 0]; % if the region doesn't belong to axon and dendrite, plot in black
        end
        plot_tree(tree.subtree{iSub}, color);
    end        
end