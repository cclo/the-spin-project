% This program count the terminal number that "only" follow by the selected
% point
function label_temp = countTerminal(treeOri, tree, label_temp, branchMin)
labeled = find(label_temp);
tpOri = find((T_tree(treeOri)));
for label = 1:length(labeled)
	currentID = labeled(label);
	subtree = getTerSubTree(tree, currentID, labeled);
	tp = find(T_tree(subtree));
	tp = findID(treeOri, [subtree.X(tp), subtree.Y(tp), subtree.Z(tp)]);
	tpReal = length(intersect(tp, tpOri));
	if tpReal < branchMin
		label_temp(currentID) = 0;
	end
end

	