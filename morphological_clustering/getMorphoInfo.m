function morphoInfo = getMorphoInfo(tree)
morphoInfo = struct;
morphoInfo.tp = find(T_tree(tree));
morphoInfo.bp = find(B_tree(tree));
morphoInfo.num_branch = length(morphoInfo.tp); 
morphoInfo.pathLength = Pvec_tree(tree);
morphoInfo.rootPath = ipar_tree(tree);
morphoInfo.terminalNumber = zeros(length(morphoInfo.rootPath), 1);
for node = 1:length(morphoInfo.rootPath)
	morphoInfo.terminalNumber(node) = sum(sum(double(morphoInfo.rootPath(morphoInfo.tp,:) == node)));
end
[tempSortedNum, tempSortIndex] = sort(morphoInfo.terminalNumber(morphoInfo.bp), 'descend');
morphoInfo.sortIndex = morphoInfo.bp(tempSortIndex); % arrange node index based on the number of terminal point of the branch points