% findID: find the ID of given point in a tree
% INPUT: 
% tree: neuron structure of a given neuron
% point: the point you want to its original ID. this input could be a
% vector, with the format like point = [point1_X, point1_Y, point_Z;
% point2_X, point2_Y, point2_Z,;......]
% OUTPUT:
% oriID: original ID of the given points. -1 means this point isn't
% contained in the tree.
function oriID = findID(tree, point)
totalID = [tree.X tree.Y tree.Z];
oriID = [];
for tar = 1:length(point(:,1))
	target = point(tar,:);
	for node = 1:size(totalID,1)
		curNode = totalID(node,:);
		if isequal(target, curNode)
			oriID = [oriID, node];
			break;
		end
		if node == size(totalID,1) %if suitable node doesn't found, return -1
			oriID = [oriID, -1];
		end
	end
end