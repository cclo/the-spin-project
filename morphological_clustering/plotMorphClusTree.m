function h = plotMorphClusTree(tree, labeled)
h = figure;
plot_tree(tree);
hold on;
grid on;
plot3(tree.X(1), tree.Y(1), tree.Z(1), '.','color',[1 0 1],'markersize',40);
cmap = colormap(jet);%[1 0 0;0 0 1;0 1 0;0 1 1;1 1 0; 1 0 1];
color_delta = floor(length(cmap)/length(labeled));
for label = 1:length(labeled)
	ID = labeled(label);
	subtree = getTerSubTree(tree, ID, labeled);
	plot_tree(subtree,cmap(label*color_delta,:));
	text(tree.X(ID), tree.Y(ID), tree.Z(ID), num2str(ID),'fontsize',15);
end