function label_temp = haveConvHull(tree, label_temp)
labeled = find(label_temp);
for label = 1:length(labeled);
	currentID = labeled(label);
	subtree = getTerSubTree(tree, currentID, labeled);
	try 
		[node vol] = convhulln([subtree.X subtree.Y subtree.Z],{'Qt'});
	catch 
		label_temp(currentID,1) = 0;
	end
end
		