% MORPHOLOGY_CLUSTERING_termiOnly devides a given tree into several subtrees base on
% their morphology.But this function dosen't care about the density and
% neuronpil innervated.
% Input:
%	tree: the tree structure desire to divide.
%	plotOption: 'true' shows the clustering result.
% Output: 
%	labeled: the ID of dividing points.
% -----------------------------------------------
% Author: Yi-Hsuan Lee
% Created: 2011/12/27, using Ubuntu Matlab R2009a

function [labeled record] = searchCleanSegment(tree, parameter)
%{
clear all;
close all;
cd /home/teresa/Dropbox/Lab/ADC/TREES1.14/
start_trees;
addpath('/home/teresa/Dropbox/Lab/ADC/MorphologyClustering/');
addpath('/home/teresa/Dropbox/Lab/ADC/MorphologyClustering/NeuropilMesh_Identify/NeuropilMesh_Identify/');
cd /home/teresa/Dropbox/Lab/ADC/MorphologyClustering/NeuropilMesh_Identify/NeuropilMesh_Identify/;
goInclude;
cd /home/teresa/Document/Lab/ADC/data/SWC/
tree = load_tree('5HT1bMARCM-F000024_seg001.swc');
plotOption = 'true';
% MED parameter
scanLength_par_up = 0.14;%branchLength/pathLengthMax;%0.07//2.5;
scanLength_par_low = 0.12;%branLengthMean/pathLengthMax;%0.06//1;
scanLength_delta = 0.005;%//0.05;
terminalRatio = 0; %0.15; %0.07 the lower terminalRatio makes it more difficult to label the current node
numTerminalThreshold = 0.05;%0.05
%}

% find max path length
pathLength = Pvec_tree(tree);
pathLengthMax = max(pathLength);

% dynamically adjust scan length
label_node = zeros(length(tree.dA), 1);
numBranch = length(find(T_tree(tree)));
branchMin = numBranch*parameter.numTerminalThreshold;
record = [];
tree_new = tree;
for scan = parameter.scanLength_par_up:-parameter.scanLength_delta:parameter.scanLength_par_low
	%scanLength = branLengthMean*scan;
	scanLength = scan*pathLengthMax;
	% calculate terminal difference ratio
	[label_temp record] = termDiffRatio(tree_new, scanLength, parameter.terminalRatio);
	% if there is no labeled point, continue
	if isempty(find(label_temp, 1))
		continue;
	end
	% check if each group has enough branches
	label_temp = countTerminal(tree, tree_new, label_temp, branchMin);
	%{
	% check if each group only innervate one brain region, if not, discard
	% it
	label_temp = calInnervateRatio(tree_new, label_temp, innervateThreshold);
	%}
	% if there is no labeled point, continue
	if isempty(find(label_temp, 1))
		continue;
	end
	% label in the complete tree
	labeled = find(label_temp);
	oriID = findID(tree, [tree_new.X(labeled), tree_new.Y(labeled), tree_new.Z(labeled)]);
	label_node(oriID) = 1;
	if ~isempty(labeled)
		subtree_disc = [];
		for label = 1:length(labeled)
			currentID = labeled(label);
			[sub subtree] = sub_tree(tree_new, currentID, 'none');
			subtree_disc = [subtree_disc, (find(sub))'];
		end
		tree_new = delete_tree(tree_new, subtree_disc);
	end
end
% check if each group can find corresponding subtree
%{
label_node = haveConvHull(tree, label_node);
%}
labeled = find(label_node);


