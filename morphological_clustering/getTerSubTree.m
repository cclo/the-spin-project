function [subtree savePoint] = getTerSubTree(tree, currentID, labeled)
% getTerSubTree return a substructure composed of nodes that use
% "currentID" as root and skip all the nodes in "labeled"
%
% Inputs
% -------
% - tree: neuron info
% - currentID: desired substructure root ID
% - labeled: other labeled points ID
% 
% Output
% ------
% - subtree: final substructure
% - savePoint: original ID of this substructure
%
% Example
% -------
% tree = load_tree('Cha-F-500093.swc');
% currenID = 48;
% labeled = [48 114];
% [subtree savePoint] = getTerSubTree(tree, currentID, labeled)
% subtree = 
%
%        dA: [37x37 double]
%         X: [37x1 double]
%         Y: [37x1 double]
%         Z: [37x1 double]
%         D: [37x1 double]
%         R: [37x1 double]
%         rnames: {'0'}
%         name: 'Cha-F-500093'
%
% savePoint = 
%         [1*37 double]
%   
tp = find(T_tree(tree));
rootPath = ipar_tree(tree);
rootPath = rootPath(tp,:);
savePoint = [];
for T = 1:length(tp)
	path = rootPath(T,:);
	[commonNode inxPath] = intersect(path, labeled); % check if the path contains labeled nodes
    % if currentID 
    % 1)happens to be one of the "commonNode", and  
    % 2)is the one nearest to the current terminal point, than preserve these points   
	if sum(double(commonNode == currentID))==1 && path(min(inxPath)) == currentID
		savePoint = [savePoint, path(1:min(inxPath))];
	end
end
savePoint = unique(savePoint); % some nodes migth be recorded several times
pointDiscard = setdiff((1:length(tree.dA)), savePoint); %find unrelated nodes
subtree = delete_tree(tree, pointDiscard, 'none'); % remove unrelated nodes

	