% This program performs morphology clustering to given trees.
% The whole procedure includes clean improbable branches, searching
% critical points, searching clean segments, and finally decides dividing
% points.
% INPUT:
% 
% morphoClust: morphology clustering parameters
% OUTPUT:
% subtrees: morphology clustering results
% h: figure handle

function [subtrees relationship h label] = morphologyClustering_debug(tree, morphoClust, drawOption_morphoClust)
    % morphology clustering
    labeled = cell(1,2);
    labeled{1,1} = findCriticalPoint(tree{1,2},morphoClust.cp.threshold, (1-morphoClust.cp.preserve)); % the label point selected by critical point
    labeled{1,2} = searchCleanSegment(tree{1,2}, morphoClust.cleanSeg); % the label point selected by clean segments
    labeled_temp = zeros(length(tree{1,2}.dA),1);
    labeled_temp([labeled{1,1}' labeled{1,2}']) = 1;
    numBranch = length(find(T_tree(tree{1,2})));
    branchMin = numBranch*morphoClust.numTerminalThreshold;
    labeled_temp = countTerminal(tree{1,2}, tree{1,2}, labeled_temp, branchMin);
	label = find(labeled_temp);
    % check if there is sequential relationship between clusters (only
    % check those having cp)
    relationship = zeros(1, length(label));
    if ~isempty(labeled{1,1}) && length(label)>2
        ipar = ipar_tree(tree{1,2});
        for lab = 1:length(labeled{1,1})
            point_tmp = labeled{1,1}(lab);
            for sub = 1:length(label)
                if ~isempty(find(ipar(label(sub),:) == point_tmp, 1))
                    relationship(sub) = lab;
                end
            end
        end
    end
    % label the label point on the original tree
    label = findID(tree{1,1}, [tree{1,2}.X(label), tree{1,2}.Y(label), tree{1,2}.Z(label)]);
    subtrees = cell(1,length(label)+1);
    % initialize subtree{1} region index
    subtrees{1} = tree{1,1};
    subtrees{1}.rnames = {'0','1'};
    subtrees{1}.R(1) = 2;
    subtrees{1}.R(2:end) = 1;
    % get terminal subtree(use original tree)
    for sub = 1:length(label)
        labeled_temp = label(sub);
        subtrees{1+sub} = getTerSubTree(tree{1,1}, labeled_temp, label);
    end 
    if isempty(label)
        subtrees{2} = tree {1,1};
    end
    % plot result
    if drawOption_morphoClust
        h = plotMorphClusTree(tree{1,1}, label);
    end
end
