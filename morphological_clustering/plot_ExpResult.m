% PLOT_EXPRESULT plot tree structure base on rname. Axon will be ploted in
% green, dendrite in blue.
% Input:
%	tree: tree structure loaded into trees toolbox.
% Output:
%	a tree plot handle
%--------------------------------------------------------------
% Author: Yi-Hsuan Lee
% Created: 2011/12/30, using Ubuntu Matlab R2009a

function h = plot_ExpResult(tree)
legend_string = {}; % for saving string to display as legend
rname = cellfun(@str2num, tree.rnames); 
parent = idpar_tree(tree);
child = (1:length(tree.dA))';
h_obj = []; % save object handles produced
h_select = []; % save the ID for each type's(soma, axon, dendrite) first object
% There are two kinds of region index: 1) follow original swc format, where
% 2 is axon, 3 is dndrite; 2) SPIN identified polarity: 20~23 is axon,
% 30~33 is dendrite
% if it is SPIN identified format
if sum(find(rname >= 20,1))
    RI_parent = fix(rname(tree.R(parent))/10);
    RI_child = fix(rname(tree.R(child))/10);
    consistPolarity = find((RI_parent - RI_child) == 0);
    axons = intersect(find(RI_child == 2), consistPolarity);
    dendrites = intersect(find(RI_child == 3), consistPolarity);
else
    % if it is original swc format
    RI_parent = rname(tree.R(parent));
    RI_child = rname(tree.R(parent));
    consistPolarity = find((RI_parent - RI_child) == 0);
    axons = intersect(find(RI_child == 2), consistPolarity);
    dendrites = intersect(find(RI_child == 3), consistPolarity);
end
soma = 1;
undefine = setdiff((1:length(RI_child)), [soma axons dendrites]);
hold on;
% plot soma
if ~isempty(soma)
    h_select = [h_select; length(h_obj)+1];
    h_obj = [h_obj; plot3(tree.X(soma), tree.Y(soma), tree.Z(soma), '.','color','r','markersize',40)];
    legend_string = [legend_string 'Soma'];
end
% plot axons
if ~isempty(axons)
    h_select = [h_select; length(h_obj)+1];
    X_axon = [tree.X(parent(axons)) tree.X(child(axons))]';
    Y_axon = [tree.Y(parent(axons)) tree.Y(child(axons))]';
    Z_axon = [tree.Z(parent(axons)) tree.Z(child(axons))]';
    h_obj = [h_obj; line(X_axon, Y_axon, Z_axon, 'color', [0 1 0], 'linewidth', 2)];
    legend_string = [legend_string 'Axon'];
end
% plot dendrites
if ~isempty(dendrites)
    h_select = [h_select; length(h_obj)+1];
    X_dendrite = [tree.X(parent(dendrites)) tree.X(child(dendrites))]';
    Y_dendrite = [tree.Y(parent(dendrites)) tree.Y(child(dendrites))]';
    Z_dendrite = [tree.Z(parent(dendrites)) tree.Z(child(dendrites))]';
    h_obj = [h_obj; line(X_dendrite, Y_dendrite, Z_dendrite, 'color', [0 0 1], 'linewidth', 2)];  
    legend_string = [legend_string 'Dendrite'];
end
% plot undefine
if ~isempty(undefine)
    X_undefine = [tree.X(parent(undefine)) tree.X(child(undefine))]';
    Y_undefine = [tree.Y(parent(undefine)) tree.Y(child(undefine))]';
    Z_undefine = [tree.Z(parent(undefine)) tree.Z(child(undefine))]';
    line(X_undefine, Y_undefine, Z_undefine, 'color', [0 0 0], 'linewidth', 2);
end
hold off;
h = gcf;
% only display legend for the selected objects
legend(h_obj(h_select), legend_string); 
%{
for node = 1:length(tree.dA)
    if parent(node) == 0
        soma = node;
        continue;
    end
	X = [tree.X(node) tree.X(parent(node))];
	Y = [tree.Y(node) tree.Y(parent(node))];
	Z = [tree.Z(node) tree.Z(parent(node))];
	if identity(node) == identity(parent(node)) && identity(node) ~= 0
		if identity(node) == 2
			color = 'g';
		elseif identity(node) == 3
			color = 'b';
        elseif identity(node) == 1
			color = 'r';
		end
	else
		color = 'k';
	end
	plot3(X, Y, Z,'color',color,'linewidth',2);
	hold on;
end
%}
% plot soma
% plot3(tree.X(soma), tree.Y(soma), tree.Z(soma), '.','color','r','markersize',40);

