function [label_temp record] = termDiffRatio(tree, scanLength, terminalRatio)
morphoInfo = getMorphoInfo(tree);
morphoInfo.bp = setdiff(morphoInfo.bp,1);% Soma shouldn't be taken as bp 
label_temp = zeros(length(tree.dA), 1);
record = [];
% calculate the ratio of terminalNumber difference between neighboring topological points
for ind = 1:length(morphoInfo.sortIndex)
	tempIndex = morphoInfo.sortIndex(ind);
	tempDiffRatio = 0; %for those don't have branch points within scanlength. assign [] will cause inaccurate judgement at line 33
	if length(find(morphoInfo.rootPath(tempIndex,:))) <= 1 % skip soma
		continue;
	end
	for node = 2:length(find(morphoInfo.rootPath(tempIndex,:)))
		tempNode = morphoInfo.rootPath(tempIndex,node);
		length_diff = morphoInfo.pathLength(tempIndex)-morphoInfo.pathLength(tempNode);
		% if the length difference exceeds scan length, than
		% break(since after this point, every point will exceed scan
		% length)
		if length_diff >= scanLength 
			if node == 2
				label_temp(tempIndex, 1) = 1;
			end
			break;
		end
		% if this point happens to be branch point, calculate
		% terminal number ratio
		if sum(double(morphoInfo.bp == tempNode))>0 
			% then compute their terminalNumber difference ratio.
			tempDiff = morphoInfo.terminalNumber(tempNode) - morphoInfo.terminalNumber(tempIndex);
			tempDiffRatio = [tempDiffRatio, tempDiff/morphoInfo.terminalNumber(tempIndex, 1)];
		end
	end
	% if terminalNumber difference ratio of nodes within scanLength is
	% smaller than terminalRatio, then we label this node temporarily
	if max(tempDiffRatio) <= terminalRatio 
		label_temp(tempIndex, 1) = 1;
		record = [record; tempIndex, max(tempDiffRatio), 1];
		continue;
	end
	record = [record; tempIndex, max(tempDiffRatio), 0];
end
