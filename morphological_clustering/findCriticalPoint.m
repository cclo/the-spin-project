% findCriticalPoint    find critical points in this neuron. 
%
% labeled = findCriticalPoint(tree,threshold_cp, threshold_lost)
%----------------------------------------------------------
%
% When doing morphological clustering, an important feature helping us to 
% visually distinguish one cluster from another is critical points.
% Critical points are points that indicate the split of the neuron trunk, 
% and these points could be detected by a sudden drop of terminal points
% followed by each node when arranging in descending order. The "drop" is
% defined by variable "threshold_cp". Only terminal points difference 
% larger than %threshold_cp*100 of the total terminal points will be recognized
% as a valid drop. Terminal points not followed by the critical points 
% can't be more than %threshold_lost*100 of terminal points, otherwise the 
% critical points are very likely to locate on the small branches other than
% the trunk.
% 
% Input
% -----
% - tree: neuron structure 
% - threshold_cp: criteria for defining a drop
% - threshold_lost: the tolerant percentage of terminal points not followed
%   by the critical point.  
%
% Output
% ------
% - labeled: the node ID of the critical points
%
% Example
% -------
%  tree = load_tree('sample.swc');
%  threshold_cp = 0.35;
%  threshold_lost = 0.15;
%  labeled = findCriticalPoint(tree,threshold_cp, threshold_lost);
%
function labeled = findCriticalPoint(tree,threshold_cp, threshold_lost)
info = getMorphoInfo(tree);
label_temp = zeros(length(tree.dA), 1);
bp_termiPoint = info.terminalNumber(info.bp);
[value index] = sort(bp_termiPoint,'descend');
for order = 1:length(value)-1
    if value(order)/value(1) < threshold_cp
        break;
    end
	terminalDiff = (value(order) - value(order+1));
	terminalDiffRatio = terminalDiff/value(1);
	if terminalDiffRatio > threshold_cp % if index(order) = criticalPoint
		label_temp(info.bp(index(order+1))) = 1;
        possibleNode = info.bp(index(value == terminalDiff));
        % label the point that don't pass info.bp(index(order+1)) (the
        % labeled point)
        for possNode = 1:length(possibleNode)
            node_tmp = possibleNode(possNode);
            if isempty(find(info.rootPath(node_tmp,:) == info.bp(index(order+1)), 1))
                label_temp(node_tmp) = 1;
            end
        end
        %{
        for possNode = 1:length(possibleNode)
            node_tmp = possibleNode(possNode);
            stopNode = find(info.rootPath(node_tmp,:) == info.bp(index(order)));
            if isempty(intersect(info.rootPath(node_tmp,2:stopNode-1), info.bp))
                label_temp(node_tmp) = 1;
            end
        end
        %}
    end
end
labeled = find(label_temp);

if ~isempty(labeled)
    count = 0;
    for num = 1:length(info.tp)
        if isempty(intersect(info.rootPath(info.tp(num), :), labeled))
            count = count+1;
        end
    end
    if (count/value(1)) > threshold_lost
        labeled = [];
    end
end
