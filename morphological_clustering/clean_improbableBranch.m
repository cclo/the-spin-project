function [tree_cleaned tree_trunk h] = clean_improbableBranch(tree, parameter, drawOption) 
% CLEAN_IMPROBABLEBRANCH clean artificial branches from image
% noise. 
% The reconstruction results are severely affected by image quality. To
% exlude the effect of noise, we delete branches that directly connected to
% the neuron trunck without further branching. 
% 
% Input:
% ---------
% - tree: the tree structure desiring to clean
% - parameter: structure variable containing
% -- parameter.times:: i.e. N_CleanTimes. See SPIN_OnlineResource_v0_1_2.pdf
%   section D for definition.
% -- parameter.deleteTerminal:: i.e. Th_RemoveLen. See SPIN_OnlineResource_v0_1_2.pdf
%   section D for definition.
% -- drawOption: plot the cleaned neuron or not
%
% Output:
% ----------
% - tree_cleaned: the resulting cleaned tree
% - trunk: the final trunk of the tree
% - h: figure handle 
%
% Example:
% ----------
%   tree = load_tree('sample.swc');
%   parameter.times = 3;
%   parameter.deleteTerminal = 0.37;
%   [tree_cleaned tree_trunk h] = clean_improbableBranch(tree, parameter,1);

% calculate branch length to define "short branch"
branchLength = branch_Length(tree);
length_threshold = max(branchLength)*0.95; 

% 1.find the trunk
% 1.1 find the outline of trunk: if the terminal length is shorter than
% length_threshold, then delete
tree_trunk = tree;
for times = 1:parameter.times
    tree_trunk = clean_tree_lengthOnly(tree_trunk, length_threshold, 'none');
end
% 1.2 fix trunk (delete terminal) start from shortest path ( to delete as long
% as possible)
tp = find(T_tree(tree_trunk));
pathLength = Pvec_tree(tree_trunk);
pathLength_tp = pathLength(tp);
[val inx] = sort(pathLength_tp);
tp_sort = [tree_trunk.X(tp(inx)) tree_trunk.Y(tp(inx)) tree_trunk.Z(tp(inx))];
for num = 1:length(inx)
    node_cur = findID(tree_trunk, tp_sort(num,:));
    ipar = ipar_tree(tree_trunk);
    bp = find(B_tree(tree_trunk));
    pathLength = Pvec_tree(tree_trunk);
    path = ipar(node_cur,:);
    pathLength_threshold = pathLength(node_cur)*(1-parameter.deleteTerminal);
    for node = 1:length(path)
        node_temp = path(node);
        % If the soma happened to be the terminal point, break;
        if node_temp == 1 && node == 1
            break;
        end
        pathLength_temp = pathLength(node_temp);
        if pathLength_temp < pathLength_threshold || ~isempty(intersect(node_temp,bp)) || node_temp == 1
            delete = path(1:node-1);
            tree_trunk = delete_tree(tree_trunk, delete);
            break;
        end
    end
end

% 2. delete artificial branches: delete branches connecting directly to the
% trunk without further branching
tp = find(T_tree(tree)); % terminal point
bp = find(B_tree(tree)); % branch point
ipar = ipar_tree(tree); % path to root
cp_trunk = find(C_tree(tree_trunk));
point_delete = [];
% check every tp
for num_tp = 1:length(tp)
	tp_temp = tp(num_tp);
	branch_temp = ipar(tp_temp,:);
	% find nearest bp
	for point = 1:length(branch_temp);
		point_temp = branch_temp(point);
		% if soma is on this branch, then break
		if point_temp == 1
			break;
		end
		% check if this is a branch point
		if ~isempty(find(bp == point_temp,1))
			% check if this point connects directly to the trunk
			trunkID = findID(tree_trunk, [tree.X(point_temp), tree.Y(point_temp), tree.Z(point_temp)]); 
			if trunkID == -1 || isempty(intersect(cp_trunk, trunkID))
				break;
            end
			point_delete = [point_delete branch_temp(1:point-1)];	
            break;
		end
	end
end
tree_cleaned = delete_tree(tree, point_delete);

%plot result
if drawOption
	h = figure;
    plot_tree(tree);
	plot_tree(tree_cleaned, [0 1 0]);
    plot_tree(tree_trunk, [1 0 0]);
	hold on;grid on;
	plot3(tree.X(1), tree.Y(1), tree.Z(1), '.', 'color', 'r', 'markersize', 30);
    legend( 'Deleted branched','Cleaned neuron', 'Trunk','Soma');   
else
    h = [];
end



   