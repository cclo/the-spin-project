% branch_Length   calculate branch lengths of the neuron.
%
% branchLength = branch_Length(tree)
% -------------------------------------------------------------------
%
% divide tree into branches(by branch points) and returns the branch lengths 
%
% Input
% -----
% - tree: neuron structure (which has been loaded into TREES toolbox) 
%
% Output
% ------
% - branchLength: all the branch lengths of the neuron
%
% Example
% -------
% tree = load_tree('sample.swc');
% branchLength = branch_Length(tree)
%
% 
function branchLength = branch_Length(tree)
pathLength = Pvec_tree(tree);
branchID = dissect_tree(tree);
branchLength = diff(pathLength(branchID), [], 2);