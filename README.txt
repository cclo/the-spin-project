SPIN Version 0.1.1 2014/01/14

Notes
-----
- Bug fix 
- (New function) goIdentifyPolarity_GUI: export user setting
- (New function) goManualDenoise: Add a button to skip the procedure of artificial branch removal


Description
-----------
- SPIN(A Method of Skeleton-based Polarity Identification for Neurons) is developed for identifying neuron polarity.


Installation
----------- 
- SPIN is an open-source software package available for download in the form of Matlab code at http://life.nthu.edu.tw/~lablcc/SPIN. 
- Recommended system requirements: Matlab 2009 or above; Windows 7, Mac 10.6.8 or Ubuntu 12.04 (or above).


Documentations
--------------
- Please see SPIN_Online_Resource.pdf for detailed instruction.


Dependencies
------------
SPIN depends on the following toolboxes and functions, including:
- TRESS toolbox (available at http://www.treestoolbox.org/).
  Note: Some functions in the TREES toolbox in the SPIN package were modified and new functions were added to fit the specific applications in SPIN.
- machine learning toolbox (available at http://neural.cs.nthu.edu.tw/jang/matlab/toolbox/machineLearning/).
  Note: Some functions in the  machine learning toolbox in the SPIN package were modified to fit the specific applications in SPIN.
- Select3d(available at http://www.codeforge.com/article/126126) , 
- uibutton (available at http://www.mathworks.com/matlabcentral/fileexchange/10743-uibutton-gui-pushbuttons-with-better-labels/content/uibutton.m),
- mtit(available at http://www.mathworks.com/matlabcentral/fileexchange/3218-mtit-a-pedestrian-major-title-creator/content/mtit.m) 
- data of sample neurons with skeleton data are available from Flycircuit database (http://www.flycircuit.tw/). 



License
-------
- SPIN is released under GNU General Public License (GPLv3), see license.txt.


Bug Reporting
-------------
- See SPIN_Online_Resource.pdf for authors information.

