function [ldaScore polarity] = judgePolarity(rawData, weight,identityOrder)
ldaScore = weight*rawData;
max_temp = max(ldaScore);
min_temp = min(ldaScore);
threshold = mean([max_temp min_temp]);
polarity = zeros(1,length(rawData(1,:)));
if length(ldaScore) > 1
    for sub = 1:length(rawData(1,:))
        if ldaScore(sub) > threshold
            polarity(sub) = identityOrder(1);
        else
            polarity(sub) = identityOrder(2);
        end
    end
end
%if there is 0 or 1 substructure, label the polarity as undefine(0)
if length(ldaScore) == 1
    polarity(1) = 0;
end