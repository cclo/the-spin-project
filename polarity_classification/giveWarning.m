% this function gives warning to identified results
% warning_dist = type I warning: if the identified result is conflict with its group
% characteristics
% warning_blackSheep = type II warning: if the clusters on the same side have different
% polarities

function morphoInfo = giveWarning(morphoInfo, classifierInfo)
% give warning if stansard deviation distance difference is smaller than 0
morphoInfo.resultEval.warning_dist = zeros(1,length(morphoInfo.ldaScore));
morphoInfo.resultEval.warning_blackSheep = zeros(1,length(morphoInfo.ldaScore));
morphoInfo.resultEval.warning = zeros(1,length(morphoInfo.ldaScore));
if length(morphoInfo.ldaScore) > 2
    % type I warning
    [c inx_max] = max(morphoInfo.ldaScore);
    [c inx_min] = min(morphoInfo.ldaScore);
    exclude = [inx_max, inx_min];
    for num = 1:length(morphoInfo.ldaScore)
        if ismember(num, exclude)
            continue;
        end
        % calculate z-score
        for class = 1:length(classifierInfo.class)
            morphoInfo.resultEval.dist2mean(num,class) = abs(morphoInfo.ldaScore(num) - classifierInfo.mean(class))/classifierInfo.std(class);
        end
        judgedID = find(classifierInfo.class == morphoInfo.output(num));   % find the identity index of current substructure
        anotherID = setdiff(1:length(classifierInfo.class), judgedID);     % find the identity index of another identity
        morphoInfo.resultEval.distDiff(num) = (morphoInfo.resultEval.dist2mean(num, judgedID)- morphoInfo.resultEval.dist2mean(num, anotherID));
        if morphoInfo.resultEval.distDiff(num)>0  
            %morphoInfo.resultEval.warning_dist(num) = 1;
            morphoInfo.resultEval.warning_dist(num) = 1;
            morphoInfo.resultEval.warning(num) = 1;
        end
    end
    % type II warning
    %{
    % don't give warning on extreme value
    [c inx_max] = max(DS_unknown.ldaScore(member));
    [c inx_min] = min(DS_unknown.ldaScore(member));
    exclude = member([inx_max, inx_min]);
    %}
    tempRelationship = morphoInfo.relationship;
    tempRelationship(morphoInfo.del_data-1) = [];
    for sub = 1:length(unique(tempRelationship))
        member_sub = find(tempRelationship == sub);
        if length(unique(morphoInfo.output(member_sub))) >= 2
            %morphoInfo.resultEval.warning_blackSheep(setdiff(member_sub, exclude)) = 1;
            morphoInfo.resultEval.warning_blackSheep(member_sub) = 1;
            morphoInfo.resultEval.warning(member_sub) = 1;
        end
    end
end
    
