% This function label identified identity to tree
% negative structure index means warning cases
function tree_sub = labelPolarity_modify(polarity, tree_sub, delEle)
polarity_belong = 1:length(tree_sub)-1;
polarity_belong(delEle-1) = [];
for sub = 1:length(polarity_belong)
    sub_current = polarity_belong(sub); % get current substrcture ID
    % check whether the region index has been added to tree.rnames
    regionInx = find(strcmp(num2str(polarity(sub)), tree_sub{1,1}.rnames)); 
    % if not, add new region index
    if isempty(regionInx)
        tree_sub{1,1}.rnames = [tree_sub{1,1}.rnames num2str(polarity(sub))];
        regionInx = length(tree_sub{1,1}.rnames);
        node = findID(tree_sub{1,1}, [tree_sub{1,sub_current+1}.X, tree_sub{1,sub_current+1}.Y, tree_sub{1,sub_current+1}.Z]); 
        tree_sub{1,1}.R(node) = regionInx; 
    else
        node = findID(tree_sub{1,1}, [tree_sub{1,sub_current+1}.X, tree_sub{1,sub_current+1}.Y, tree_sub{1,sub_current+1}.Z]); 
        tree_sub{1,1}.R(node) = regionInx; 
    end
    % add region index to each subtree
    tree_sub{1,sub_current+1}.rnames = {num2str(polarity(sub))};
    tree_sub{1,sub_current+1}.R = ones(size(tree_sub{1,sub_current+1}.R));
end
tree_sub{1,1}.R(1) = find(strcmp('1', tree_sub{1,1}.rnames));
%{
oriStructureInx = length(tree_sub{1,1}.rnames);
for sub = 1:length(polarity)
    tree_sub{1,1}.rnames{oriStructureInx+sub} = num2str(polarity(sub));
    node = findID(tree_sub{1,1}, [tree_sub{1,1+sub}.X, tree_sub{1,1+sub}.Y, tree_sub{1,1+sub}.Z]); 
    tree_sub{1,1}.R(node) = oriStructureInx+sub; 
end
%}