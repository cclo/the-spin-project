function scaledFeature = scaled(denominator, numerator)
denominator = repmat(denominator, 1, length(numerator(1,:)));
scaledFeature = numerator./denominator; 