% This function label identified identity to tree
function tree_sub = labelPolarity(polarity, tree_sub)
for sub = 1:length(polarity)
    tree_sub{1,1}.rnames{polarity(sub)+1} = num2str(polarity(sub));
    node = findID(tree_sub{1,1}, [tree_sub{1,1+sub}.X, tree_sub{1,1+sub}.Y, tree_sub{1,1+sub}.Z]); 
    tree_sub{1,1}.R(node) = (polarity(sub)+1); 
end
