% plotClassificationResult plots identified neuronal polarity results
% INPUT:
% tree_sub: neuron structure information, tree_sub{1,1}is complete neuron, the rest is corresponding substructures
% identificationPolarity: identified neuronal polarity of each substructure (tree_sub{1,2:end})
% warning: warning type of each substructure (tree_sub{1,2:end}) 
% OUPUT:
% h: figure handle
% h_obj: object handles that will display legend
% legend_string: corresponding string for labeling each object

% Result figure. the colors of backbone indicate different substructures (according to the result from morphological clustering); 
% the colors of dots represent identified neuronal polarity.
% soma: red([1 0 0])
% axon: green([0 1 0])
% dendrite: blue([0 0 1])

function [h h_obj legend_string] = plotClassificationResult(tree_sub, tree_answer, optNewfig, optLegend)
%{
optNewfig = 1;
tree_answer = tree{1};
%}
if optNewfig
    h = figure;
end
hold on;
cmap = setdiff(colormap(jet), eye(3), 'rows'); % preserve RGB for labeling polarity
colorDelta = floor(length(cmap)/(length(tree_sub)));
marker = {'.','*', 'p', 's'}; % nowarning use 'o', get warning use 'p'
legend_string = {'Soma'};
h_obj = []; % handles of objects that will show legend 
% plot soma and make correct legend
h_obj = [h_obj plot3(tree_sub{1,1}.X(1), tree_sub{1,1}.Y(1), tree_sub{1,1}.Z(1), '.','color','r','markersize',40)]; 

% if morpho clustering results in more than 2 clusters, than label axon/dendrite
tp = find(T_tree(tree_sub{1,1}));
if length(tree_sub)-1 >= 2 
    %{
    % plot classified result
    for sub = 2:2%length(tree_sub)
        tp = find(T_tree(tree_sub{1,sub}));
        color = colorMarker(identifiedPolarity(sub-1),:);
        markerType = warningtag(sub-1)+1;
        plot3(tree_sub{1,sub}.X(tp), tree_sub{1,sub}.Y(tp), tree_sub{1,sub}.Z(tp), marker{markerType},'color', color, 'markerfacecolor',color,'markersize',10);
    end
    %}
    regionInx = str2double(tree_sub{1,1}.rnames);
    %find axon: there are 2 kinds of axon: 20 (SPIN identified)  or 2
    %(experimental or handlabel results)
    axonId = find(regionInx == 20);
    if ~isempty(axonId)
        axon = intersect(find(tree_sub{1,1}.R == axonId), tp);
        h_obj = [h_obj plot3(tree_sub{1,1}.X(axon), tree_sub{1,1}.Y(axon), tree_sub{1,1}.Z(axon), marker{1},'color','g','markersize',30)]; 
        legend_string = [ legend_string 'Axon'];
    end
    axonId = find(regionInx == 2);
    if ~isempty(axonId)
        axon = intersect(find(tree_sub{1,1}.R == axonId), tp);
        h_obj = [h_obj plot3(tree_sub{1,1}.X(axon), tree_sub{1,1}.Y(axon), tree_sub{1,1}.Z(axon), marker{1},'color','g','markersize',30)]; 
        legend_string = [ legend_string 'Axon'];
    end
    %find axon warning I
    axonWarnId = find(regionInx == 21, 1);
    if ~isempty(axonWarnId)
        axonWarn = intersect(find(tree_sub{1,1}.R == axonWarnId), tp);
        h_obj = [h_obj plot3(tree_sub{1,1}.X(axonWarn), tree_sub{1,1}.Y(axonWarn), tree_sub{1,1}.Z(axonWarn), marker{2},'color','g','markersize',20)]; 
        legend_string = [ legend_string 'Axon(warning I)'];
    end
    %find axon warning II
    axonWarnId = find(regionInx == 22, 1);
    if ~isempty(axonWarnId)
        axonWarn = intersect(find(tree_sub{1,1}.R == axonWarnId), tp);
        h_obj = [h_obj plot3(tree_sub{1,1}.X(axonWarn), tree_sub{1,1}.Y(axonWarn), tree_sub{1,1}.Z(axonWarn), marker{3},'color','g','markersize',20)]; 
        legend_string = [ legend_string 'Axon(warning II)'];
    end
    %find axon warning I+II
    axonWarnId = find(regionInx == 23, 1);
    if ~isempty(axonWarnId)
        axonWarn = intersect(find(tree_sub{1,1}.R == axonWarnId), tp);
        h_obj = [h_obj plot3(tree_sub{1,1}.X(axonWarn), tree_sub{1,1}.Y(axonWarn), tree_sub{1,1}.Z(axonWarn), marker{4},'color','g', 'markerfacecolor', 'g', 'markersize',10)]; 
        legend_string = [ legend_string 'Axon(warning I + II)'];
    end
    % find dendrite
    dendriteId = find(regionInx == 30);
    if ~isempty(dendriteId)
        dendrite = intersect(find(tree_sub{1,1}.R == dendriteId), tp);
        h_obj = [h_obj plot3(tree_sub{1,1}.X(dendrite), tree_sub{1,1}.Y(dendrite), tree_sub{1,1}.Z(dendrite), marker{1},'color','b','markersize',30)]; 
        legend_string = [ legend_string 'Dendrite'];
    end
    dendriteId = find(regionInx == 3);
    if ~isempty(dendriteId)
        dendrite = intersect(find(tree_sub{1,1}.R == dendriteId), tp);
        h_obj = [h_obj plot3(tree_sub{1,1}.X(dendrite), tree_sub{1,1}.Y(dendrite), tree_sub{1,1}.Z(dendrite), marker{1},'color','b','markersize',30)]; 
        legend_string = [ legend_string 'Dendrite'];
    end
    % find dendrite warning I
    dendriteWarnId = find(regionInx == 31);
    if ~isempty(dendriteWarnId)
        dendriteWarn = intersect(find(tree_sub{1,1}.R == dendriteWarnId),tp);
        h_obj = [h_obj plot3(tree_sub{1,1}.X(dendriteWarn), tree_sub{1,1}.Y(dendriteWarn), tree_sub{1,1}.Z(dendriteWarn), marker{2},'color','b','markersize',20)]; 
        legend_string = [ legend_string 'Dendrite(warning I)'];
    end
    % find dendrite warning II
    dendriteWarnId = find(regionInx == 32);
    if ~isempty(dendriteWarnId)
        dendriteWarn = intersect(find(tree_sub{1,1}.R == dendriteWarnId),tp);
        h_obj = [h_obj plot3(tree_sub{1,1}.X(dendriteWarn), tree_sub{1,1}.Y(dendriteWarn), tree_sub{1,1}.Z(dendriteWarn), marker{3},'color','b','markersize',20)]; 
        legend_string = [ legend_string 'Dendrite(warning II)'];
    end
    % find dendrite warning I + II
    dendriteWarnId = find(regionInx == 33);
    if ~isempty(dendriteWarnId)
        dendriteWarn = intersect(find(tree_sub{1,1}.R == dendriteWarnId),tp);
        h_obj = [h_obj plot3(tree_sub{1,1}.X(dendriteWarn), tree_sub{1,1}.Y(dendriteWarn), tree_sub{1,1}.Z(dendriteWarn), marker{4},'color','b', 'markerfacecolor', 'b', 'markersize',10)]; 
        legend_string = [ legend_string 'Dendrite(warning I + II)'];
    end
    if ~isempty(tree_answer) % if there is experiment result then plot the backbone with the corresponding colors: axon=>g, dendrite=>b
        plot_ExpResult(tree_answer);
    else
        plot_tree(tree_sub{1,1});
        for sub = 2:length(tree_sub)
            h_obj = [h_obj plot_tree(tree_sub{sub}, cmap(colorDelta*(sub-1),:))];
            legend_string = [ legend_string strcat('subtree ', num2str(sub-1))];
        end
    end
    num_divide = length(tree_sub);
    % label cluster ID behind the dividing point
    for dp = 2:num_divide
        %dp_cur = findID(tree_sub{num}{1,1}, [tree_sub{num}{1,dp}.X(1), tree_sub{num}{1,dp}.Y(1),tree_sub{num}{1,dp}.Z(1)]);
        %text(tree_sub{num}{1,dp}.X(1), tree_sub{num}{1,dp}.Y(1),tree_sub{num}{1,dp}.Z(1), num2str(dp_cur), 'fontsize',20);
        text(tree_sub{1,dp}.X(1), tree_sub{1,dp}.Y(1),tree_sub{1,dp}.Z(1), num2str(dp-1), 'fontsize',20);        
    end
else
    plot_tree(tree_sub{1});
end
title(tree_sub{1,1}.name, 'fontsize', 12);
grid on;
if optLegend
    legend(h_obj, legend_string);
end
if ~optNewfig
    h = gcf;
end



%{
% plot type I warning
exclude = [];
member = find(warning.warning_dist);
%cellfun(@(x) plot_tree(x,[0 1 1]), tree_sub(member+1), 'UniformOutput', false);
cellfun(@(x) plot3(x.X(1), x.Y(1), x.Z(1),'.','color',[0 1 1], 'markersize',30), tree_sub(member+1), 'UniformOutput', false);
if isempty(member)
    exclude = [exclude find(strcmpi(legend_string,'type I warning')) ];
end
% plot type II warning
member = find(warning.warning_blackSheep);
cellfun(@(x) plot3(x.X(1), x.Y(1), x.Z(1),'.','color',[1 0 1], 'markersize',30), tree_sub(member+1), 'UniformOutput', false);
%cellfun(@(x) plot_tree(x,[1 0 1]), tree_sub(member+1), 'UniformOutput', false);
if isempty(member)
    exclude = [exclude find(strcmpi(legend_string,'type II warning'))];
end
 %legend(legend_string(setdiff(1:length(legend_string), exclude)));
%}