function GUI_handLabel( varargin )
% GUI_HANDLABEL is a function contain action script of each button in goHandLabel.
%
%	GUI_handLabel('initialize'):
%		read 'neuronList.txt' file and draw first neuron and tag it.
%	GUI_handLabel('previous'):
%		draw previous neuron if the current one is not the first neuron.
%	GUI_handLabel('next'):
%		draw next neuron if the current neuron is not the last neuron.
%	GUI_handLabel('axon'):
%		label selected cluster as axons.
%	GUI_handLabel('dendrite'):
%		label selected cluster as dendrite
%	GUI_handLabel('replot'):
%		reset the figure, axon, dendrite and cluster state of neuron.
%	GUI_handLabel('export'):
%       export one complete neuron and several substructures in swc format 
%       with region index(axon, dendrite, or soma)
%   GUI_handLabel('click'):
%        plot the selected cluster.
%   GUI_handLabel('findCloseBranchNode'):
%       find the nearest branch node around the click position by "select3d".
%   GUI_handLabel('plot_neuron'):
%       plot neuron 
%
%	Require:
%		select3d, readTraceLine_tree, getTerSubTree
%
%	---------
%	Author : Yen-Nan Lin
%	Created: 2011-05-18, using mac Matlab R2009a
%	History:
%	2011-05-26: add display neuron name, and export each neurite swc file

if length(varargin) == 1
	feval( varargin{ 1 } );
	return;
end
end

function initialize 
guiHandle = gcf;
set(guiHandle, 'tag', 'GUI_handLabel');
data.srcDir = './SWC_cleaned/'; % swc source directory
data.tarDir = './SWC_labeled/'; % target directory to put resulting files
if ~isdir(data.tarDir)
    mkdir(data.tarDir);
end 
data.nameList = textread('./fileList.txt', '%s'); % read filelist
data.iName = 1;
data.neuron = readTraceLine_tree(strcat(data.srcDir, data.nameList{data.iName})); % load neuron data
figure;figureHandle = plot_ExpResult(data.neuron); % plot neuron
fprintf('%d\t%s\n', data.iName, data.neuron.name);
set(figureHandle, 'tag', '_morphology_', 'windowbuttondownfcn', 'GUI_handLabel(''click'')');
setappdata(guiHandle, 'data', data);
end

function previous
guiHandle = findobj('tag', 'GUI_handLabel');
data = getappdata(guiHandle, 'data');
data.iName = data.iName - 1;
if data.iName < 1
	disp('It is the first neuron!');	
    return;
end
figureHandle = findobj('tag', '_morphology_');
if ~isempty(figureHandle)
	close(figureHandle);
end
data.neuron = [];
data.neuron = readTraceLine_tree(strcat(data.srcDir, data.nameList{data.iName}));
figure;figureHandle = plot_ExpResult(data.neuron);
fprintf('%d\t%s\n', data.iName, data.neuron.name);
set(figureHandle, 'tag', '_morphology_', 'windowbuttondownfcn', 'GUI_handLabel(''click'')');
setappdata(guiHandle, 'data', data);
end

function next
guiHandle = findobj('tag', 'GUI_handLabel');
data = getappdata(guiHandle, 'data');
data.iName = data.iName + 1;
if data.iName > length(data.nameList)
	disp('It is the last neuron!');	
    return
end
figureHandle = findobj('tag', '_morphology_');
if ~isempty(figureHandle)
	close(figureHandle);
end
data.neuron = [];                                                                                                                                             
data.neuron = readTraceLine_tree(strcat(data.srcDir, data.nameList{data.iName}));
figure;figureHandle = plot_ExpResult(data.neuron);
fprintf('%d\t%s\n', data.iName, data.neuron.name);
set(figureHandle, 'tag', '_morphology_', 'windowbuttondownfcn', 'GUI_handLabel(''click'')');
setappdata(guiHandle, 'data', data);
end

function axon
guiHandle = findobj('tag', 'GUI_handLabel');
data = getappdata(guiHandle, 'data');
if ~isfield(data.neuron, 'axon')
	data.neuron.axon = [];
end
% collect nodes that will be labeled as axon
data.neuron.axon = [data.neuron.axon data.neuron.nodeID{end}]; 
%data.neuron.RIorder = [data.neuron.RIorder 2];
data.neuron.RIorder(length(data.neuron.nodeID)) = 2;
textHandle = findobj('tag', 'text');
set(textHandle, 'FontSize', 16, 'color', 'r');
disp( ['Label ' int2str( data.neuron.dp(end) ) ' node as axon!'] );
setappdata(guiHandle, 'data', data);
end

function dendrite
guiHandle = findobj('tag', 'GUI_handLabel');
data = getappdata(guiHandle, 'data');
if ~isfield(data.neuron, 'dendrite')
	data.neuron.dendrite = [];
end
% collect nodes that will be labeled as dendrite
data.neuron.dendrite = [data.neuron.dendrite data.neuron.nodeID{end}]; 
%data.neuron.RIorder = [data.neuron.RIorder 3];
data.neuron.RIorder(length(data.neuron.nodeID)) = 3;
textHandle = findobj('tag', 'text');
set(textHandle, 'FontSize', 16, 'color', 'b');
disp( ['Label ' int2str( data.neuron.dendrite(end) ) ' node as dendrite!'] );
setappdata(guiHandle, 'data', data);
end

function replot
guiHandle = findobj('tag', 'GUI_handLabel');
data = getappdata(guiHandle, 'data');
figureHandle = findobj('tag', '_morphology_');
if ~isempty(figureHandle)
	close(figureHandle);
end
data.neuron = [];                                                                                                                                             
data.neuron = readTraceLine_tree(strcat(data.srcDir, data.nameList{data.iName}));
figure;figureHandle = plot_ExpResult(data.neuron);
set(figureHandle, 'tag', '_morphology_', 'windowbuttondownfcn', 'GUI_handLabel(''click'')');
setappdata(guiHandle, 'data', data);
end

function export
guiHandle = findobj('tag', 'GUI_handLabel');
data = getappdata(guiHandle, 'data');
% initialize the region index of complete tree
somaInx = find(strcmp('1', data.neuron.rnames));
soma = data.neuron.R == somaInx;
data.neuron.rnames = {'0', '1'};
data.neuron.R = ones(size(data.neuron.R));
data.neuron.R(soma) = 2;
% if this neuron has axon, create a region name "2" for it
if isfield(data.neuron, 'axon')
    data.neuron.rnames{end+1} = '2';
    % label axon nodes as axon 
    data.neuron.R(data.neuron.axon) = length(data.neuron.rnames);   
end
% if this neuron has dendrites, create a region name "3" for it
if isfield(data.neuron, 'dendrite')
    data.neuron.rnames{end+1} = '3';
    % label axon nodes as axon
    data.neuron.R(data.neuron.dendrite) = length(data.neuron.rnames);
end
% label regio index for each substructure and export 
index_temp = 0; %count substructures having region index(for naming)
for isub = 1:length(data.neuron.subtrees)
    if data.neuron.RIorder(isub) == 2
        % Set the region index
        data.neuron.subtrees{isub}.rnames = {'2'};
        data.neuron.subtrees{isub}.R = ones(size(data.neuron.subtrees{isub}.R)); 
        index_temp = index_temp +1;
    end
    if data.neuron.RIorder(isub) == 3
        % Set the region index
        data.neuron.subtrees{isub}.rnames = {'3'};
        data.neuron.subtrees{isub}.R = ones(size(data.neuron.subtrees{isub}.R)); 
        index_temp = index_temp +1;
    end
    if data.neuron.RIorder(isub) == 0
        continue;
    end
    fileName = strcat(data.tarDir, data.neuron.name, '_', num2str(index_temp),'.swc');
    swc_tree(data.neuron.subtrees{isub}, fileName);
end
swc_tree(data.neuron, strcat(data.tarDir,data.neuron.name,'.swc'));
disp(['Export ' data.neuron.name '.swc file!'])
setappdata(guiHandle, 'data', data);
end

function click
guiHandle = findobj('tag', 'GUI_handLabel');
data = getappdata(guiHandle, 'data');
[ clickPosition, vertexPosition ] = select3d;
if isempty(vertexPosition)
	return;
end
hold on;
% initialize field
if ~isfield(data.neuron, 'dp') % dividing point
	data.neuron.dp = [];
end
if ~isfield(data.neuron, 'nodeID');
    data.neuron.nodeID = {};
end
if ~isfield(data.neuron, 'subtrees');
    data.neuron.subtrees = {};
end
if ~isfield(data.neuron, 'RIorder');
    data.neuron.RIorder = []; % RI = region index; record the RI of each subtree
end
closeBranchNode = findCloseBranchNode(data.neuron, vertexPosition);
data.neuron.dp = [ data.neuron.dp, closeBranchNode ]; % add closeBranchNode into dividing point list
[temp_subtree temp_ID] = getTerSubTree(data.neuron, data.neuron.dp(end), data.neuron.dp); 
data.neuron.subtrees = [ data.neuron.subtrees temp_subtree];
data.neuron.nodeID = [data.neuron.nodeID temp_ID];
fprintf('%d\n',length(data.neuron.nodeID));
% plot neuron with substructures
clf;
plot_ExpResult(data.neuron);
cmap = colormap(spring(length(data.neuron.dp))); % get color map
for isub = 1:length(data.neuron.dp)
    hp_temp = plot_tree(data.neuron.subtrees{isub}, cmap(isub,:));
    set(hp_temp, 'linewidth',1.5);
end
branchNodePosition = [data.neuron.X(closeBranchNode) data.neuron.Y(closeBranchNode) data.neuron.Z(closeBranchNode)];
text(branchNodePosition(1), branchNodePosition(2), branchNodePosition(3), ...
	int2str( data.neuron.dp(end) ), 'FontSize', 14, 'tag', 'text' );
hold off;
setappdata(guiHandle, 'data', data);
end

function [ closeBranchNode ] = findCloseBranchNode( neuron, vertexPosition )
minDistance = inf;
closeBranchNode = -1;
for iBranchNode = 1:length(neuron.bp)
	branchNodeId = neuron.bp(iBranchNode);
	branchNodePosition = [neuron.X(branchNodeId) neuron.Y(branchNodeId)  neuron.Z(branchNodeId)];
	testDistance = norm(branchNodePosition - vertexPosition, 2);
	if testDistance < minDistance
		closeBranchNode = branchNodeId;
		minDistance = testDistance;
	end
end
end

function HP = plot_neuron(neuron, color, linewidth)
if nargin == 1 || isempty(color) 
    color = [0 0 0];
    linewidth = 1;
end
if nargin == 2 
    linewidth = 1;
end
hold on;
hp_temp = plot_tree(neuron, color, [], [], [],  '-3l');
set(hp_temp, 'linewidth',linewidth);
plot3(neuron.X(1), neuron.Y(1), neuron.Z(1), '.', 'color', 'r', 'markersize', 30);
title(neuron.name);
HP = gcf;
hold off
end

