function [ neuron ] = readTraceLine_tree( recordName )
% READTRACELINE read tracing line data and output its content in structure.
% INPUT 
%   recordName: neuron name without file extensions, ex: 'Cha-F-500093.swc'
%
% OUTPUT
%	neuron.bp         : ID of branch point.
%	neuron.tp         : ID of terminal point.
%   neuron.cp         : ID of continuous point.
%	neuron.NODE       : should be an Nx3 or Nx4 matrix with the format 
%                    [ID X Y] or [ID X Y Z] where ID is an integer, 
%                    and X, Y, Z are cartesian position coordinates,
%                    for searching shortest path
%   neuron.SEGMENT    : should be an Mx3 matrix with the format [ID N1 N2]
%                    where ID is an integer, and N1, N2 correspond to node 
%                    IDs from NODES list such that there is an [undirected]
%                    edge/segment between node N1 and node N2, 
%                    for searching the shortest path
%   neuron.pathLength : path length from current point to root
%   neuron.path2root  : path indices from current point to root
%   For the rest of fileds, see 'load_tree' 
%
% EXAMPLE
% tree = readTraceLine_tree('Cha-F-500093')
% tree = 
%
%            dA: [148x148 double]
%             X: [148x1 double]
%             Y: [148x1 double]
%             Z: [148x1 double]
%             D: [148x1 double]
%             R: [148x1 double]
%        rnames: {'0'  '1'  '2'  '3'}
%          name: 'Cha-F-500093'
%            bp: [16x1 double]
%            tp: [17x1 double]
%            cp: [115x1 double]
%          NODE: [148x4 double]
%       SEGMENT: [147x3 double]
%    pathLength: [148x1 double]
%     path2root: [148x59 double]
%
%	---------
%	Author : Yi-Hsuan Lee
%	Created: 2012-09-13, using Ubuntu 10.04 Matlab R2009a

neuron = load_tree(strcat(recordName, '.swc')); 
neuron.bp = find(B_tree(neuron));
neuron.tp = find(T_tree(neuron));
neuron.cp = find(C_tree(neuron));
neuron.NODE = [(1:length(neuron.dA))' neuron.X neuron.Y neuron.Z];
parent = idpar_tree(neuron);
child = 1:length(neuron.dA);
neuron.SEGMENT = [(1:length(neuron.dA)-1)' parent(2:end) child(2:end)'];
neuron.pathLength = Pvec_tree(neuron);
neuron.path2root = ipar_tree(neuron);
end
