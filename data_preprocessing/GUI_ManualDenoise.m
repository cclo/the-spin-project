function GUI_ManualDenoise( varargin )
% GUI_ManualDenoise is a function contain action script of each buttom in goManualDenoise.
%	GUI_ManualDenoise('initialize'):
%		read 'neuronList.txt' file and draw first neuron and tag it.
%	GUI_ManualDenoise('exportAll'):
%       skip manual denoise and export all the files to "SWC_cleaned"
%       directory for further processing.
%	GUI_ManualDenoise('previous'):
%		draw previous neuron if it is not the first neuron.
%	GUI_ManualDenoise('next'):
%		draw next neuron if it is not the last neuron.
%	GUI_ManualDenoise('clean'):
%		clean unwanted branches which has beeb identified as noise manually
%	GUI_ManualDenoise('replot'):
%       reset the neuron        
%	GUI_ManualDenoise('export'):
%		export the cleaned neuron in swc format.
%   GUI_ManualDenoise('click'):
%       plot the selected path.
%   GUI_ManualDenoise('findCloseNode'):
%       find the nearest node in the neuron by "select3d".
%   GUI_ManualDenoise('plot_neuron'):
%       plot neuron 
%	Require:
%		select3d
%
%	---------
%	Author : Yi-Hsuan Lee, Yen-Nan Lin
%	Created: 2011-05-18, using mac Matlab R2009a. Develop for manually
%	label polarity.
%	History:
%	2011-05-26: add display neuron name, and export each neurite swc file
%   2012-10-22: modified as manually denoise GUI tool for skeleton 
%   neuron ,using windows7 Matlab R2012a
%   
if length(varargin) == 1
	feval( varargin{ 1 } );
	return;
end
end

function initialize 
guiHandle = gcf;
set(guiHandle, 'tag', 'GUI_handLabel');
data.tarDir = './SWC_cleaned/';
data.srcDir = './swc_rawdata/';
if ~isdir(data.tarDir)
    mkdir(data.tarDir);
end 
data.nameList = textread('./fileList.txt', '%s');
data.iName = 1;
data.neuron = readTraceLine_tree(strcat(data.srcDir, data.nameList{data.iName}));
figure;figureHandle = plot_ExpResult(data.neuron);
fprintf('%d\t%s\n', data.iName, data.neuron.name);
set(figureHandle, 'tag', '_morphology_', 'windowbuttondownfcn', 'GUI_ManualDenoise(''click'')');
setappdata(guiHandle, 'data', data);
end

function exportAll 
guiHandle = findobj('tag', 'GUI_handLabel');
data = getappdata(guiHandle, 'data');
disp('Export all the neurons!');
for i = 1:length(data.nameList)
    copyfile(strcat(data.srcDir, data.nameList{i}, '.swc'), ...
        strcat(data.tarDir, data.nameList{i}, '.swc'));
end
end

function previous
guiHandle = findobj('tag', 'GUI_handLabel');
data = getappdata(guiHandle, 'data');
data.iName = data.iName - 1;
if data.iName < 1
	disp('It is the first neuron!');	
    return;
end
figureHandle = findobj('tag', '_morphology_');
if ~isempty(figureHandle)
	close(figureHandle);
end
data.neuron = [];
data.neuron = readTraceLine_tree(strcat(data.srcDir, data.nameList{data.iName}));
figure;figureHandle = plot_ExpResult(data.neuron);
fprintf('%d\t%s\n', data.iName, data.neuron.name);
set(figureHandle, 'tag', '_morphology_', 'windowbuttondownfcn', 'GUI_ManualDenoise(''click'')');
setappdata(guiHandle, 'data', data);
end

function next
guiHandle = findobj('tag', 'GUI_handLabel');
data = getappdata(guiHandle, 'data');
data.iName = data.iName + 1;
if data.iName > length(data.nameList)
	disp('It is the last neuron!');	
    return
end
figureHandle = findobj('tag', '_morphology_');
if ~isempty(figureHandle)
	close(figureHandle);
end
data.neuron = [];
data.neuron = readTraceLine_tree(strcat(data.srcDir, data.nameList{data.iName})); %load neuron data
figure;figureHandle = plot_ExpResult(data.neuron);
fprintf('%d\t%s\n', data.iName, data.neuron.name);
set(figureHandle, 'tag', '_morphology_', 'windowbuttondownfcn', 'GUI_ManualDenoise(''click'')');
setappdata(guiHandle, 'data', data);
end

function clean
% add unwanted nodes to deleting list
guiHandle = findobj('tag', 'GUI_handLabel');
data = getappdata(guiHandle, 'data');
if ~isfield(data.neuron, 'clean')
	data.neuron.clean = [];
end
% find deleting nodes
selectedBp = intersect(data.neuron.path, data.neuron.bp); % the branch point included in the selected deleting path
endPoint = max(data.neuron.tempNode); % the end point in the selected path
for iTp = 1:length(data.neuron.tp)
    tp_temp = data.neuron.tp(iTp);
    tp_path = data.neuron.path2root(tp_temp,:);
    bp_overlap = intersect(selectedBp, tp_path); %check if current path containing selected branch points 
    % find those have the selected branch point but don't have selected
    % latest cp
    if ~isempty(bp_overlap) && isempty(intersect(endPoint, tp_path))
        for ipath = 1:length(tp_path)
            if ~isempty(find(bp_overlap == tp_path(ipath), 1))
                inx = ipath;
                data.neuron.clean = [data.neuron.clean tp_path(1:inx-1)];
                break;
            end
        end
    end    
end
data.neuron.cleanNeuron = delete_tree(data.neuron, data.neuron.clean);
old_hp = findobj('tag', '_morphology_');
clf(old_hp);
figure(old_hp); %set old_hp as current figure
%plot_neuron(data.neuron, 'm', 2);
plot_ExpResult(data.neuron.cleanNeuron);
view([data.az,data.el]);
data.neuron.path = [];
data.neuron.tempNode = [];
setappdata(guiHandle, 'data', data);
end

function replot
guiHandle = findobj('tag', 'GUI_handLabel');
data = getappdata(guiHandle, 'data');
figureHandle = findobj('tag', '_morphology_');
if ~isempty(figureHandle)
	close(figureHandle);
end
data.neuron = [];
data.neuron = readTraceLine_tree(strcat(data.srcDir, data.nameList{data.iName}));
figure;figureHandle = plot_ExpResult(data.neuron);
set(figureHandle, 'tag', '_morphology_', 'windowbuttondownfcn', 'GUI_ManualDenoise(''click'')');
setappdata(guiHandle, 'data', data);
end

function export
guiHandle = findobj('tag', 'GUI_handLabel');
data = getappdata(guiHandle, 'data');
if ~isfield(data.neuron, 'cleanNeuron')
    data.neuron.cleanNeuron = data.neuron;
end
swc_tree(data.neuron.cleanNeuron, strcat(data.tarDir,data.neuron.name,'.swc'));
disp(['Export ' data.neuron.name '.swc file!'])
setappdata(guiHandle, 'data', data);
end

function click
guiHandle = findobj('tag', 'GUI_handLabel');
data = getappdata(guiHandle, 'data');
[ clickPosition, vertexPosition ] = select3d;
if isempty(vertexPosition)
	return;
end
if ~isfield(data.neuron, 'path')
	data.neuron.path = [];
end
if ~isfield(data.neuron, 'tempNode')
	data.neuron.tempNode = [];
end
closeNode = findCloseNode(data.neuron, vertexPosition);
data.neuron.tempNode = [data.neuron.tempNode closeNode];
% the number of data.neuron.tempNode sould be less than 2. If not, compare the
% distance between node 1,3 and node 2,3, and preserve the shorter one.
if length(data.neuron.tempNode) == 3
    dist_1stPoint = abs(data.neuron.pathLength(data.neuron.tempNode(1)) - data.neuron.pathLength(data.neuron.tempNode(3)));
    dist_2ndPoint = abs(data.neuron.pathLength(data.neuron.tempNode(2)) - data.neuron.pathLength(data.neuron.tempNode(3)));
    [C I] = min([dist_1stPoint, dist_2ndPoint]);
    data.neuron.tempNode(I) = [];
end
% replot current situation
[data.az,data.el] = view; % record current viewpoint
%{
clf;
if isfield(data.neuron, 'cleanNeuron')
    %plot_neuron(data.neuron, 'm', 2);
    plot_ExpResult(data.neuron.cleanNeuron);
else
    plot_ExpResult(data.neuron);
end
%}
hold on;
plot3(data.neuron.X(data.neuron.tempNode), data.neuron.Y(data.neuron.tempNode), data.neuron.Z(data.neuron.tempNode), '.', 'color', 'g', 'markersize', 20); 
view([data.az,data.el]); % record current viewpoint
for num = 1:length(data.neuron.tempNode)
    text(data.neuron.X(data.neuron.tempNode(num)), data.neuron.Y(data.neuron.tempNode(num)), data.neuron.Z(data.neuron.tempNode(num)), ...
	int2str(data.neuron.tempNode(num)), 'FontSize', 14, 'tag', 'text' );
end
if length(data.neuron.tempNode) == 2
    [HP data.neuron.path] = plotsect_tree(data.neuron, sort(data.neuron.tempNode), [1 0 0]);
    set(HP, 'linewidth',5);    
end
hold off;
setappdata(guiHandle, 'data', data);
end

function [ closeNode ] = findCloseNode( neuron, vertexPosition )
minDistance = inf;
closeNode = -1;
%Find the closest node
for iNode = 1:length(neuron.X)
	nodePosition = [neuron.X(iNode) neuron.Y(iNode) neuron.Z(iNode)];
	testDistance = norm(nodePosition - vertexPosition, 2);
	if testDistance < minDistance
		closeNode = iNode;
		minDistance = testDistance;
	end
end
%{
%Find the closest continuous point
cNode = neuron.cp;
for iNode = 1:length(cNode)
	nodeId = cNode(iNode);
	nodePosition = [neuron.X(nodeId) neuron.Y(nodeId) neuron.Z(nodeId)];
	testDistance = norm(nodePosition - vertexPosition, 2);
	if testDistance < minDistance
		closeCNode = nodeId;
		minDistance = testDistance;
	end
end
%}
end

function HP = plot_neuron(neuron, color, linewidth)
if nargin == 1 || isempty(color) 
    color = [0 0 0];
    linewidth = 3;
end
if nargin == 2 
    linewidth = 3;
end
hold on;
hp_temp = plot_tree(neuron, color, [], [], [],  '-3l');
set(hp_temp, 'linewidth',linewidth);
plot3(neuron.X(1), neuron.Y(1), neuron.Z(1), '.', 'color', 'r', 'markersize', 30);
title(neuron.name);
HP = gcf;
hold off
end

