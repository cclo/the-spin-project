function meanTpR = meanTpRadius(tree_sub)
meanTpR = zeros(1, length(tree_sub));
for iSub = 1:length(tree_sub)
    tp = T_tree(tree_sub{iSub});
    if iSub == 1
        meanTpR(iSub) = max(tree_sub{iSub}.D(tp))/2;
    else
        meanTpR(iSub) = mean(tree_sub{iSub}.D(tp))/2;
    end
end