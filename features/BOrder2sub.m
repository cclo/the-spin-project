% BOrder2sub calculates the branch order from soma to substructure root
function BO = BOrder2sub(tree_sub)
BO = zeros(1, length(tree_sub));
BO_complete = BO_tree(tree_sub{1});
for iSub = 1:length(tree_sub)
    % For complete neuron (tree_sub{1}), use the maximun branch order in this neuron 
    if iSub == 1
        BO(iSub) = max(BO_complete);
    else
        % find the node ID of substructure root in complete neuron
        oriID = findID(tree_sub{1},...
            [tree_sub{iSub}.X(1) tree_sub{iSub}.Y(1) tree_sub{iSub}.Z(1)]); 
        BO(iSub) = BO_complete(oriID);
    end
end