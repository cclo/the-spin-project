function wd = WDRatio(tree_sub)
wd = zeros(1, length(tree_sub));
for iSub = 1:length(tree_sub)
    d = (max(tree_sub{iSub}.Z) - min(tree_sub{iSub}.Z));
    % if denominator is 0, assign wh = 0
    if d == 0
        wd(iSub) = 0;
    else
         w = (max(tree_sub{iSub}.X) - min(tree_sub{iSub}.X));
         wd(iSub) = w/d;
    end
end