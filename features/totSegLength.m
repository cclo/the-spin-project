% calculate total segment length
function allLength = totSegLength(tree_sub)
allLength = zeros(1, length(tree_sub));
for iSub = 1:length(tree_sub)
    allLength(iSub) = sum(len_tree(tree_sub{iSub}));
end