function meanR = meanRadius(tree_sub)
meanR = zeros(1, length(tree_sub));
for iSub = 1:length(tree_sub)
    if iSub == 1
        meanR(iSub) = max(tree_sub{iSub}.D)/2;
    else
        meanR(iSub) = mean(tree_sub{iSub}.D)/2;
    end
end