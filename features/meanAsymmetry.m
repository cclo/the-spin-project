function meanAsym = meanAsymmetry(tree_sub)
meanAsym = zeros(1, length(tree_sub));
for iSub = 1:length(tree_sub)
    bp = B_tree(tree_sub{iSub});
    asym = asym_tree(tree_sub{iSub});
    meanAsym(iSub) = mean(asym(bp));        
end