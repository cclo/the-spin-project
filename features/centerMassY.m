function cmY = centerMassY(tree_sub)
cmY = zeros(1, length(tree_sub));
for iSub = 1:length(tree_sub)
    cmY(iSub) = mean(tree_sub{iSub}.Y);
end