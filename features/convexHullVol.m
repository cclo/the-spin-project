function cHullvol = convexHullVol(tree_sub)
cHullvol = zeros(1, length(tree_sub));
for iSub = 1:length(tree_sub)
    try
        [khull cHullvol(iSub)] = convhull([tree_sub{iSub}.X,...
                                  tree_sub{iSub}.Y,...
                                  tree_sub{iSub}.Z]);
    catch err
        if (strcmp(err.identifier,'MATLAB:convhull:NotEnoughPtsConvhullErrId'))
            msg = ['Error computing the convex hull. Not enough unique points specified. Use NaN to replace. '...
                'Check ' tree_sub{1}.name ' substructure # ' num2str(iSub)];
            warning(msg);
            cHullvol(iSub) = NaN;
        end
    end
end