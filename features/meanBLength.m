function meanBL = meanBLength(tree_sub)
meanBL = zeros(1, length(tree_sub));
for iSub = 1:length(tree_sub)
    sect = dissect_tree(tree_sub{iSub});
    pLen = Pvec_tree(tree_sub{iSub});
    bLen = diff(pLen(sect), [], 2);
    % I don't know why the constrain 0.2 is needed. See "stats_tree" in
    % TREES toolbox
    meanBL(iSub) = mean(bLen(bLen>0.2)); 
end