function meanVor = meanVoronoi(tree_sub)
meanVor = zeros(1, length(tree_sub));
for iSub = 1:length(tree_sub)
    bp = B_tree(tree_sub{iSub});
    tp = T_tree(tree_sub{iSub});
    BTp = find(bp | tp);
    dhull = hull_tree(tree_sub{iSub}, [], [], [], [], 'none');
    points = dhull.vertices;
    [ HP VO KK vol] = vhull_tree(tree_sub{iSub},...
                                 [], points, BTp, [], 'none');
    meanVor(iSub) = mean(vol);
end