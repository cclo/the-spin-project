function cmX = centerMassX(tree_sub)
cmX = zeros(1, length(tree_sub));
for iSub = 1:length(tree_sub)
    cmX(iSub) = mean(tree_sub{iSub}.X);
end