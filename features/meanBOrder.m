% meanBOrder gets branch orders of topological points for each subtree
function meanBO = meanBOrder(tree_sub)
meanBO = zeros(1, length(tree_sub));
for iSub = 1:length(tree_sub)
    % get topological points
    bp = B_tree(tree_sub{iSub}); % branch point
    tp = T_tree(tree_sub{iSub}); % terminal point
    BTp = bp | tp; % terminal + branch point = topological point
    % count branch order
    BO = BO_tree(tree_sub{iSub}); 
    meanBO(iSub) = mean(BO(BTp));
end