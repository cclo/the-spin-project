function meanRatio = meanPEucRatio(tree_sub)
meanRatio = zeros(1, length(tree_sub));
for iSub = 1:length(tree_sub)
    eucl = eucl_tree(tree_sub{iSub});
    plen = Pvec_tree(tree_sub{iSub});
    bp = B_tree(tree_sub{iSub}); % get branch point
    tp = T_tree(tree_sub{iSub}); % get terminal point
    BTp = bp | tp; % get terminal & branch point
    peucl = eucl(BTp)./plen(BTp);
    meanRatio(iSub) = mean(peucl(~isnan(peucl))); % NaN happens on root, where both eucl and plen are 0
end