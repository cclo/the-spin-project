function cmZ = centerMassZ(tree_sub)
cmZ = zeros(1, length(tree_sub));
for iSub = 1:length(tree_sub)
    cmZ(iSub) = mean(tree_sub{iSub}.Z);
end