% PLength2sub calculates the path length between soma and substructure root
function pLen = PLength2sub(tree_sub)
pLen = zeros(1, length(tree_sub));
pLength_complete = Pvec_tree(tree_sub{1});
for iSub = 1:length(tree_sub)
    % For complete neuron (tree_sub{1}), use the maximun path length in this neuron 
    if iSub == 1
        pLen(iSub) = max(pLength_complete);
    else
        % find the node ID of substructure root in complete neuron
        oriID = findID(tree_sub{1},...
            [tree_sub{iSub}.X(1) tree_sub{iSub}.Y(1) tree_sub{iSub}.Z(1)]); 
        pLen(iSub) = pLength_complete(oriID);
    end
end