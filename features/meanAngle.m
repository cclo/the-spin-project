function meanAng = meanAngle(tree_sub)
meanAng = zeros(1, length(tree_sub));
for iSub = 1:length(tree_sub)
    angles = angleB_tree(tree_sub{iSub});
    meanAng(iSub) = mean(angles(~isnan(angles)));
end