function bf = balFactor(tree_sub)
bf = zeros(1, length(tree_sub));
for iSub = 1:length(tree_sub)
    bf(iSub) = estimateBF_tree(tree_sub{iSub});
end