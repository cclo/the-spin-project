function wh = WHRatio(tree_sub)
wh = zeros(1, length(tree_sub));
for iSub = 1:length(tree_sub)
    h = (max(tree_sub{iSub}.Y) - min(tree_sub{iSub}.Y));
    % if denominator is 0, assign wh = 0
    if h == 0
        wh(iSub) = 0;
    else
         w = (max(tree_sub{iSub}.X) - min(tree_sub{iSub}.X));
         wh(iSub) = w/h;
    end
end