function maxBO = maxBOrder(tree_sub)
maxBO = zeros(1, length(tree_sub));
for iSub = 1:length(tree_sub)
    maxBO(iSub) = max(BO_tree(tree_sub{iSub}));
end