function maxPath = maxPLength(tree_sub)
maxPath = zeros(1, length(tree_sub));
for iSub = 1:length(tree_sub)
    maxPath(iSub) = max(Pvec_tree(tree_sub{iSub}));
end