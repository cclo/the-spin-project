function meanDiffR = meanRootTpDiffRadius(tree_sub)
meanDiffR = zeros(1, length(tree_sub));
for iSub = 1:length(tree_sub)
    if iSub == 1
        meanDiffR(iSub) = (max(tree_sub{iSub}.D) - min(tree_sub{iSub}.D))/2;
    else
        tp = T_tree(tree_sub{iSub});
        meanDiffR(iSub) = mean(tree_sub{iSub}.D(1) - tree_sub{iSub}.D(tp))/2;
    end
end