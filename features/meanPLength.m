function meanPL = meanPLength(tree_sub)
meanPL = zeros(1, length(tree_sub));
for iSub = 1:length(tree_sub)
    meanPL(iSub) = mean(Pvec_tree(tree_sub{iSub}));
end